﻿/************************************************************************/
/* 查询历史成交                                                         */
/* MainBusinessQueryHisWidget                                           */
/************************************************************************/
#include "mainbusinessqueryhiswidget.h"
#include "CommUtil/comfunc.h"

MainBusinessQueryHisWidget::MainBusinessQueryHisWidget(QWidget *parent) :
    QWidget(parent)
{
    setupModel();
    setupViews();
}

void MainBusinessQueryHisWidget::setupModel()
{
    m_modelBusinessQueryHisDetails = new QStandardItemModel(0, 10, this);
    m_modelBusinessQueryHisDetails->setHeaderData(0, Qt::Horizontal, QStringLiteral("资金账号"));
    m_modelBusinessQueryHisDetails->setHeaderData(1, Qt::Horizontal, QStringLiteral("成交日期"));
    m_modelBusinessQueryHisDetails->setHeaderData(2, Qt::Horizontal, QStringLiteral("成交时间"));
    m_modelBusinessQueryHisDetails->setHeaderData(3, Qt::Horizontal, QStringLiteral("成交序号"));
    m_modelBusinessQueryHisDetails->setHeaderData(4, Qt::Horizontal, QStringLiteral("合约代码"));
    m_modelBusinessQueryHisDetails->setHeaderData(5, Qt::Horizontal, QStringLiteral("买卖"));
    m_modelBusinessQueryHisDetails->setHeaderData(6, Qt::Horizontal, QStringLiteral("开平"));
    m_modelBusinessQueryHisDetails->setHeaderData(7, Qt::Horizontal, QStringLiteral("成交价格"));
    m_modelBusinessQueryHisDetails->setHeaderData(8, Qt::Horizontal, QStringLiteral("手数"));
    m_modelBusinessQueryHisDetails->setHeaderData(9, Qt::Horizontal, QStringLiteral("套保标识"));

    m_modelBusinessQueryHis = new QStandardItemModel(0, 8, this);
    m_modelBusinessQueryHis->setHeaderData(0, Qt::Horizontal, QStringLiteral("资金账号"));
    m_modelBusinessQueryHis->setHeaderData(1, Qt::Horizontal, QStringLiteral("合约"));
    m_modelBusinessQueryHis->setHeaderData(2, Qt::Horizontal, QStringLiteral("交易所"));
    m_modelBusinessQueryHis->setHeaderData(3, Qt::Horizontal, QStringLiteral("买卖"));
    m_modelBusinessQueryHis->setHeaderData(4, Qt::Horizontal, QStringLiteral("开平"));
    m_modelBusinessQueryHis->setHeaderData(5, Qt::Horizontal, QStringLiteral("成交均价"));
    m_modelBusinessQueryHis->setHeaderData(6, Qt::Horizontal, QStringLiteral("成交手数"));
    m_modelBusinessQueryHis->setHeaderData(7, Qt::Horizontal, QStringLiteral("手续费"));
}

void MainBusinessQueryHisWidget::setupViews()
{
    hboxlayout = new QHBoxLayout;
    QLabel* accountLabel = new QLabel(QStringLiteral("资金账号"));
    m_accountCombox = new QComboBox;
    m_QueryHisCheckbox = new QCheckBox(QStringLiteral("查历史"));
    m_querybutton = new QPushButton(QStringLiteral("查询"));
    m_exportbutton = new QPushButton(QStringLiteral("导出"));
    accountLabel->setFixedWidth(80);
    m_accountCombox->setFixedWidth(100);
    m_querybutton->setFixedWidth(80);
    m_exportbutton->setFixedWidth(80);

    hboxlayout->addWidget(accountLabel);
    hboxlayout->addWidget(m_accountCombox);
    hboxlayout->addWidget(m_QueryHisCheckbox);
    hboxlayout->addWidget(m_querybutton);
    hboxlayout->addWidget(m_exportbutton);
    hboxlayout->addStretch();
    hboxlayout->addStretch();

	m_DateBoxLayout = new QHBoxLayout;
	QDate  NowDate = QDate::currentDate();
	m_StartLabel = new QLabel(QStringLiteral("起始日期"));
	m_EndLabel = new QLabel(QStringLiteral("结束日期"));
	m_StartDate = new QDateEdit(NowDate);
	m_EndDate   = new QDateEdit(NowDate); 
	m_StartLabel->setFixedWidth(80);
	m_EndLabel->setFixedWidth(80);
	m_StartDate->setFixedWidth(80);
	m_EndDate->setFixedWidth(80);
	m_DateBoxLayout->addWidget(m_StartLabel);
	m_DateBoxLayout->addWidget(m_StartDate);
	m_DateBoxLayout->addWidget(m_EndLabel);
	m_DateBoxLayout->addWidget(m_EndDate);
	m_DateBoxLayout->addStretch();
	m_DateBoxLayout->addStretch();
	m_StartLabel->setHidden(true);
	m_EndLabel->setHidden(true);
	m_StartDate->setHidden(true);
	m_EndDate->setHidden(true);

    hboxlayout1 = new QHBoxLayout;
    m_RadioButton1 = new QRadioButton(QStringLiteral("明细"));
    m_RadioButton2 = new QRadioButton(QStringLiteral("汇总"));
    hboxlayout1->addWidget(m_RadioButton1);
    hboxlayout1->addWidget(m_RadioButton2);
    hboxlayout1->addStretch();
    hboxlayout1->addStretch();

    MainLayout = new QGridLayout(this);

    m_tableBusinessQueryHis = new QTableView;
    m_tableBusinessQueryHis->setAlternatingRowColors(true);
    QFont font = m_tableBusinessQueryHis->horizontalHeader()->font();
    font.setBold(true);
    m_tableBusinessQueryHis->horizontalHeader()->setFont(font);

    m_tableBusinessQueryHis->setModel(m_modelBusinessQueryHis);
    m_tableBusinessQueryHis->setEditTriggers(QAbstractItemView::NoEditTriggers);
    m_tableBusinessQueryHis->verticalHeader()->setVisible(false); //隐藏列表头
    m_tableBusinessQueryHis->verticalHeader()->setFixedWidth(40);
    m_tableBusinessQueryHis->setSelectionBehavior(QAbstractItemView::SelectRows);
    m_tableBusinessQueryHis->setSelectionMode(QAbstractItemView::SingleSelection);

    MainLayout->addLayout(hboxlayout,0,0);
	MainLayout->addLayout(m_DateBoxLayout,1,0);
    MainLayout->addWidget(m_tableBusinessQueryHis,2,0);
    //MainLayout->addLayout(hboxlayout1,3,0);
    MainLayout->setMargin(20);

	//创建菜单、菜单项
	this->m_tableBusinessQueryHis->setContextMenuPolicy(Qt::CustomContextMenu);
	m_RightPopMenu = new QMenu(this->m_tableBusinessQueryHis);
	QAction* updateAction = new QAction(QStringLiteral("刷新"),this);
	QAction* outputAction = new QAction(QStringLiteral("导出"),this); 
	m_RightPopMenu->addAction(updateAction);
	m_RightPopMenu->addAction(outputAction);
    //右键弹出菜单事件绑定
	connect(this->m_tableBusinessQueryHis,SIGNAL(customContextMenuRequested(const QPoint&)),this,SLOT(RightClickedMenuPop(const QPoint&)));
    //查询按钮事件响应
    connect(m_querybutton,SIGNAL(clicked()),this,SLOT(QueryButtonClicked()));
    connect(m_RadioButton1, SIGNAL(toggled(bool)), this, SLOT(radioBtnSlot1()));
    connect(m_RadioButton2, SIGNAL(toggled(bool)), this, SLOT(radioBtnSlot2()));
	connect(m_QueryHisCheckbox,SIGNAL(stateChanged(int)),this,SLOT(QueryHisStateChanged(int)));
	m_RadioButton1->setChecked(true);
}

/************************************************************************/
/*右键弹出菜单响应函数                                                  */
/************************************************************************/
void MainBusinessQueryHisWidget::RightClickedMenuPop(const QPoint& pos)
{
	m_RightPopMenu->exec(QCursor::pos());
}

/************************************************************************/
/* 点击查历史CheckBox事件响应                                           */
/************************************************************************/
void MainBusinessQueryHisWidget::QueryHisStateChanged(int state){
	if (state == Qt::Unchecked){
		m_StartLabel->setHidden(true);
		m_EndLabel->setHidden(true);
		m_StartDate->setHidden(true);
		m_EndDate->setHidden(true);
	} 
	else if(state == Qt::Checked){
		m_StartLabel->setHidden(false);
		m_EndLabel->setHidden(false);
		m_StartDate->setHidden(false);
		m_EndDate->setHidden(false);
	}
}

/************************************************************************/
/* 显示成交记录                                                         */
/************************************************************************/
void MainBusinessQueryHisWidget::ShowTradeDetails(CThostFtdcTradeField *pTrade){
    int iRow = m_modelBusinessQueryHisDetails->rowCount();

    ///资金账号
    ((QStandardItemModel*) m_modelBusinessQueryHisDetails)->setItem(iRow, 0, new QStandardItem(pTrade->UserID));
    ///委托日期
    ((QStandardItemModel*) m_modelBusinessQueryHisDetails)->setItem(iRow, 1, new QStandardItem(QString(pTrade->TradeDate)));
    ///委托时间
    ((QStandardItemModel*) m_modelBusinessQueryHisDetails)->setItem(iRow, 2, new QStandardItem(QString(pTrade->TradeTime)));
    ///委托号
    ((QStandardItemModel*) m_modelBusinessQueryHisDetails)->setItem(iRow, 3, new QStandardItem(pTrade->OrderLocalID));
    ///合约
    ((QStandardItemModel*) m_modelBusinessQueryHisDetails)->setItem(iRow, 4, new QStandardItem(pTrade->InstrumentID));
    ///买卖
    ((QStandardItemModel*) m_modelBusinessQueryHisDetails)->setItem(iRow, 5, new QStandardItem(ComFunc::GetBSName(pTrade->Direction)));
    ///开平
    ((QStandardItemModel*) m_modelBusinessQueryHisDetails)->setItem(iRow, 6, new QStandardItem(ComFunc::GetOCName(pTrade->OffsetFlag)));
    ///价格
    ((QStandardItemModel*) m_modelBusinessQueryHisDetails)->setItem(iRow, 7, new QStandardItem(QString::number(pTrade->Price)));
    ///委手
    ((QStandardItemModel*) m_modelBusinessQueryHisDetails)->setItem(iRow, 8, new QStandardItem(QString::number(pTrade->Volume)));
    ///套保标识
    ((QStandardItemModel*) m_modelBusinessQueryHisDetails)->setItem(iRow, 9, new QStandardItem(ComFunc::GetHedgeType(pTrade->HedgeFlag)));
}

/************************************************************************/
/* 点击查询按钮事件响应                                                 */
/************************************************************************/
void MainBusinessQueryHisWidget::QueryButtonClicked(){
	m_modelBusinessQueryHisDetails->removeRows(0,m_modelBusinessQueryHisDetails->rowCount());
    emit ReqQryTrade(m_accountCombox->currentText());
}
void MainBusinessQueryHisWidget::radioBtnSlot1()
{
    if (m_RadioButton1->isChecked())
    {
        m_tableBusinessQueryHis->setModel(m_modelBusinessQueryHisDetails);
    }
}

void MainBusinessQueryHisWidget::radioBtnSlot2()
{
    if (m_RadioButton2->isChecked())
    {
        m_tableBusinessQueryHis->setModel(m_modelBusinessQueryHis);
    }
}
