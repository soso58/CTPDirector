﻿/************************************
*************************************
       自定义组合行情显示页面
*************************************
*************************************/
#include "marketcomexch.h"

MarketComexch::MarketComexch(QWidget *parent) :
    QWidget(parent)
{
    setupModel();
    setupViews();
}

void MarketComexch::setupModel()
{
    m_MarketselfDelegate = new MarketComExchDelegate;
    m_tableView = new TableView(m_MarketselfDelegate);

    QStringList header;
    header << QStringLiteral("第一腿") << QStringLiteral("第二腿") << QStringLiteral("现买量") << QStringLiteral("现买价") << QStringLiteral("现卖价") << QStringLiteral("现卖量");
    m_tableView->initHeader(header);

    connect(this,SIGNAL(UpdateRow(int,QStringList)),m_tableView,SLOT(UpdateRow(int,QStringList)));
}

void MarketComexch::setupViews()
{
    MainLayout = new QGridLayout(this);
    MainLayout->addWidget(m_tableView,0,0);
    MainLayout->setMargin(20);
}
/**
 * 显示自定义组合行情
 * @brief MarketComexch::ShowDepthMarketData
 * @param pFirstLeg    第一腿行情
 * @param pSecondLeg   第二腿行情
 */
void MarketComexch::ShowDepthMarketData(CThostFtdcDepthMarketDataField *pFirstLeg,CThostFtdcDepthMarketDataField *pSecondLeg){

    //最高买价买入量
    unsigned int buy_high_amount1= pFirstLeg->BidVolume1;///申买量一
    unsigned int buy_high_amount2= pSecondLeg->BidVolume1;///申买量一
    //最低卖价卖量
    unsigned int sale_low_amount1= pFirstLeg->AskVolume1;///申卖量一
    unsigned int sale_low_amount2= pSecondLeg->AskVolume1;///申卖量一
    //最高买入价
    double buy_high_price1=pFirstLeg->BidPrice1;//申买价一
    double buy_high_price2=pSecondLeg->BidPrice1;//申买价一
    //最低卖出价
    double sale_low_price1=pFirstLeg->AskPrice1;///申卖价一
    double sale_low_price2=pSecondLeg->AskPrice1;///申卖价一

    // 现买量
    int buy_high_amount = (buy_high_amount1>sale_low_amount2)?sale_low_amount2:buy_high_amount1;
    // 现卖量
    int sale_low_amount = (sale_low_amount1>buy_high_amount2)?buy_high_amount2:sale_low_amount1;

    // 现买价
    QString csBuyPrice;
    if ((buy_high_price1 > 0) && (sale_low_price2 > 0)){

        csBuyPrice = QString::number(buy_high_price1-sale_low_price2);
    }else{
        csBuyPrice = "-";
    }
    // 现卖价
    QString csSellPrice;
    if ((sale_low_price1 > 0) && (buy_high_price2 > 0)){
        csSellPrice = QString::number(sale_low_price1-buy_high_price2);
    }else{
        csSellPrice = "-";
    }

    QStringList row;
    row<<QString(pFirstLeg->InstrumentID);
    row<<QString(pSecondLeg->InstrumentID);
    row<<QString::number(buy_high_amount);
    row<<csBuyPrice;
    row<<csSellPrice;
    row<<QString::number(sale_low_amount);

    //判断该自定义组合合约在TableView中是否存在 存在则update 不存在则add
    int iRowCnt = m_tableView->rowCount();
    for (int i = 0; i < iRowCnt; i++)
    {
        QModelIndex InstrumentIDIndex1  = m_tableView->model->index(i,0);
        QString InstrumentID1 = m_tableView->model->data(InstrumentIDIndex1,Qt::DisplayRole).toString();

        QModelIndex InstrumentIDIndex2  = m_tableView->model->index(i,1);
        QString InstrumentID2 = m_tableView->model->data(InstrumentIDIndex2,Qt::DisplayRole).toString();
        //QString str = QString(QLatin1String(pDepthMarketData->InstrumentID));

        if (InstrumentID1 == QString(pFirstLeg->InstrumentID) && InstrumentID2 == QString(pSecondLeg->InstrumentID))
        {
            emit UpdateRow(i,row);
			//发射信号 进行组合单下单的判断
			emit SigCombineOrder(QString(pFirstLeg->InstrumentID),QString(pSecondLeg->InstrumentID),buy_high_amount,csBuyPrice,sale_low_amount,csSellPrice);
            return;
        }
    }
    m_tableView->addRow(row);

	//发射信号 进行组合单下单的判断
	emit SigCombineOrder(QString(pFirstLeg->InstrumentID),QString(pSecondLeg->InstrumentID),buy_high_amount,csBuyPrice,sale_low_amount,csSellPrice);


    /*PComSelfCalcRes pRes = new ComSelfCalcRes;

    pRes->buy_high_amount = buy_high_amount;
    pRes->sale_low_amount = sale_low_amount;
    pRes->csBuyPrice = csBuyPrice;
    pRes->csSellPrice = csSellPrice;
    pRes->nRowIndex = nRowIndex;
    pRes->csLeg1= CComFun::FormatCS(pMsgLeg1->contract_code);
    pRes->csLeg2= CComFun::FormatCS(pMsgLeg2->contract_code);*/
}
