﻿#include "mainentrustqueryhiswidget.h"
#include <QMessageBox>

MainEntrustQueryHisWidget::MainEntrustQueryHisWidget(QWidget *parent) :
    QWidget(parent)
{
    setupModel();
    setupViews();
}

void MainEntrustQueryHisWidget::setupModel()
{
    m_modelEntrustQueryHis = new QStandardItemModel(0, 13, this);
    m_modelEntrustQueryHis->setHeaderData(0, Qt::Horizontal, QStringLiteral("资金账号"));
    m_modelEntrustQueryHis->setHeaderData(1, Qt::Horizontal, QStringLiteral("委托日期"));
    m_modelEntrustQueryHis->setHeaderData(2, Qt::Horizontal, QStringLiteral("委托时间"));
    m_modelEntrustQueryHis->setHeaderData(3, Qt::Horizontal, QStringLiteral("委托号"));
	m_modelEntrustQueryHis->setHeaderData(4, Qt::Horizontal, QStringLiteral("相关报单"));
    m_modelEntrustQueryHis->setHeaderData(5, Qt::Horizontal, QStringLiteral("合约"));
    m_modelEntrustQueryHis->setHeaderData(6, Qt::Horizontal, QStringLiteral("买卖"));
    m_modelEntrustQueryHis->setHeaderData(7, Qt::Horizontal, QStringLiteral("开平"));
    m_modelEntrustQueryHis->setHeaderData(8, Qt::Horizontal, QStringLiteral("价格"));
    m_modelEntrustQueryHis->setHeaderData(9, Qt::Horizontal, QStringLiteral("委手"));
    m_modelEntrustQueryHis->setHeaderData(10, Qt::Horizontal, QStringLiteral("成手"));
    m_modelEntrustQueryHis->setHeaderData(11, Qt::Horizontal, QStringLiteral("状态"));
    m_modelEntrustQueryHis->setHeaderData(12, Qt::Horizontal, QStringLiteral("套保标识"));
}

void MainEntrustQueryHisWidget::setupViews()
{
    hboxlayout = new QHBoxLayout;
    QLabel* accountLabel = new QLabel(QStringLiteral("资金账号"));
    m_accountCombox = new QComboBox;
    m_QueryHisCheckbox = new QCheckBox(QStringLiteral("查历史"));
    m_querybutton = new QPushButton(QStringLiteral("查询"));
    m_exportbutton = new QPushButton(QStringLiteral("导出"));
    accountLabel->setFixedWidth(80);
    m_accountCombox->setFixedWidth(100);
    m_querybutton->setFixedWidth(80);
    m_exportbutton->setFixedWidth(80);

    hboxlayout->addWidget(accountLabel);
    hboxlayout->addWidget(m_accountCombox);
    hboxlayout->addWidget(m_QueryHisCheckbox);
    hboxlayout->addWidget(m_querybutton);
    hboxlayout->addWidget(m_exportbutton);
    hboxlayout->addStretch();
    hboxlayout->addStretch();

	m_DateBoxLayout = new QHBoxLayout;
	QDate  NowDate = QDate::currentDate();
	m_StartLabel = new QLabel(QStringLiteral("起始日期"));
	m_EndLabel = new QLabel(QStringLiteral("结束日期"));
	m_StartDate = new QDateEdit(NowDate);
	m_EndDate   = new QDateEdit(NowDate); 
	m_StartLabel->setFixedWidth(80);
	m_EndLabel->setFixedWidth(80);
	m_StartDate->setFixedWidth(80);
	m_EndDate->setFixedWidth(80);
	m_DateBoxLayout->addWidget(m_StartLabel);
	m_DateBoxLayout->addWidget(m_StartDate);
	m_DateBoxLayout->addWidget(m_EndLabel);
	m_DateBoxLayout->addWidget(m_EndDate);
	m_DateBoxLayout->addStretch();
	m_DateBoxLayout->addStretch();
	m_StartLabel->setHidden(true);
	m_EndLabel->setHidden(true);
	m_StartDate->setHidden(true);
	m_EndDate->setHidden(true);


    MainLayout = new QGridLayout(this);

    m_tableEntrustQueryHis = new QTableView;
    m_tableEntrustQueryHis->setAlternatingRowColors(true);
    QFont font = m_tableEntrustQueryHis->horizontalHeader()->font();
    font.setBold(true);
    m_tableEntrustQueryHis->horizontalHeader()->setFont(font);
//    tableOrder->setStyleSheet("QTableView::item:selected { selection-color: rgb(0, 0, 0) }" "QTableView::item:selected { background-color: rgb(255, 255, 0) }"
//                "QTableView{background-color: rgb(0, 0, 0);" "alternate-background-color: rgb(41, 36, 33);}");

    m_tableEntrustQueryHis->setModel(m_modelEntrustQueryHis);
    m_tableEntrustQueryHis->setEditTriggers(QAbstractItemView::NoEditTriggers);
    m_tableEntrustQueryHis->verticalHeader()->setVisible(false); //隐藏列表头
    m_tableEntrustQueryHis->verticalHeader()->setFixedWidth(40);
    m_tableEntrustQueryHis->setSelectionBehavior(QAbstractItemView::SelectRows);
    m_tableEntrustQueryHis->setSelectionMode(QAbstractItemView::SingleSelection);

    m_tableEntrustQueryHis->setColumnWidth(0, 80);
    m_tableEntrustQueryHis->setColumnWidth(1, 80);
    m_tableEntrustQueryHis->setColumnWidth(2, 80);
    m_tableEntrustQueryHis->setColumnWidth(3, 80);
    m_tableEntrustQueryHis->setColumnWidth(4, 80);
    m_tableEntrustQueryHis->setColumnWidth(5, 80);
    m_tableEntrustQueryHis->setColumnWidth(6, 80);
    m_tableEntrustQueryHis->setColumnWidth(7, 80);
    m_tableEntrustQueryHis->setColumnWidth(8, 80);
    m_tableEntrustQueryHis->setColumnWidth(9, 80);
    m_tableEntrustQueryHis->setColumnWidth(10, 80);
    m_tableEntrustQueryHis->setColumnWidth(11, 80);
    m_tableEntrustQueryHis->setColumnWidth(12, 80);

    MainLayout->addLayout(hboxlayout,0,0);
	MainLayout->addLayout(m_DateBoxLayout,1,0);
    MainLayout->addWidget(m_tableEntrustQueryHis,2,0);
    MainLayout->setMargin(20);

	//创建菜单、菜单项
	this->m_tableEntrustQueryHis->setContextMenuPolicy(Qt::CustomContextMenu);
	m_RightPopMenu = new QMenu(this->m_tableEntrustQueryHis);
	m_deleteOrderAction = new QAction(QStringLiteral("撤单"),this); 
	m_updateAction = new QAction(QStringLiteral("刷新"),this); 	
	m_outputAction = new QAction(QStringLiteral("导出"),this); 
	m_RightPopMenu->addAction(m_deleteOrderAction);
	m_RightPopMenu->addAction(m_updateAction);	
	m_RightPopMenu->addAction(m_outputAction);

    //右键弹出菜单事件绑定
	connect(this->m_tableEntrustQueryHis,SIGNAL(customContextMenuRequested(const QPoint&)),this,SLOT(RightClickedMenuPop(const QPoint&)));
	//查询按钮响应事件绑定
	connect(this->m_querybutton,SIGNAL(clicked()),this,SLOT(QueryButtonClicked()));
	//查询历史CheckBox状态切换事件绑定
	connect(this->m_QueryHisCheckbox,SIGNAL(stateChanged(int)),this,SLOT(QueryHisStateChanged(int)));
	
}
///右键弹出菜单响应函数
void MainEntrustQueryHisWidget::RightClickedMenuPop(const QPoint& pos)
{
	m_RightPopMenu->exec(QCursor::pos());
}

void MainEntrustQueryHisWidget::QueryHisStateChanged(int state){
	if (state == Qt::Unchecked){
		m_StartLabel->setHidden(true);
		m_EndLabel->setHidden(true);
		m_StartDate->setHidden(true);
		m_EndDate->setHidden(true);
	}else if(state == Qt::Checked){
		m_StartLabel->setHidden(false);
		m_EndLabel->setHidden(false);
		m_StartDate->setHidden(false);
		m_EndDate->setHidden(false);
	}
}

void MainEntrustQueryHisWidget::QueryButtonClicked(){
	QStringList strAttrs;
	strAttrs.push_back(m_accountCombox->currentText());
	if (m_QueryHisCheckbox->isChecked())
	{
		if(m_StartDate->date() > m_EndDate->date()){
			QMessageBox::critical(NULL,QString("ERROR"),QStringLiteral("起始时间不能大于终止时间!"));
			return;
		}else{
			strAttrs.push_back(m_StartDate->date().toString("yyyyMMdd"));
			strAttrs.push_back(m_EndDate->date().toString("yyyyMMdd"));
		}
	}
    m_modelEntrustQueryHis->removeRows(0,m_modelEntrustQueryHis->rowCount());
    emit QueryOrder(strAttrs);
}
void MainEntrustQueryHisWidget::ShowOrderDetails(CThostFtdcOrderField *pOrder){

    int iRow = m_modelEntrustQueryHis->rowCount();
    for (int i = 0; i < iRow ; i++){
        ///委托号
        QString strOrderLocalID =  ((QStandardItemModel*) m_modelEntrustQueryHis)->item(i,3)->text();
        if (0 == strcmp(strOrderLocalID.toStdString().c_str(),pOrder->OrderLocalID)){
            iRow = i;
            break;
        }
    }

    ///资金账号
    ((QStandardItemModel*) m_modelEntrustQueryHis)->setItem(iRow, 0, new QStandardItem(pOrder->UserID));
    ///委托日期
    ((QStandardItemModel*) m_modelEntrustQueryHis)->setItem(iRow, 1, new QStandardItem(QString(pOrder->InsertDate)));
    ///委托时间
    ((QStandardItemModel*) m_modelEntrustQueryHis)->setItem(iRow, 2, new QStandardItem(QString(pOrder->InsertTime)));
    ///委托号
    ((QStandardItemModel*) m_modelEntrustQueryHis)->setItem(iRow, 3, new QStandardItem(pOrder->OrderLocalID));
	///相关报单
	((QStandardItemModel*) m_modelEntrustQueryHis)->setItem(iRow, 4, new QStandardItem(pOrder->RelativeOrderSysID));
    ///合约
    ((QStandardItemModel*) m_modelEntrustQueryHis)->setItem(iRow, 5, new QStandardItem(pOrder->InstrumentID));
    ///买卖
    if (pOrder->Direction == THOST_FTDC_D_Buy){
        ((QStandardItemModel*) m_modelEntrustQueryHis)->setItem(iRow, 6, new QStandardItem(QStringLiteral("买入")));
    } else if(pOrder->Direction == THOST_FTDC_D_Sell){
        ((QStandardItemModel*) m_modelEntrustQueryHis)->setItem(iRow, 6, new QStandardItem(QStringLiteral("卖出")));
    }
    ///开平
    if (pOrder->CombOffsetFlag[0] == '0'){//开仓
        ((QStandardItemModel*) m_modelEntrustQueryHis)->setItem(iRow, 7, new QStandardItem(QStringLiteral("开仓")));
    }
    else{//平仓
        ((QStandardItemModel*) m_modelEntrustQueryHis)->setItem(iRow, 7, new QStandardItem(QStringLiteral("平仓")));
    }
    ///价格
    ((QStandardItemModel*) m_modelEntrustQueryHis)->setItem(iRow, 8, new QStandardItem(QString::number(pOrder->LimitPrice)));
    ///委手
    ((QStandardItemModel*) m_modelEntrustQueryHis)->setItem(iRow, 9, new QStandardItem(QString::number(pOrder->VolumeTotalOriginal)));
    ///成手
    ((QStandardItemModel*) m_modelEntrustQueryHis)->setItem(iRow, 10, new QStandardItem(QString::number(pOrder->VolumeTraded)));
    ///状态
	if (strcmp(pOrder->StatusMsg,"") == 0)
	{  //条件单
		if (pOrder->OrderStatus == THOST_FTDC_OST_Unknown){
			((QStandardItemModel*) m_modelEntrustQueryHis)->setItem(iRow, 11, new QStandardItem(QStringLiteral("条件单:状态未知")));
		}else if (pOrder->OrderStatus == THOST_FTDC_OST_NotTouched)	{
			((QStandardItemModel*) m_modelEntrustQueryHis)->setItem(iRow, 11, new QStandardItem(QStringLiteral("条件单:尚未触发")));
		}else if(pOrder->OrderStatus == THOST_FTDC_OST_Touched)	{
			((QStandardItemModel*) m_modelEntrustQueryHis)->setItem(iRow, 11, new QStandardItem(QStringLiteral("条件单:已触发")));
		}
	} else{
		((QStandardItemModel*) m_modelEntrustQueryHis)->setItem(iRow, 11, new QStandardItem(QString::fromLocal8Bit(pOrder->StatusMsg)));
	}    
    ///套保标识
    if (pOrder->CombHedgeFlag[0] == THOST_FTDC_HF_Speculation){
        ((QStandardItemModel*) m_modelEntrustQueryHis)->setItem(iRow, 12, new QStandardItem(QStringLiteral("投机")));
    }else if(pOrder->CombHedgeFlag[0] == THOST_FTDC_HF_Arbitrage){
        ((QStandardItemModel*) m_modelEntrustQueryHis)->setItem(iRow, 12, new QStandardItem(QStringLiteral("套利")));
    }else if(pOrder->CombHedgeFlag[0] == THOST_FTDC_HF_Hedge){
        ((QStandardItemModel*) m_modelEntrustQueryHis)->setItem(iRow, 12, new QStandardItem(QStringLiteral("套保")));
    }
}
