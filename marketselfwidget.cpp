﻿/************************************
*************************************
       自选行情显示页面
*************************************
*************************************/
#include "marketselfwidget.h"

MarketSelfWidget::MarketSelfWidget(QWidget *parent) :
    QWidget(parent)
{
    setupModel();
    setupViews();
}

void MarketSelfWidget::setupModel()
{
    m_MarketselfDelegate = new MarketSelfDelegate;
    m_tableView = new TableView(m_MarketselfDelegate);

    QStringList header;
    header << QStringLiteral("合约代码") << QStringLiteral("最新价") << QStringLiteral("买价") << QStringLiteral("买量") << QStringLiteral("卖价") << QStringLiteral("卖量");
    header << QStringLiteral("涨跌") << QStringLiteral("涨跌幅") << QStringLiteral("成交量") << QStringLiteral("持仓量") << QStringLiteral("最高") << QStringLiteral("最低");
    header << QStringLiteral("开盘价") << QStringLiteral("昨结算") << QStringLiteral("涨停板") << QStringLiteral("跌停板") << QStringLiteral("均价")<<QStringLiteral("涨跌");
    m_tableView->initHeader(header);
    m_tableView->setBackgroundRole(QPalette::Shadow);
}

void MarketSelfWidget::setupViews()
{
    MainLayout = new QGridLayout(this);

    MainLayout->addWidget(m_tableView,0,0);
    MainLayout->setMargin(20);

    connect(this,SIGNAL(UpdateRow(int,QStringList)),m_tableView,SLOT(UpdateRow(int,QStringList)));
}
void MarketSelfWidget::ShowDepthMarketData(CThostFtdcDepthMarketDataField *pDepthMarketData){
	QStringList row_list;
    //单合约行情显示
	row_list << QString(pDepthMarketData->InstrumentID);///合约代码
	row_list << QString::number(pDepthMarketData->LastPrice);///最新价
	row_list << QString::number(pDepthMarketData->BidPrice1);///申买价一
	row_list << QString::number(pDepthMarketData->BidVolume1);///申买量一
	row_list << QString::number(pDepthMarketData->AskPrice1);///申卖价一
	row_list << QString::number(pDepthMarketData->AskVolume1);///申卖量一
	row_list << QString::number(double(pDepthMarketData->LastPrice -pDepthMarketData->PreSettlementPrice));///涨跌 当前价减去昨结
    double tmp = (pDepthMarketData->LastPrice -pDepthMarketData->PreSettlementPrice)/pDepthMarketData->PreSettlementPrice*100;
    row_list <<QString::number(tmp,'g',3)  + QString("%");///涨跌幅
	row_list << QString::number(pDepthMarketData->Volume);///数量
	row_list << QString::number(pDepthMarketData->OpenInterest);///持仓量
	row_list << QString::number(pDepthMarketData->HighestPrice);///最高价
	row_list << QString::number(pDepthMarketData->LowestPrice);///最低价
	row_list << QString::number(pDepthMarketData->OpenPrice);///今开盘
	row_list << QString::number(pDepthMarketData->PreSettlementPrice);///上次结算价
	row_list << QString::number(pDepthMarketData->UpperLimitPrice);///涨停板价
	row_list << QString::number(pDepthMarketData->LowerLimitPrice);///跌停板价
	row_list << QString::number(pDepthMarketData->AveragePrice,'f');///当日均价
	
    int iRowCnt = m_tableView->rowCount();
	for (int i = 0; i < iRowCnt; i++)
	{
        QModelIndex InstrumentIDIndex  = m_tableView->model->index(i,0);
        QString InstrumentID = m_tableView->model->data(InstrumentIDIndex,Qt::DisplayRole).toString();
		QString str = QString(QLatin1String(pDepthMarketData->InstrumentID));
		if (InstrumentID == str)
		{
            QModelIndex lastPriceIndex  = m_tableView->model->index(i,1);
            double oldLastPrice = m_tableView->model->data(lastPriceIndex,Qt::DisplayRole).toDouble();
			if (pDepthMarketData->LastPrice >= oldLastPrice){
				row_list<<"1";//涨
			}else{
				row_list<<"0";//跌
			}
			emit UpdateRow(i,row_list);
			return;
		} 
	}
	if (row_list.size() < 18)
	{
        row_list<<"1";
	}

    m_tableView->addRow(row_list);
}
