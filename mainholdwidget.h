﻿#ifndef MAINHOLDWIDGET_H
#define MAINHOLDWIDGET_H

#include <QWidget>
#include <qstandarditemmodel.h>
#include <qabstractitemmodel.h>
#include <string.h>
#include <qtableview.h>
#include <qheaderview.h>
#include <qgridlayout.h>
#include <QHBoxLayout>
#include <QLabel>
#include <QComboBox>
#include <QPushButton>
#include <QMenu>
#include <QAction>
#include"CtpInterface/ThostFtdcUserApiStruct.h"

class MainHoldWidget : public QWidget
{
    Q_OBJECT
public:
    explicit MainHoldWidget(QWidget *parent = 0);

    void OnEntrust(CThostFtdcInputOrderField &OrderInfo);

signals:
    void ReqQryInvestorPosition(QString strAccountID);
    //报单请求
    void OnOrderInsert(CThostFtdcInputOrderField *pInputOrder);

public slots:
    void QueryButtonClicked();
	//右键弹出菜单响应函数
	void RightClickedMenuPop(const QPoint& pos);
	//市价平仓事件绑定
	void ShijiaAction();
	//对价平仓事件绑定
	void DuijiaAction();
	//刷新事件绑定
	void UpdateHold();
	//导出事件绑定
	void OutputHold();

public:
    void setupModel();
    void setupViews();
    void ShowInvestorPosition(CThostFtdcInvestorPositionField *pInvestorPosition);


public:
    QTableView*         m_tableHold;
    QAbstractItemModel* m_modelHold;
    QComboBox*          m_accountCombox;
	//保存持仓信息
	QVector<CThostFtdcInvestorPositionField>  m_HoldOrderVector;

private:
    QPushButton*		m_querybutton;
    QPushButton*		m_exportbutton;
	QMenu*				m_RightPopMenu;
	QAction*			m_ShijiaAction; 
	QAction*			m_DuijiaAction; 
	QAction*			m_updateAction;
	QAction*			m_outputAction; 

    QItemSelectionModel *selectionModel;
    QGridLayout *MainLayout;
    QHBoxLayout *hboxlayout;
};

#endif // MAINHOLDWIDGET_H
