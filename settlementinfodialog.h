#ifndef SETTLEMENTINFODIALOG_H
#define SETTLEMENTINFODIALOG_H

#include <QDialog>

namespace Ui {
class SettlementInfoDialog;
}

class SettlementInfoDialog : public QDialog
{
    Q_OBJECT

public:
    explicit SettlementInfoDialog(QWidget *parent = 0);
    ~SettlementInfoDialog();
    void OutputContent(QString content);
public slots:
    void SlotOKButtonClicked();
    void SlotCancelButtonClicked();
private:
    Ui::SettlementInfoDialog *ui;
};

#endif // SETTLEMENTINFODIALOG_H
