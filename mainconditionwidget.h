#ifndef MAINCONDITIONWIDGET_H
#define MAINCONDITIONWIDGET_H

#include <QWidget>
#include <qstandarditemmodel.h>
#include <qabstractitemmodel.h>
#include <string.h>
#include <qtableview.h>
#include <qheaderview.h>
#include <qgridlayout.h>
#include <QHBoxLayout>
#include <QCheckBox>
#include <QPushButton>
#include <QMenu>
#include <QAction>
#include "CtpInterface/ThostFtdcUserApiStruct.h"
#include "CommUtil/common.h"

class MainConditionWidget : public QWidget
{
    Q_OBJECT
public:
    explicit MainConditionWidget(QWidget *parent = 0);

    //void ShowConditionOrder(CThostFtdcInputOrderField* pOrder,ConditionType* pCondition);
	void ShowConditionOrder(CThostFtdcOrderField *pOrder);
signals:

public slots:
    ///�Ҽ������˵���Ӧ����
	void RightClickedMenuPop(const QPoint& pos);

public:
    void setupModel();
    void setupViews();

public:
    QTableView*				m_tableCondition;
    QAbstractItemModel*		m_modelCondition;

private:
    QItemSelectionModel*    selectionModel;
    QGridLayout*            MainLayout;
    QHBoxLayout*            hboxlayout;
	QMenu*					m_RightPopMenu;
};

#endif // MAINCONDITIONWIDGET_H
