﻿#ifndef MAINAVERAGEENTRUSTWIDGET_H
#define MAINAVERAGEENTRUSTWIDGET_H

#include <QWidget>
#include <qstandarditemmodel.h>
#include <qabstractitemmodel.h>
#include <string.h>
#include <qtableview.h>
#include <qheaderview.h>
#include <qgridlayout.h>
#include <QHBoxLayout>
#include <QLabel>
#include <QComboBox>
#include <QPushButton>
#include <QCheckBox>
#include <QLineEdit>
#include <QSpinBox>
#include <QTreeWidget>
#include "CtpInterface/ThostFtdcUserApiStruct.h"
#include "CommUtil/common.h"
#include <QTimerEvent>

class TimerObject : public QObject {
	Q_OBJECT
public:
	TimerObject();

private:
};

class MainAverageEntrustWidget : public QWidget
{
    Q_OBJECT
public:
    explicit MainAverageEntrustWidget(QWidget *parent = 0);

signals:
    // 添加均价单
    //void NewAverageOrder(AVERAGEORDERINFO averageOrder);
	void SigStartTimer(QString AverageNo,int iTimeSpan);

	//均价报单请求
	void OnOrderInsert(CThostFtdcInputOrderField *pInputOrder,QString AverageNo);

public slots:
    void SelectContract(const QModelIndex & index);
    void OKButtonClicked();
	void SlotStartTimer(QString AverageNo,int iTimeSpan);
public:
    void setupViews();
	void ShowAverageOrder(int icnt, void* pData);
	void ShowAverageOrder(QString strBatchNo, CThostFtdcOrderField *pOrder);
	void SetIfTimer(QString AverageNo);
	void SendOneOrderOfAvg(QString AverageNo, double dPrice=0);


protected:
	void timerEvent(QTimerEvent *);

public:
    QTreeWidget*                m_treeWidget;
    QComboBox*                  m_accountCombox;//资金帐号
    QLineEdit*                  m_contractEdit;//合约

    QComboBox*                  m_bsCombox;//买卖
    QComboBox*                  m_ocCombox;//开平
    QSpinBox*                   m_HandsSpinBox;//每次下单手数
    QSpinBox*                   m_TotalHandsSpinBox;//总手数

    QLineEdit*                  m_timeEdit;//时间间隔
    QCheckBox*                  m_OrderTypeCheckbox;//套保CheckBox
private:
    //均价单Map
    QMap<QString, STAverageInfo> m_MapAverageOrder;
	QMap<int,QString>            m_MapTimerID;

    QItemSelectionModel*        selectionModel;
    QGridLayout*                MainLayout;
    QGridLayout*                layout;
    QPushButton*                m_ConfirmButton;
};

#endif // MAINAVERAGEENTRUSTWIDGET_H
