#ifndef COMFUNC_H
#define COMFUNC_H
#include<QString>
#include<QByteArray>
#include "../CtpInterface/ThostFtdcUserApiStruct.h"

class ComFunc
{
public:
    ComFunc();

    ///判断Qstring中是不是纯数字
    static int isDigitStr(QString src);
	static QString GetCofcoStatusName(int iStatus);
	static QString GetCofcoOrderType(int iType);
	static QString GetBSName( TThostFtdcDirectionType Direction);
	static QString GetOCName( TThostFtdcOffsetFlagType OffsetFlag);
	static QString GetHedgeType(TThostFtdcHedgeFlagType HedgeFlag);
	static QString GetCurrTime();

};

#endif // COMFUNC_H
