﻿#include "comfunc.h"
#include <QDateTime>

ComFunc::ComFunc()
{

}
///判断Qstring中是不是纯数字
int ComFunc::isDigitStr(QString src){
    QByteArray ba = src.toLatin1();//QString 转换为 char*
    const char *s = ba.data();

    while(*s && *s>='0' && *s<='9')
        s++;

    if (*s){ //不是纯数字
        return -1;
    }else{ //纯数字
        return 0;
    }
}

QString ComFunc::GetCofcoStatusName(int iStatus)
{
	QString sStatus;
	switch(iStatus)
	{
	case 1 :{sStatus=QStringLiteral("已报"); break;}
	case 2 :{sStatus=QStringLiteral("部成"); break;}	
	case 3 :{sStatus=QStringLiteral("完成"); break;}
	case 4 :{sStatus=QStringLiteral("已撤"); break;}		
	default: {sStatus=QStringLiteral("未知"); break;};
	}
	return sStatus;
}

QString ComFunc::GetCofcoOrderType(int iType)
{
	QString sType;
	switch(iType)
	{
	case 1 :{sType=QStringLiteral("均价"); break;}
	case 2 :{sType=QStringLiteral("套利"); break;}	
	case 3 :{sType=QStringLiteral("移仓"); break;}	
	default: {sType=QStringLiteral("未知"); break;};
	}
	return sType;
}

QString ComFunc::GetBSName( TThostFtdcDirectionType Direction)
{
	QString sBS;
	switch(Direction)
	{
	case THOST_FTDC_D_Buy :{sBS=QStringLiteral("买入"); break;}
	case THOST_FTDC_D_Sell :{sBS=QStringLiteral("卖出"); break;}				 
	default: {sBS=QStringLiteral("未知"); break;};
	}
	return sBS;
}

QString ComFunc::GetOCName( TThostFtdcOffsetFlagType OffsetFlag)
{
	QString sOC;
	switch(OffsetFlag)
	{
	case THOST_FTDC_OF_Open :sOC=QStringLiteral("开仓"); break;
	case THOST_FTDC_OF_Close :sOC=QStringLiteral("平仓"); break;
	case THOST_FTDC_OF_CloseToday :sOC=QStringLiteral("平今仓"); break;
	default: sOC=QStringLiteral("未知"); break;
	}
	return sOC;
}

QString ComFunc::GetHedgeType( TThostFtdcHedgeFlagType HedgeFlag)
{
	QString csHedge;
	switch(HedgeFlag)
	{
	case THOST_FTDC_HF_Speculation :{csHedge=QStringLiteral("投机"); break;}
	case THOST_FTDC_HF_Arbitrage :{csHedge=QStringLiteral("套利"); break;}	
	case THOST_FTDC_HF_Hedge :{csHedge=QStringLiteral("套保"); break;}	
	default: {csHedge=QStringLiteral("投机"); break;};
	}
	return csHedge;
}

QString ComFunc::GetCurrTime()
{
	QDateTime oTime =  QDateTime::currentDateTime();
	return oTime.toString("hhmmss");
}