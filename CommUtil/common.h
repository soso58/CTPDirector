#ifndef COMMON_H
#define COMMON_H

#define SHADOW_WIDTH            5
#define WINDOW_WIDTH            680
#define WINDOW_HEIGHT           372
#define WINDOW_START_X          0
#define WINDOW_START_Y          0
#define WINDOW_PAGE_COUNT       4
#define WINDOW_BUTTON_COUNT     4
#define WINDOW_PAGE_MOVE        20
#define WINDOW_ONEBUTTON_WIDTH  170
#define DEFAULT_SKIN            ":/skin/17_big"

#include<QString>

enum MSGTYPE{
	HS=1,			    //恒生
	FFTS,		        //委托
	OTHER,              //其他类型消息
};

enum MSGSTATUS{
	FAILURE,			    //S失败
	SUCCESS,		        //成功
};
// 均价单结构体
typedef struct _ST_AVERAGE_INFO {
	QString    var0_AverageNumber;     //均价单号
	QString    var1_szAccount;         //资金账号
	QString    var2_sContractCode;     //合约名称
	QString    var3_sBsName;           //买卖   
	QString    var4_sOcName;           //开平		
	QString    var8_sHedge;            //投机套保	
	QString    var10_sStatus;          //订单状态
	QString    var11_sRunStatus;       //启用状态	

	QString    var15_sEntrustTime;     //委托时间
	QString    var16_sGapStatus;       //小节状态   开始，委托，完成
	QString    var17_sEntrustType;     //上次委托类型
	QString    var18_sEntrustNo;       //前次委托号

	int        TimeSpin;               //时间间隔
	int        EachHand;               //每手下单数
	int        TotalHand;              //总手数
	int        SendHand;               //已报手数
	int        BusinessHand;           //已成手数	

	double     UpPrice;                //涨停板
	double     DownPrice;              //跌停板
	double     BussPrice;        //成交均价

	bool       bRspOrder;              // 下单反馈
	bool       bRspEntrust;            // 收到委托回报，收到委托回报才能继续判断行情
	bool       bRspBusiness;           // 收到成交回报
} STAverageInfo;

// 组合单结构体
typedef struct _ST_COMBINE_INFO {
	QString    var0_CombineNumber;     //组合单号
	QString    var1_szAccount;         //资金账号
	QString    var2_sLeg1;             //第一腿
	QString    var3_sLeg2;             //第二腿
	QString    var4_sOrderType;        // 套利 移仓
	QString    var5_sBsName;           //买卖   
	QString    var6_sOcName;           //开平
	int        TotalHand;              //总手数
	double     var8_Price;             //价差
	QString    var9_sHedge1;           //一腿投机套保
	QString    var9_sHedge2;           //二腿投机套保
	QString    var10_sStatus;          //订单状态
	int        SendHand1;              //第一腿发送手数
	int        SendHand2;              //第二腿发送手数
	int        BusHand1;               //第一腿已成手数
	int        BusHand2;               //第二腿已成手数
	QString    var15_sEntrustTime;     //委托时间
	QString    var16_sEnableStatus;    //启用状态
	double     dBussPrice;             //成交价差

	QString    sEntrustNoLeg1;         //前次委托号
	QString    sEntrustNoLeg2;         //前次委托号
	bool       bRspOrderLeg1;          // 下单反馈
	bool       bRspEntrustLeg1;        // 收到委托回报，收到委托回报才能继续判断行情
	bool       bRspBusinessLeg1;       // 收到成交回报
	bool       bRspOrderLeg2;          // 下单反馈
	bool       bRspEntrustLeg2;        // 收到委托回报，收到委托回报才能继续判断行情
	bool       bRspBusinessLeg2;       // 收到成交回报

	double     dUpPrice1;              // 涨停1
	double     dDownPrice1;            // 跌停1
	double     dUpPrice2;              // 涨停2
	double     dDownPrice2;            // 跌停2
	int        iLastEntrustTime;       //上次委托时间

} STCombineInfo;

//委托反馈信息
struct COrderRspInfo
{
	int          entrust_no;            //0 委托号
	char         futures_account[21];   //1 交易编码
	char         futu_exch_type[11];    //2 交易所类别
	char         contract_code[13];     //3 合约代码
	char         entrust_bs[9];         //4 买卖标识
	char         entrust_direction[9];  //6 开平标识
	char         hedge_type[9];         //7 套保标识
	int          fund_account;          //8 资金账户
	char         futu_report_no[21];    //9 本地单号
	char         firm_no[9];            //10 会员号
	char         operator_no[9];        //11 操作员号
	int          client_group;          //12 客户类别
	int          entrust_amount;        //13 委托数量
	int          business_total_amount; //14 成交总数量
	int          cacel_amount;          //15 撤单数量
	double       entrust_price;         //16 委托价格
	char         entrust_status;        //17 委托状态
	int          branch_no;             //18 营业部号
	int          batch_no;              //19 委托批号
	char         futu_entrust_type;     //20 委托类型
	int          amount_per_hand;       //21 合约称数
	char         forceclose_reason;     //22 强平原因
	int          init_date;             //23 交易日期
	int          curr_time;             //24 当前时间
	char         confirm_no [21];       //25 主场单号
	int          weave_type;            //26 组合委托类型
	char         arbitrage_code [21];   //27 套利合约代码
	int          time_condition;        //28 有效期类型
	int          volume_condition;      //29 成交量类型
	int          futu_entrust_prop;     //30 期货委托属性
	double       frozen_fare;           //31 冻结总费用
	//20120428 tanghui
	char		 error_message[255];    //32 错误信息
};

typedef struct _MSG_QUEUE_CONTEXT {
	int iMsgType;    //服务器消息类型   HS FFTS
	int iMsglen;     // buffer changdu
	int iMsgDtlType; // 详细类型
	int iMsgStatus;  // 执行状态 0失败  1成功
	int iMsgFrom;    // 回调时，保存发送句柄 hsend  
	int iMsgTime;    // 消息发过来的时间
	//int iHsType;     //恒生系统类型    0：交易  1：委托反馈 2：成交回报  -1错误  3:自定义单腿 4：自定义组合 5：交易所组合
	union
	{
		void* msgContext;                //交易
		COrderRspInfo m_pCOrderData;     //委托
		//CRealRspInfo m_pRealData;        //成交
		//CMarketInfo m_pCMarketInfo;      //行情		
		//CArgMarketInfo m_pArgMarketInfo; //交易所组合行情		
		//IFuMessage* m_pFuMessage;        //接口
	}msg;

} Msg_Queue_Context, *PMsg_Queue_Context;


struct ServerInfo{
    QString ServerName;
    QString ServerTradeAddr;
    QString ServerMarketAddr;
};

struct ConditionType{
    QString     PriceKind;
    QString     Symbol;
    double      Price;
    QString     SaveKind;
};

struct MarketComExchInstrument{
    QString m_strLeg1;
    QString m_strLeg2;
};

typedef enum{
	UI_EN,
	UI_ZH
}LANGUAGE;

//typedef enum{
    //SAFE,
    //SINA,
    //RENREN
//}LOGIN;

typedef enum{
	HELP_ONLINE,
	PLATFORM_HELP,
	LOGIN_HOME,
	PROTECT
}ACTION;

//枚举按钮的几种状态
typedef enum{
	NORMAL,
	ENTER,
	PRESS,
	NOSTATUS
}ButtonStatus;

#endif // COMMON_H
