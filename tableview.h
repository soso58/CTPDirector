#ifndef TABLEVIEW_H
#define TABLEVIEW_H

#include <QTableView>
#include <QAbstractTableModel>
#include <QItemDelegate>
#include <QStringList>
#include <QPainter>
#include <QHeaderView>

/******************************************************
 * 自定义组合行情显示的代理
 * MarketComExchDelegate
 ******************************************************/
class MarketComExchDelegate : public QItemDelegate
{
    Q_OBJECT

public:

    MarketComExchDelegate(QObject* parent = 0);
    virtual void paint(QPainter* painter, const QStyleOptionViewItem& option, const QModelIndex& index) const;

};
/******************************************************
 * 自选行情显示的代理
 * MarketSelfDelegate
 ******************************************************/
class MarketSelfDelegate : public QItemDelegate
{
    Q_OBJECT

public:

    MarketSelfDelegate(QObject* parent = 0);
    virtual void paint(QPainter* painter, const QStyleOptionViewItem& option, const QModelIndex& index) const;

};

/********************TableModel********************/
class TableModel : public QAbstractTableModel
{
    Q_OBJECT

public:

    TableModel(QObject *parent = 0);
    ~TableModel(void);
    void setHorizontalHeaderList(QStringList horizontalHeaderList);
    void setVerticalHeaderList(QStringList verticalHeaderList);
    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    int columnCount(const QModelIndex &parent = QModelIndex()) const;
    QVariant data(const QModelIndex &index, int role) const;
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;
    Qt::ItemFlags flags(const QModelIndex &index) const;
    void setModalDatas(QList< QStringList > *rowlist);
    void refrushModel();
	void UpdateData(int i);

signals:

    void updateCount(int count);

private:

    QStringList horizontal_header_list;
    QStringList vertical_header_list;
    QList< QStringList > *arr_row_list;

};

/********************TableView********************/
class TableView : public QTableView
{
    Q_OBJECT

public:

    TableView(QItemDelegate *delegate,QWidget *parent=0);
    ~TableView(void);
    void addRow(QStringList rowList);
    int rowCount();
    void initHeader(QStringList header);

signals:

    void updateCount(int count);

public slots:

    void remove();
    void clear();
	void UpdateRow(int iIndex,QStringList row_list);

public:
	TableModel *model;

private:
    QList< QStringList > grid_data_list;
    QItemDelegate *m_delegate;

};

#endif // TABLEVIEW_H
