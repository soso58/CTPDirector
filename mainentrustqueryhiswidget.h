﻿#ifndef MAINENTRUSTQUERYHISWIDGET_H
#define MAINENTRUSTQUERYHISWIDGET_H

#include <QWidget>
#include <qstandarditemmodel.h>
#include <qabstractitemmodel.h>
#include <string.h>
#include <qtableview.h>
#include <qheaderview.h>
#include <qgridlayout.h>
#include <QHBoxLayout>
#include <QLabel>
#include <QComboBox>
#include <QPushButton>
#include <QCheckBox>
#include <QDateEdit>
#include <QMenu>
#include <QAction>
#include "CtpInterface/ThostFtdcUserApiStruct.h"

class MainEntrustQueryHisWidget : public QWidget
{
    Q_OBJECT
public:
    explicit MainEntrustQueryHisWidget(QWidget *parent = 0);

signals:
    void QueryOrder(QStringList strAttrs);

public slots:
	void QueryButtonClicked();
	void QueryHisStateChanged(int state);
	void RightClickedMenuPop(const QPoint& pos);

public:
    void setupModel();
    void setupViews();
    void ShowOrderDetails(CThostFtdcOrderField *pOrder);
    

public:
    QTableView*             m_tableEntrustQueryHis;
    QAbstractItemModel*     m_modelEntrustQueryHis;
    QComboBox*              m_accountCombox;

private:
    QItemSelectionModel *selectionModel;
    QGridLayout*            MainLayout;
    QHBoxLayout*            hboxlayout;
    QPushButton*            m_querybutton;
    QPushButton*            m_exportbutton;
	QCheckBox*				m_QueryHisCheckbox;
	QHBoxLayout*            m_DateBoxLayout;
	QDateEdit*				m_StartDate;
	QDateEdit*				m_EndDate;
	QLabel*					m_StartLabel;
	QLabel*					m_EndLabel;

	QMenu*					m_RightPopMenu;
	QAction*				m_deleteOrderAction; 
	QAction*				m_updateAction; 	
	QAction*				m_outputAction;
};

#endif // MAINENTRUSTQUERYHISWIDGET_H
