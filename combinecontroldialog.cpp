#include "combinecontroldialog.h"
#include"ui_CombineControlDlg.h"

CombineControlDialog::CombineControlDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CombineControlDlg)
{
    ui->setupUi(this);
    ui->m_CombineType->addItem("套利");
    ui->m_CombineType->addItem("移仓");
    ui->m_SellRadio->setChecked(true);
    ui->m_KaiRadio->setChecked(true);
    ui->m_PriceMargin->setText("-");
    ui->m_LotCnt->setValue(0);

	connect(ui->m_OKButton,SIGNAL(clicked()),this,SLOT(OKButtonClicked()));
}

CombineControlDialog::~CombineControlDialog(){
    delete ui;
}

///选择自定义组合行情
void CombineControlDialog::SelectCombContract(const QModelIndex & oIndex){
    int iRow = oIndex.row();
    //第一腿合约
    QModelIndex oContract = oIndex.sibling(iRow,0);
    QString strFirstLeg = oContract.data().toString();
    ui->m_FirstLeg->setText(strFirstLeg);

    //第二腿合约
    oContract = oIndex.sibling(iRow,1);
    QString strSecondLeg = oContract.data().toString();
    ui->m_SecondLeg->setText(strSecondLeg);

    //最新价
    //QModelIndex oPrice = index.sibling(iRow,1);
    //double i = oPrice.data().toDouble();
    //this->m_SpecifiedPriceSpinBox->setValue(i);
    //手数
    //this->m_HandsSpinBox->setValue(1);
}

void CombineControlDialog::SetAccountName(QString strAccountName){
    ui->m_AccountName->addItem(strAccountName);
}

void CombineControlDialog::OKButtonClicked(){
    /*COMBINORDERINFO typeOrder;

	strcpy_s(typeOrder.AccountID, ui->m_AccountName->currentText().toStdString().c_str());

	if (ui->m_BuyRadio->isChecked()){
		typeOrder.BS = 1;//选中买入
	}else{
		typeOrder.BS = 2;//选中卖出
	}
	typeOrder.EnableStatus= 1;

	if (ui->m_LotCnt->text().toInt() == 0)
	{
		//委托手数不符
		return;
	}else{
		typeOrder.EntrustAmount= ui->m_LotCnt->text().toInt();
	}
	if (ui->m_PriceMargin->text() == "-")//委托价差
	{
		typeOrder.EntrustPrice = 0;
	}else{
		typeOrder.EntrustPrice = ui->m_PriceMargin->text().toDouble();
	}
	
	if (ui->m_TaobaoLeg1->isChecked()){
		typeOrder.Entrusttype	= 2;
		typeOrder.FirstBase		= 2;	
	} 
	else{
		typeOrder.Entrusttype	= 1;
		typeOrder.FirstBase		= 1;	
	}

	if (ui->m_TaobaoLeg2->isChecked()){
		typeOrder.SecondBase    = 2;
	} 
	else{
		typeOrder.SecondBase    = 1;
	}
	


	strcpy_s(typeOrder.FirstContractID, ui->m_FirstLeg->text().toStdString().c_str());
	typeOrder.FristTradeAmount=0;
	typeOrder.iMsgType=2;    //组合单

	if (ui->m_KaiRadio->isChecked()){
		typeOrder.OC = 1;	//选中开仓
	}else if(ui->m_PingRadio->isChecked()){
		typeOrder.OC = 2;	//选中平仓
	}else{
		typeOrder.OC = 4;	//选中平今仓
	}

	strcpy_s(typeOrder.SecondContractID, ui->m_SecondLeg->text().toStdString().c_str());

	typeOrder.SecondTradeAmount=0;
	typeOrder.TradePrice=0;

	typeOrder.Type= ui->m_CombineType->currentIndex()+1;

	strcpy_s(typeOrder.UserID, ui->m_AccountName->currentText().toStdString().c_str());

    emit NewCombineOrder(typeOrder);*/
}
