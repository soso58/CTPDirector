#ifndef MAINPREENTRSUTWIDGET_H
#define MAINPREENTRSUTWIDGET_H

#include <QWidget>
#include <qstandarditemmodel.h>
#include <qabstractitemmodel.h>
#include <string.h>
#include <qtableview.h>
#include <qheaderview.h>
#include <qgridlayout.h>
#include <QHBoxLayout>
#include <QPushButton>
#include <QCheckBox>
#include <QMenu>
#include <QAction>
#include "CtpInterface/ThostFtdcUserApiStruct.h"

class MainPreEntrustWidget : public QWidget
{
    Q_OBJECT
public:
    explicit MainPreEntrustWidget(QWidget *parent = 0);

signals:
	void SigRemoveParkedOrder(CThostFtdcRemoveParkedOrderField* pRemoveParkedOrder);
public slots:
    ///右键弹出菜单响应函数
	void RightClickedMenuPop(const QPoint& pos);
    void RemoveParkedOrder();

    ///全部选中事件绑定
    void SelectButtonClicked();
    ///撤销选中事件绑定
    void DeleteSelectedButtonClicked();
    ///清除选中事件绑定
    void ClearSelectedButtonClicked();
	///显示可撤事件绑定
	void ShowStateChanged(int state);

public:
    void setupModel();
    void setupViews();
    void ShowParkedOrders(CThostFtdcParkedOrderField *pParkedOrder);
	///删除预埋单响应
	void RemoveParkedOrder(CThostFtdcRemoveParkedOrderField *pRemoveParkedOrder);

public:
    QTableView*									m_tablePreEntrust;
    QAbstractItemModel*							m_modelPreEntrust;
	QMap<QString,CThostFtdcParkedOrderField>	m_ParkedOrdersMap;

private:
    QItemSelectionModel*                        m_selectionModel;
    QGridLayout*                                MainLayout;
    QHBoxLayout*                                hboxlayout;

    QMenu*                                      m_RightPopMenu;
    QAction*				                    m_DeleteAction;
    QAction*				                    m_outputAction;

    QPushButton*                                m_SelectAll;
    QPushButton*                                m_DeleteSelected;
    QPushButton*                                m_ClearSelected;
    QCheckBox*                                  m_ShowCheckBox;
};

#endif // MAINPREENTRSUTWIDGET_H
