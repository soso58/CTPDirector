﻿#ifndef UTIL_H
#define UTIL_H

#include <QSettings>
#include <QFontMetrics>
#include "../CommUtil/common.h"

class Util
{

public:

	static bool writeInit(QString path, QString userKey, QString userValue);
	static bool readInit(QString path, QString userKey, QString &userValue);
	static bool updateText(QString text, int maxWidth, QString &elidedText);
	static QString getSkinName();

};

#endif
