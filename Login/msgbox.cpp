#include "msgbox.h"  
#include "utilskin.h"

MsgBox::MsgBox(QWidget *parent)
	: DropShadowWidget(parent)
{
	this->resize(280, 160);

	titleLabel = new QLabel();
	titleIconLabel = new QLabel();
	closeBtn = new PushButton();
	okBtn = new QPushButton();
	cancelBtn = new QPushButton();

	QPixmap title_pixmap(":/img/safe");
	titleIconLabel->setPixmap(title_pixmap);
	titleIconLabel->setFixedSize(16, 16);
	titleIconLabel->setScaledContents(true);
	closeBtn->setPicName(QString(":/sysButton/close"));

	titleLabel->setFixedHeight(30);
	titleLabel->setStyleSheet("color:white;");

	QHBoxLayout *titleLayout = new QHBoxLayout();
	titleLayout->addWidget(titleIconLabel, 0, Qt::AlignVCenter);
	titleLayout->addWidget(titleLabel, 0, Qt::AlignVCenter);
	titleLayout->addStretch();
	titleLayout->addWidget(closeBtn, 0, Qt::AlignTop);
	titleLayout->setSpacing(5);
	titleLayout->setContentsMargins(10, 0, 5, 20);

	//设置提示图片
	msgLabel = new QLabel();
	msgLabel->setFixedSize(45, 45);
	msgLabel->setScaledContents(true);

	//设置提示信息，让QLabel能够自动判断并换行显示：
	askLabel = new QLabel();
	askLabel->setWordWrap(true);

	QHBoxLayout *centerLayout = new QHBoxLayout();
	centerLayout->addWidget(msgLabel);
	centerLayout->addWidget(askLabel);
	centerLayout->setSpacing(10);
	centerLayout->setContentsMargins(15, 0, 15, 0);

	okBtn->setFixedSize(60, 25);
	cancelBtn->setFixedSize(60, 25);
	okBtn->setObjectName("loginGreenButton");
	cancelBtn->setObjectName("loginGreenButton");

	QHBoxLayout *bottomLayout = new QHBoxLayout();
	bottomLayout->addStretch();
	bottomLayout->addWidget(okBtn);
	bottomLayout->addWidget(cancelBtn);
	bottomLayout->setSpacing(10);
	bottomLayout->setContentsMargins(0, 0, 10, 10);

	QVBoxLayout *mainLayout = new QVBoxLayout();
	mainLayout->addLayout(titleLayout);
	mainLayout->addLayout(centerLayout);
	mainLayout->addStretch();
	mainLayout->addLayout(bottomLayout);
	mainLayout->setSpacing(0);
	mainLayout->setContentsMargins(SHADOW_WIDTH, SHADOW_WIDTH, SHADOW_WIDTH, SHADOW_WIDTH);

	setLayout(mainLayout);

	QObject::connect(okBtn, SIGNAL(clicked()), this, SLOT(okOperate()));
	QObject::connect(closeBtn, SIGNAL(clicked()), this, SLOT(cancelOperate()));
	QObject::connect(cancelBtn, SIGNAL(clicked()), this, SLOT(cancelOperate()));

	this->translateLanguage();
}

MsgBox::~MsgBox()
{

}

void MsgBox::translateLanguage()
{
	closeBtn->setToolTip(tr("close"));
	okText = tr("ok");
	cancelText = tr("cancel");
}

void MsgBox::setInfo(QString titleInfo, QString info, QPixmap pixmap, bool isOkHiden)
{
	titleLabel->setText(titleInfo);
	askLabel->setText(info);
	msgLabel->setPixmap(pixmap);

	//是否隐藏确定按钮
	okBtn->setHidden(isOkHiden);
	if(isOkHiden)
	{
		cancelBtn->setText(okText);
	}
	else
	{
		okBtn->setText(okText);
		cancelBtn->setText(cancelText);
	}
}

void MsgBox::paintEvent(QPaintEvent *event)
{
	DropShadowWidget::paintEvent(event);
	QString skinName = Util::getSkinName();

	int height = 35;
	QPainter painter(this);
	painter.setPen(Qt::NoPen);
    painter.setBrush(Qt::white);
	painter.drawPixmap(QRect(SHADOW_WIDTH, SHADOW_WIDTH, this->width()-2*SHADOW_WIDTH, this->height()-2*SHADOW_WIDTH), QPixmap(skinName));
	painter.drawRect(QRect(SHADOW_WIDTH, height, this->width()-2*SHADOW_WIDTH, this->height()-height-SHADOW_WIDTH));
}

void MsgBox::okOperate()
{
	this->accept();
}

void MsgBox::cancelOperate()
{
	this->reject();
}