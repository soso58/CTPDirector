﻿#ifndef MSGBOX_H
#define MSGBOX_H

#include <QDialog>
#include <QLabel>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include "../CommUtil/common.h"
#include "pushbutton.h"
#include "dropshadowwidget.h"

class MsgBox : public DropShadowWidget
{
	Q_OBJECT

public:

	explicit MsgBox(QWidget *parent = 0);
	~MsgBox();
	void translateLanguage();
	void setInfo(QString titleInfo, QString info, QPixmap pixmap, bool isOkHiden);

signals:

	void okMessageHidden(bool isChecked);
	void msgChecked(bool isChecked, bool isOk);
	
private slots:

	void okOperate();
	void cancelOperate();

protected:

	void paintEvent(QPaintEvent *event);

private:

	QLabel *titleLabel;
	QLabel *titleIconLabel;
	QLabel *msgLabel; //提示图片
	QLabel *askLabel; //提示信息
	PushButton *closeBtn; //提示框上的关闭按钮
	QPushButton *okBtn; //下载按钮
	QPushButton *cancelBtn; //取消按钮

	QString okText;
	QString cancelText;

};

#endif  //MSGBOX_H
