#include "utilskin.h"

bool Util::writeInit(QString path, QString userKey, QString userValue)
{
	if(path.isEmpty() || userKey.isEmpty())
	{
		return false;
	}
	else
	{
		//创建配置文件操作对象
		QSettings *config = new QSettings(path, QSettings::IniFormat);

		//将信息写入配置文件
		config->beginGroup("config");
		config->setValue(userKey, userValue);
		config->endGroup();

		return true;
	} 
}

bool Util::readInit(QString path, QString userKey, QString &userValue)
{
	userValue = QString("");
	if(path.isEmpty() || userKey.isEmpty())
	{
		return false;
	}
	else
	{
		//创建配置文件操作对象
		QSettings *config = new QSettings(path, QSettings::IniFormat);

		//读取用户配置信息
		userValue = config->value(QString("config/") + userKey).toString();

		return true;
	}  
}

bool Util::updateText(QString text, int maxWidth, QString &elidedText)
{
	elidedText = QString("");
	if(text.isEmpty() || maxWidth <= 0)
	{
		return false;
	}

	QFont ft;
	QFontMetrics fm(ft);
	elidedText = fm.elidedText(text, Qt::ElideRight, maxWidth);

	return true;
}

QString Util::getSkinName()
{
	QString skinName = DEFAULT_SKIN;
	bool isRead = Util::readInit(QString("./user.ini"), QString("skin"), skinName);

	return skinName;
}