#include "accountitem.h"

AccountItem::AccountItem(QWidget *parent)
: QWidget(parent)
{
	mousePress = false;
	accountNum = new QLabel();
	deleteBtn = new QPushButton();

	QPixmap pixmap(":/loginDialog/delete");
	deleteBtn->setIcon(pixmap);
	deleteBtn->setIconSize(pixmap.size());
	deleteBtn->setStyleSheet("background:transparent;");
	connect(deleteBtn, SIGNAL(clicked()), this, SLOT(removeAccount()));

	QHBoxLayout *main_layout = new QHBoxLayout();
	main_layout->addWidget(accountNum);
	main_layout->addStretch();
	main_layout->addWidget(deleteBtn);
	main_layout->setContentsMargins(5, 5, 5, 5);
	main_layout->setSpacing(5);
	this->setLayout(main_layout);
}

AccountItem::~AccountItem()
{

}

void AccountItem::setAccountNumber(QString account_text)
{
	accountNum->setText(account_text);
}

QString AccountItem::getAccountNumber()
{
	QString accountNum_text = accountNum->text();

	return accountNum_text;
}

void AccountItem::removeAccount()
{
	QString accountNum_text = accountNum->text();
	emit removeAccount(accountNum_text);
}

void AccountItem::mousePressEvent(QMouseEvent *event)
{
	//ֻ����������
	if(event->button() == Qt::LeftButton) 
	{
		mousePress = true;
	}
}

void AccountItem::mouseReleaseEvent(QMouseEvent *event)
{
	if(mousePress && (this->rect()).contains(event->pos()))
	{
		emit showAccount(accountNum->text());
	}
	mousePress = false;
}