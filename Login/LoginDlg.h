#ifndef LOGINDLG_H
#define LOGINDLG_H

#include <QDialog>
#include <QMessageBox>
#include <QWidgetList>
#include <QListWidgetItem>
#include <QComboBox>
#include <QGroupBox>
#include <QLineEdit>
#include <QCheckBox>
#include <QLabel>
#include <QPushButton>
#include <QSettings>
#include "accountitem.h"
#include "msgbox.h"
#include "../CtpInterface/ThostFtdcMdApi.h"
#include "../CtpInterface/ThostFtdcTraderApi.h"
#include "../CtpInterface/ThostFtdcMdSpiImpl.h"
#include "../CtpInterface/ThostFtdcTraderSpiImpl.h"
#include "../mainwindow.h"

struct AccountInfo{
	QString AccountID;
	QString Password;
    QString BrokerID;
};


class CLoginDlg : public QDialog
{
	Q_OBJECT

public:
	//Ui::LoginDlg ui;
	QGroupBox *loginGroup;
	QLabel *labelServerSel;
	QLabel *labelUser;

    QLabel *labelStatus;

	QLabel *labelPassword;
	QLineEdit *editPassword;
	QCheckBox *checkSaveFlag;
	QPushButton *btnLogin;
	QPushButton *btnCancel;
	QComboBox *cmbServerList;
	QLineEdit *editBrokerId;
	QLabel *labelBrokerId;
	QComboBox *cmbUsername;
	QListWidget *listWidget;

	MsgBox *msgBox; //删除用户提示界面
public slots:
	void UserNameChanged(const QString &);

    void showAccount(QString sText);
    void removeAccount(QString sText);
	//CTP服务器反馈
    void OnRspUserLogin(CThostFtdcRspInfoField *pRspInfo);

    void ShowStatusText(QString text);

signals:
    void SigShowStatusText(QString text);

public:
	CLoginDlg(QWidget *parent = 0);
	~CLoginDlg();

	void SaveAccountInfo();
    void ReadIniFile();
	void userLogin(void);
	void cancelLogin();
	void displayUser(QString account);
    void removeUser(QString account);

    void OnRspQryInstrument();
private:
	vector<AccountInfo>  m_vAccountInfo;
    vector<QString>      m_vAccountHis;


	//通讯服务器反馈信息
	int  m_ResponsedType;
	bool m_bResponsed;
	QString m_strErrorMsg;
	//CTP服务器反馈信息
	bool m_bCTPResponsed;
	bool m_iErrorID;
	QString m_strCTPErrorMsg;

	bool			m_dbOk;
	//通讯服务器的用户名和密码
    QString			m_szUsername;
    QString			m_szPassword;
    QString			m_szBrokerId;

    QSettings*		m_AccountSettings;
    QSettings*		m_ServerSettings;
};

#endif //LOGINDLG_H
