#ifndef CONDITIONDIALOG_H
#define CONDITIONDIALOG_H

#include <QDialog>
#include "CommUtil/common.h"

namespace Ui {
class ConditionDialog;
}

class conditionDialog : public QDialog
{
    Q_OBJECT
public:
    explicit conditionDialog(QWidget *parent = 0);
    ~conditionDialog();

    void SetPrice(double dprice);

signals:
    void SigSendCondition(ConditionType oCondition);

public slots:
    void OKButtonClicked();
    void CancelButtonClicked();

private:
    Ui::ConditionDialog*    ui;
};

#endif // CONDITIONDIALOG_H
