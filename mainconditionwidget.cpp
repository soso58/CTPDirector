﻿#include "mainconditionwidget.h"

MainConditionWidget::MainConditionWidget(QWidget *parent) :
    QWidget(parent)
{
    setupModel();
    setupViews();
}

void MainConditionWidget::setupModel()
{
    m_modelCondition = new QStandardItemModel(0, 12, this);
    m_modelCondition->setHeaderData(0, Qt::Horizontal, QStringLiteral("条件单号"));
    m_modelCondition->setHeaderData(1, Qt::Horizontal, QStringLiteral("资金账号"));
    m_modelCondition->setHeaderData(2, Qt::Horizontal, QStringLiteral("合约"));
    m_modelCondition->setHeaderData(3, Qt::Horizontal, QStringLiteral("买卖"));
    m_modelCondition->setHeaderData(4, Qt::Horizontal, QStringLiteral("开平"));
    m_modelCondition->setHeaderData(5, Qt::Horizontal, QStringLiteral("委托价格"));
    m_modelCondition->setHeaderData(6, Qt::Horizontal, QStringLiteral("委手"));  //b/s
    m_modelCondition->setHeaderData(7, Qt::Horizontal, QStringLiteral("触发条件"));
    m_modelCondition->setHeaderData(8, Qt::Horizontal, QStringLiteral("下单日期"));
    m_modelCondition->setHeaderData(9, Qt::Horizontal, QStringLiteral("状态"));
    m_modelCondition->setHeaderData(10, Qt::Horizontal, QStringLiteral("保存期限"));
    m_modelCondition->setHeaderData(11, Qt::Horizontal, QStringLiteral("订单类型"));
}

void MainConditionWidget::setupViews()
{
    hboxlayout = new QHBoxLayout;
    QPushButton *button1 = new QPushButton(QStringLiteral("全部激活"));
    QPushButton *button2 = new QPushButton(QStringLiteral("全部停止"));
    QPushButton *button3 = new QPushButton(QStringLiteral("全部撤销"));
    QCheckBox *Checkbox = new QCheckBox(QStringLiteral("显示可发"));
    hboxlayout->addWidget(button1);
    hboxlayout->addWidget(button2);
    hboxlayout->addWidget(button3);
    hboxlayout->addStretch();
    hboxlayout->addStretch();
    hboxlayout->addWidget(Checkbox);

    MainLayout = new QGridLayout(this);

    m_tableCondition = new QTableView;
    m_tableCondition->setAlternatingRowColors(true);
    QFont font = m_tableCondition->horizontalHeader()->font();
    font.setBold(true);
    m_tableCondition->horizontalHeader()->setFont(font);
//    tableCondition->setStyleSheet("QTableView::item:selected { selection-color: rgb(0, 0, 0) }" "QTableView::item:selected { background-color: rgb(255, 255, 0) }"
//                "QTableView{background-color: rgb(0, 0, 0);" "alternate-background-color: rgb(41, 36, 33);}");

    m_tableCondition->setModel(m_modelCondition);
    m_tableCondition->setEditTriggers(QAbstractItemView::NoEditTriggers);
    m_tableCondition->verticalHeader()->setVisible(false); //隐藏列表头
    m_tableCondition->verticalHeader()->setFixedWidth(40);
    m_tableCondition->setSelectionBehavior(QAbstractItemView::SelectRows);
    m_tableCondition->setSelectionMode(QAbstractItemView::SingleSelection);

    m_tableCondition->setColumnWidth(0, 80);
    m_tableCondition->setColumnWidth(1, 80);
    m_tableCondition->setColumnWidth(2, 80);
    m_tableCondition->setColumnWidth(3, 80);
    m_tableCondition->setColumnWidth(4, 80);
    m_tableCondition->setColumnWidth(5, 80);
    m_tableCondition->setColumnWidth(6, 80);
    m_tableCondition->setColumnWidth(7, 150);
    m_tableCondition->setColumnWidth(8, 80);
    m_tableCondition->setColumnWidth(9, 80);
    m_tableCondition->setColumnWidth(10, 80);
    m_tableCondition->setColumnWidth(11, 80);

    MainLayout->addWidget(m_tableCondition,0,0,1,6);
    //MainLayout->addLayout(hboxlayout,1,0,1,6);
    MainLayout->setMargin(20);

	//创建菜单、菜单项
	this->m_tableCondition->setContextMenuPolicy(Qt::CustomContextMenu);
	m_RightPopMenu = new QMenu(this->m_tableCondition);
	QAction* EnableAction = new QAction(QStringLiteral("激活订单"),this); 
	QAction* ModifyAction = new QAction(QStringLiteral("修改订单"),this); 
	QAction* DeleteAction = new QAction(QStringLiteral("撤销订单"),this); 
	QAction* DisableAction = new QAction(QStringLiteral("停止激活"),this); 
	QAction* outputAction = new QAction(QStringLiteral("导出"),this); 
	m_RightPopMenu->addAction(EnableAction);
	m_RightPopMenu->addAction(ModifyAction);
	m_RightPopMenu->addAction(DeleteAction);
	m_RightPopMenu->addAction(DisableAction);
	m_RightPopMenu->addAction(outputAction);
    //右键弹出菜单事件绑定
	//connect(this->m_tableCondition,SIGNAL(customContextMenuRequested(const QPoint&)),this,SLOT(RightClickedMenuPop(const QPoint&)));
}
///右键弹出菜单响应函数
void MainConditionWidget::RightClickedMenuPop(const QPoint& pos)
{
	m_RightPopMenu->exec(QCursor::pos());
}
void MainConditionWidget::ShowConditionOrder(CThostFtdcOrderField *pOrder){
	QString strCondition = "";//触发条件
	///触发条件
	switch (pOrder->ContingentCondition)
	{
	//最新价
	case THOST_FTDC_CC_LastPriceGreaterThanStopPrice:
        strCondition = QStringLiteral("最新价>") + QString::number(pOrder->StopPrice,'f');
		break;
	case THOST_FTDC_CC_LastPriceGreaterEqualStopPrice:
        strCondition = QStringLiteral("最新价>=") + QString::number(pOrder->StopPrice,'f');
		break;
	case THOST_FTDC_CC_LastPriceLesserThanStopPrice:
        strCondition = QStringLiteral("最新价<") + QString::number(pOrder->StopPrice,'f');
		break;
	case THOST_FTDC_CC_LastPriceLesserEqualStopPrice:
        strCondition = QStringLiteral("最新价<=") + QString::number(pOrder->StopPrice,'f');
		break;
	//买价
	case THOST_FTDC_CC_BidPriceGreaterThanStopPrice:
        strCondition = QStringLiteral("买价>") + QString::number(pOrder->StopPrice,'f');
		break;
	case THOST_FTDC_CC_BidPriceGreaterEqualStopPrice:
        strCondition = QStringLiteral("买价>=") + QString::number(pOrder->StopPrice,'f');
		break;
	case THOST_FTDC_CC_BidPriceLesserThanStopPrice:
        strCondition = QStringLiteral("买价<") + QString::number(pOrder->StopPrice,'f');
		break;
	case THOST_FTDC_CC_BidPriceLesserEqualStopPrice:
        strCondition = QStringLiteral("买价<=") + QString::number(pOrder->StopPrice,'f');
		break;
	//卖价
	case THOST_FTDC_CC_AskPriceGreaterThanStopPrice:
        strCondition = QStringLiteral("卖价>") + QString::number(pOrder->StopPrice,'f');
		break;
	case THOST_FTDC_CC_AskPriceGreaterEqualStopPrice:
        strCondition = QStringLiteral("卖价>=") + QString::number(pOrder->StopPrice,'f');
		break;
	case THOST_FTDC_CC_AskPriceLesserThanStopPrice:
        strCondition = QStringLiteral("卖价<") + QString::number(pOrder->StopPrice,'f');
		break;
	case THOST_FTDC_CC_AskPriceLesserEqualStopPrice:
        strCondition = QStringLiteral("卖价<=") + QString::number(pOrder->StopPrice,'f');
		break;
	}

	int iRow = m_modelCondition->rowCount();
	///条件单号
	((QStandardItemModel*) m_modelCondition)->setItem(iRow, 0, new QStandardItem(pOrder->OrderLocalID));
	///资金帐号
	((QStandardItemModel*) m_modelCondition)->setItem(iRow, 1, new QStandardItem(pOrder->UserID));
	//合约
	((QStandardItemModel*) m_modelCondition)->setItem(iRow, 2, new QStandardItem(pOrder->InstrumentID));

	///买卖方向
	if (pOrder->Direction == THOST_FTDC_D_Buy){
		((QStandardItemModel*) m_modelCondition)->setItem(iRow, 3, new QStandardItem(QStringLiteral("买入")));
	} else if(pOrder->Direction == THOST_FTDC_D_Sell){
		((QStandardItemModel*) m_modelCondition)->setItem(iRow, 3, new QStandardItem(QStringLiteral("卖出")));
	}
	//开平
	if (pOrder->CombOffsetFlag[0] == '0'){//开仓
		((QStandardItemModel*) m_modelCondition)->setItem(iRow, 4, new QStandardItem(QStringLiteral("开仓")));
	} 
	else{//平仓
		((QStandardItemModel*) m_modelCondition)->setItem(iRow, 4, new QStandardItem(QStringLiteral("平仓")));
	}

	//委托价格
    ((QStandardItemModel*) m_modelCondition)->setItem(iRow, 5, new QStandardItem(QString::number(pOrder->StopPrice)));
	//委托数量
	((QStandardItemModel*) m_modelCondition)->setItem(iRow, 6, new QStandardItem(QString::number(pOrder->VolumeTotalOriginal)));

	//触发条件
	((QStandardItemModel*) m_modelCondition)->setItem(iRow, 7, new QStandardItem(strCondition));
	//下单日期
	((QStandardItemModel*) m_modelCondition)->setItem(iRow, 8, new QStandardItem(pOrder->TradingDay));
	//条件单 状态
	//((QStandardItemModel*) m_modelCondition)->setItem(iRow, 8, new QStandardItem(QString::fromLocal8Bit(pOrder->StatusMsg)));
	if (pOrder->OrderStatus == THOST_FTDC_OST_Unknown){
		((QStandardItemModel*) m_modelCondition)->setItem(iRow, 9, new QStandardItem(QStringLiteral("状态未知")));
	}else if (pOrder->OrderStatus == THOST_FTDC_OST_NotTouched)	{
		((QStandardItemModel*) m_modelCondition)->setItem(iRow, 9, new QStandardItem(QStringLiteral("尚未触发")));
	}else if(pOrder->OrderStatus == THOST_FTDC_OST_Touched)	{
		((QStandardItemModel*) m_modelCondition)->setItem(iRow, 9, new QStandardItem(QStringLiteral("已触发")));
	}
	//保存期限
	//((QStandardItemModel*) m_modelCondition)->setItem(iRow, 10, new QStandardItem(pCondition->SaveKind));
	//套保标识
	if (pOrder->CombHedgeFlag[0] == THOST_FTDC_HF_Speculation){
		((QStandardItemModel*) m_modelCondition)->setItem(iRow, 11, new QStandardItem(QStringLiteral("投机")));
	}else if(pOrder->CombHedgeFlag[0] == THOST_FTDC_HF_Arbitrage){
		((QStandardItemModel*) m_modelCondition)->setItem(iRow, 11, new QStandardItem(QStringLiteral("套利")));
	}else if(pOrder->CombHedgeFlag[0] == THOST_FTDC_HF_Hedge){
		((QStandardItemModel*) m_modelCondition)->setItem(iRow, 11, new QStandardItem(QStringLiteral("套保")));
	}
}
/*void MainConditionWidget::ShowConditionOrder(CThostFtdcInputOrderField* pOrder,ConditionType* pCondition){
    QString strCondition = pCondition->PriceKind + pCondition->Symbol + QString::number(pCondition->Price);

	int iRow = m_modelCondition->rowCount();
	///条件单号
	((QStandardItemModel*) m_modelCondition)->setItem(iRow, 0, new QStandardItem(QString::number(m_ConditionOrderNO++)));
	///资金帐号
	((QStandardItemModel*) m_modelCondition)->setItem(iRow, 1, new QStandardItem(pOrder->UserID));
	//合约
	((QStandardItemModel*) m_modelCondition)->setItem(iRow, 2, new QStandardItem(pOrder->InstrumentID));

	///买卖方向
	if (pOrder->Direction == THOST_FTDC_D_Buy){
		((QStandardItemModel*) m_modelCondition)->setItem(iRow, 3, new QStandardItem(QStringLiteral("买入")));
	} else if(pOrder->Direction == THOST_FTDC_D_Sell){
		((QStandardItemModel*) m_modelCondition)->setItem(iRow, 3, new QStandardItem(QStringLiteral("卖出")));
	}
	//开平
	if (pOrder->CombOffsetFlag[0] == '0'){//开仓
		((QStandardItemModel*) m_modelCondition)->setItem(iRow, 4, new QStandardItem(QStringLiteral("开仓")));
	} 
	else{//平仓
		((QStandardItemModel*) m_modelCondition)->setItem(iRow, 4, new QStandardItem(QStringLiteral("平仓")));
	}

	//委托价格
	((QStandardItemModel*) m_modelCondition)->setItem(iRow, 5, new QStandardItem(QString::number(pOrder->LimitPrice)));
	//委托数量
	((QStandardItemModel*) m_modelCondition)->setItem(iRow, 6, new QStandardItem(QString::number(pOrder->VolumeTotalOriginal)));

	//触发条件
	((QStandardItemModel*) m_modelCondition)->setItem(iRow, 7, new QStandardItem(strCondition));

	//状态
	//((QStandardItemModel*) m_modelOrder)->setItem(iIndex, 8, new QStandardItem(QString::fromLocal8Bit(pOrder->StatusMsg)));
	//保存期限
	((QStandardItemModel*) m_modelCondition)->setItem(iRow, 9, new QStandardItem(pCondition->SaveKind));
	//套保标识
	if (pOrder->CombHedgeFlag[0] == THOST_FTDC_HF_Speculation){
		((QStandardItemModel*) m_modelCondition)->setItem(iRow, 10, new QStandardItem(QStringLiteral("投机")));
	}else if(pOrder->CombHedgeFlag[0] == THOST_FTDC_HF_Arbitrage){
		((QStandardItemModel*) m_modelCondition)->setItem(iRow, 10, new QStandardItem(QStringLiteral("套利")));
	}else if(pOrder->CombHedgeFlag[0] == THOST_FTDC_HF_Hedge){
		((QStandardItemModel*) m_modelCondition)->setItem(iRow, 10, new QStandardItem(QStringLiteral("套保")));
    }

}*/
