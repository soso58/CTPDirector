#include "mainwindow.h"
#include "login/logindlg.h"
#include <QApplication>
#include <QTextCodec>
#include <QFile>

MainWindow* g_pMainwindow;
CLoginDlg*  g_pLoginDlg;

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    //加载QSS样式表
    QFile qss(":/TraderQss");
    qss.open(QFile::ReadOnly);
    qApp->setStyleSheet(qss.readAll());
    qss.close();

    g_pMainwindow = new MainWindow;
    g_pLoginDlg   = new CLoginDlg;
	//登录
	if(g_pLoginDlg->exec() == 1){
		delete g_pLoginDlg;
		g_pLoginDlg = NULL;

		g_pMainwindow->resize(1000,650);
		g_pMainwindow->show();
		return app.exec();
	}else{
		delete g_pMainwindow;
		g_pMainwindow = NULL;
	}
	return 0;
}
