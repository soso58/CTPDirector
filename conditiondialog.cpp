﻿#include "conditiondialog.h"
#include "ui_ConditionDialog.h"

conditionDialog::conditionDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ConditionDialog)
{
    ui->setupUi(this);
    this->setWindowTitle(QStringLiteral("设置触发条件"));

    ui->m_PriceKindComboBox->addItem(QStringLiteral("最新价"));
    ui->m_PriceKindComboBox->addItem(QStringLiteral("买价"));
    ui->m_PriceKindComboBox->addItem(QStringLiteral("卖价"));
    ui->m_SymbolComboBox->addItem(">");
    ui->m_SymbolComboBox->addItem(">=");
    ui->m_SymbolComboBox->addItem("<");
    ui->m_SymbolComboBox->addItem("<=");
    ui->m_SaveComboBox->addItem(QStringLiteral("一天"));
    ui->m_SaveComboBox->addItem(QStringLiteral("不保存"));
    ui->m_PriceDoubleSpinBox->setRange(0,100000);

    connect(ui->m_OKButton,SIGNAL(clicked()),this,SLOT(OKButtonClicked()));
    connect(ui->m_CancelButton,SIGNAL(clicked()),this,SLOT(CancelButtonClicked()));
}

conditionDialog::~conditionDialog()
{
    delete ui;
}

void conditionDialog::SetPrice(double dprice){
    //价格
    ui->m_PriceDoubleSpinBox->setValue(dprice);
}
void conditionDialog::OKButtonClicked(){
    ConditionType oCondition;

    oCondition.PriceKind = ui->m_PriceKindComboBox->currentText();
    oCondition.Symbol    = ui->m_SymbolComboBox->currentText();
    oCondition.Price     = ui->m_PriceDoubleSpinBox->text().toDouble();
    oCondition.SaveKind  = ui->m_SaveComboBox->currentText();

    emit SigSendCondition(oCondition);
    this->accept();
}
void conditionDialog::CancelButtonClicked(){
    this->close();
}
