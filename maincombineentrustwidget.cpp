﻿#include "maincombineentrustwidget.h"

MainCombineEntrustWidget::MainCombineEntrustWidget(QWidget *parent) :
    QWidget(parent)
{
    setupViews();
}

void MainCombineEntrustWidget::setupViews()
{
	m_treeWidget = new QTreeWidget;
	m_treeWidget->setAlternatingRowColors(true);

	//设定头项名称
	m_treeWidget->headerItem()->setText(0,QStringLiteral("组合单号"));
	m_treeWidget->headerItem()->setText(1,QStringLiteral("资金账号"));
	m_treeWidget->headerItem()->setText(2,QStringLiteral("套利标识"));
	m_treeWidget->headerItem()->setText(3,QStringLiteral("第一腿"));
	m_treeWidget->headerItem()->setText(4,QStringLiteral("第二腿"));
	m_treeWidget->headerItem()->setText(5,QStringLiteral("买卖"));
	m_treeWidget->headerItem()->setText(6,QStringLiteral("开平"));
	m_treeWidget->headerItem()->setText(7,QStringLiteral("委手"));
	m_treeWidget->headerItem()->setText(8,QStringLiteral("价差"));
	m_treeWidget->headerItem()->setText(9,QStringLiteral("套保标识"));
	m_treeWidget->headerItem()->setText(10,QStringLiteral("状态"));
	m_treeWidget->headerItem()->setText(11,QStringLiteral("运行状态"));
	m_treeWidget->headerItem()->setText(12,QStringLiteral("委托时间"));
	m_treeWidget->headerItem()->setText(13,QStringLiteral("发送一"));
	m_treeWidget->headerItem()->setText(14,QStringLiteral("发送二"));
	m_treeWidget->headerItem()->setText(15,QStringLiteral("成交一"));
	m_treeWidget->headerItem()->setText(16,QStringLiteral("成交二"));
	m_treeWidget->headerItem()->setText(17,QStringLiteral("二腿套保标识"));

	m_treeWidget->header()->setSectionsClickable(true);
	m_treeWidget->header()->setSectionsMovable(true);
	QFont font = m_treeWidget->header()->font();
	font.setBold(true);
	m_treeWidget->header()->setFont(font);

	m_treeWidget->setColumnCount(18);
	m_treeWidget->setColumnWidth(0,100);
	m_treeWidget->setColumnWidth(1,100);
	m_treeWidget->setColumnWidth(2,100);
	m_treeWidget->setColumnWidth(3,120);
	m_treeWidget->setColumnWidth(4,100);
	m_treeWidget->setColumnWidth(5,100);
	m_treeWidget->setColumnWidth(6,100);
	m_treeWidget->setColumnWidth(7,100);
	m_treeWidget->setColumnWidth(8,100);
	m_treeWidget->setColumnWidth(9,100);
	m_treeWidget->setColumnWidth(10,100);
	m_treeWidget->setColumnWidth(11,100);
	m_treeWidget->setColumnWidth(12,100);
	m_treeWidget->setColumnWidth(13,100);
	m_treeWidget->setColumnWidth(14,100);
	m_treeWidget->setColumnWidth(15,100);
	m_treeWidget->setColumnWidth(16,100);
	m_treeWidget->setColumnWidth(17,100);

    MainLayout = new QGridLayout(this);
    MainLayout->addWidget(m_treeWidget,0,0);
    MainLayout->setMargin(20);
}
/************************************************************************/
/* 显示成交回报                                                         */
/************************************************************************/
void MainCombineEntrustWidget::ShowCombineOrder(QString strBatchNo, CThostFtdcTradeField *pTrade){
	int iTopCnt = m_treeWidget->topLevelItemCount();
	for(int i = 0; i < iTopCnt; i ++){
		QTreeWidgetItem* Temp = m_treeWidget->topLevelItem(i);
		QString BatchNo    = Temp->text(0);
		if(strBatchNo == BatchNo){//判断批量单号是否一致
			int iChildCnt = Temp->childCount();
			for(int j = 0; j< iChildCnt; j++){
				QTreeWidgetItem* child = Temp->child(j);
				QString strOrderRef    = child->text(1);
				if(QString(pTrade->OrderRef) == strOrderRef ){//判断OrderRef是否一致
					child->setText(6,QString::number(pTrade->Price,'f'));
					return;
				}
			}
		}
	}
}
/************************************************************************/
/* 显示报单通知                                                         */
/************************************************************************/
void MainCombineEntrustWidget::ShowCombineOrder(QString strBatchNo, CThostFtdcOrderField *pOrder){

	int iTopCnt = m_treeWidget->topLevelItemCount();
	for(int i = 0; i < iTopCnt; i ++){
		QTreeWidgetItem* Temp = m_treeWidget->topLevelItem(i);
		QString BatchNo    = Temp->text(0);
		if(strBatchNo == BatchNo){//判断批量单号是否一致
			int iChildCnt = Temp->childCount();
			for(int j = 0; j< iChildCnt; j++){
				QTreeWidgetItem* child = Temp->child(j);
				QString strOrderRef    = child->text(1);
				if(QString(pOrder->OrderRef) == strOrderRef ){//判断OrderRef是否一致
					child->setText(9,QString::fromLocal8Bit(pOrder->StatusMsg));
					return;
				}
			}
			QStringList columItemList;
			QTreeWidgetItem *child;
			columItemList<<QString(pOrder->UserID)<<QString(pOrder->OrderRef)<<QString(pOrder->OrderLocalID)<<QString(pOrder->InstrumentID);

			///买卖方向
			columItemList<<ComFunc::GetBSName(pOrder->Direction);
			//开平
			columItemList<<ComFunc::GetOCName(pOrder->CombOffsetFlag[0]);
			//委托价格
			columItemList<<QString::number(pOrder->LimitPrice);
			//委托数量
			columItemList<<QString::number(pOrder->VolumeTotalOriginal);
			//成交数量
			columItemList<<QString::number(pOrder->VolumeTraded);
			//状态
			columItemList<<QString::fromLocal8Bit(pOrder->StatusMsg);
			//委托时间
			columItemList<<QString(pOrder->TradingDay);
			//套保标识
			columItemList<<ComFunc::GetHedgeType(pOrder->CombHedgeFlag[0]);

			child = new QTreeWidgetItem(columItemList);
			Temp->addChild(child);
		}
	}
}
/************************************************************************/
/* 显示组合单信息                                                       */
/* 从通讯服务器返回的组合单信息											*/
/************************************************************************/
void MainCombineEntrustWidget::ShowCombineOrder(int icnt, void* pData){
    /*COMBINORDERINFO* pOrderInfo = (COMBINORDERINFO*)pData;

	for(int i = 0; i < icnt; i++)
	{//--存储到队列中--
		QString strKey  = QString::number(pOrderInfo[i].OrderID);

		STCombineInfo stCombine;
		stCombine.dDownPrice1=0;
		stCombine.dDownPrice2=0;
		stCombine.dUpPrice1=0;
		stCombine.dUpPrice2=0;
		stCombine.var0_CombineNumber= QString::number(pOrderInfo[i].OrderID); 
		stCombine.var10_sStatus= ComFunc::GetCofcoStatusName(pOrderInfo[i].EnableStatus); 
		stCombine.SendHand1= 0;
		stCombine.SendHand2= 0;
		stCombine.BusHand1= 0;
		stCombine.BusHand2= 0;
		stCombine.dBussPrice =0;
		stCombine.var15_sEntrustTime= QString::number(pOrderInfo[i].EntrustTime); 
		stCombine.var16_sEnableStatus= "开启";
		stCombine.var1_szAccount= QString(pOrderInfo[i].AccountID); 
		stCombine.var2_sLeg1= QString(pOrderInfo[i].FirstContractID); 
		stCombine.var3_sLeg2= QString(pOrderInfo[i].SecondContractID);
		stCombine.var4_sOrderType= ComFunc::GetCofcoOrderType(pOrderInfo[i].Type +1);
		stCombine.var5_sBsName= ComFunc::GetBSName( pOrderInfo[i].BS);
		stCombine.var6_sOcName= ComFunc::GetOCName( pOrderInfo[i].OC);
		stCombine.TotalHand=  pOrderInfo[i].EntrustAmount;
		stCombine.var8_Price= pOrderInfo[i].EntrustPrice;
		stCombine.var9_sHedge1= ComFunc::GetHedgeType(pOrderInfo[i].FirstBase -1);
		stCombine.var9_sHedge2= ComFunc::GetHedgeType(pOrderInfo[i].SecondBase -1);

		stCombine.bRspBusinessLeg1= TRUE;
		stCombine.bRspEntrustLeg1= TRUE;
		stCombine.bRspOrderLeg1=TRUE;
		stCombine.bRspBusinessLeg2= TRUE;
		stCombine.bRspEntrustLeg2= TRUE;
		stCombine.bRspOrderLeg2=TRUE;
		stCombine.iLastEntrustTime= 0;

		m_MapCombineOrder[strKey]=stCombine;

		//显示在控件上	
		QStringList Items;
		Items<<stCombine.var0_CombineNumber<<stCombine.var1_szAccount<<stCombine.var4_sOrderType<<stCombine.var2_sLeg1<<stCombine.var3_sLeg2;
		QTreeWidgetItem* A = new QTreeWidgetItem(Items);
		m_treeWidget->addTopLevelItem(A);
    }*/
}
