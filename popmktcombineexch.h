#ifndef POPMKTCOMBINEEXCH_H
#define POPMKTCOMBINEEXCH_H

#include <QWidget>
#include <qstandarditemmodel.h>
#include <qabstractitemmodel.h>
#include <string.h>
#include <qtableview.h>
#include <qheaderview.h>
#include <qgridlayout.h>
#include <QLabel>
#include <QComboBox>
#include<QLineEdit>
#include<QPushButton>
#include<Qspinbox>
#include<QMenu>
#include <QAction>

class PopMktCombineExch : public QWidget
{
    Q_OBJECT
public:
    explicit PopMktCombineExch(QWidget *parent = 0);

signals:

public slots:
	void RightClickedMenuPop(const QPoint& pos);
public:
    void setupModel();
    void setupViews();

public:
    QTableView* tablePopMktCombineExch;
    QAbstractItemModel *modelPopMktCombineExch;

private:
    QItemSelectionModel *selectionModel;
    QGridLayout *MainLayout;
	QMenu*					m_RightPopMenu;
	QAction*				m_deleteOrderAction; 
	QAction*				m_updateAction; 	
	QAction*				m_outputAction;
};

#endif // POPMKTCOMBINEEXCH_H
