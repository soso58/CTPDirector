#include "popmktcombineexchdialog.h"
#include "ui_popmktcombineexchdialog.h"
#include "mainwindow.h"
#include<QMessageBox>

PopMktCombineExchDialog::PopMktCombineExchDialog(void* pMainwindow,QWidget *parent) :
    QDialog(parent),
    ui(new Ui::PopMktCombineExchDialog)
{
    ui->setupUi(this);
    //第一腿交易所
    ui->ComboBox_FirstExchange->addItem("郑州交易所");
    ui->ComboBox_FirstExchange->addItem("大连交易所");
    ui->ComboBox_FirstExchange->addItem("上海交易所");
    ui->ComboBox_FirstExchange->addItem("金融交易所");
	ui->ComboBox_FirstExchange->addItem("显示全部");
    //第二腿交易所
    ui->ComboBox_SecondExchange->addItem("郑州交易所");
    ui->ComboBox_SecondExchange->addItem("大连交易所");
    ui->ComboBox_SecondExchange->addItem("上海交易所");
    ui->ComboBox_SecondExchange->addItem("金融交易所");
	ui->ComboBox_SecondExchange->addItem("显示全部");

    ui->tableWidget_first->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->tableWidget_first->verticalHeader()->setVisible(false);
    ui->tableWidget_first->setShowGrid(true);//显示表格线
    ui->tableWidget_first->setSelectionMode ( QAbstractItemView::SingleSelection); //设置选择模式，选择单行
    ui->tableWidget_first->setEditTriggers(QAbstractItemView::NoEditTriggers);//禁止编辑
    //ui->tableWidget_first->horizontalHeader()->setStretchLastSection(true);//该命令只是将最后一行的列宽度等于Table剩下的宽度

    ui->tableWidget_second->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->tableWidget_second->verticalHeader()->setVisible(false);
    ui->tableWidget_second->setShowGrid(true);//显示表格线
    ui->tableWidget_second->setSelectionMode ( QAbstractItemView::SingleSelection); //设置选择模式，选择单行
    ui->tableWidget_second->setEditTriggers(QAbstractItemView::NoEditTriggers);//禁止编辑
    //ui->tableWidget_second->horizontalHeader()->setStretchLastSection(true);//该命令只是将最后一行的列宽度等于Table剩下的宽度

    ui->tableWidget_selected->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->tableWidget_selected->verticalHeader()->setVisible(false);
    ui->tableWidget_selected->setShowGrid(true);//显示表格线
    ui->tableWidget_selected->setSelectionMode ( QAbstractItemView::SingleSelection); //设置选择模式，选择单行
    ui->tableWidget_selected->setEditTriggers(QAbstractItemView::NoEditTriggers);//禁止编辑
    ui->tableWidget_selected->horizontalHeader()->setStretchLastSection(true);//该命令只是将最后一行的列宽度等于Table剩下的宽度

    ///第一腿交易双击事件响应
    connect(ui->tableWidget_first,SIGNAL(itemDoubleClicked(QTableWidgetItem*)),this,SLOT(FirstDoubleClicked(QTableWidgetItem*)));
    ///第二腿交易双击事件响应
    connect(ui->tableWidget_second,SIGNAL(itemDoubleClicked(QTableWidgetItem*)),this,SLOT(SecondDoubleClicked(QTableWidgetItem*)));
    ///上移按钮响应事件
    connect(ui->Button_Up,SIGNAL(clicked()),this,SLOT(UpButtonClicked()));
    ///下移按钮响应事件
    connect(ui->Button_Down,SIGNAL(clicked()),this,SLOT(DownButtonClicked()));
    ///删除按钮响应事件
    connect(ui->Button_Del,SIGNAL(clicked()),this,SLOT(DelButtonClicked()));

	connect(ui->ComboBox_FirstExchange,SIGNAL(currentTextChanged(const QString &)),this,SLOT(FirstUpdateExchange(const QString &)));

	connect(ui->ComboBox_SecondExchange,SIGNAL(currentTextChanged(const QString &)),this,SLOT(SecondUpdateExchange(const QString &)));

    SetMainDlg(pMainwindow);
}

PopMktCombineExchDialog::~PopMktCombineExchDialog()
{
    delete ui;
}

void PopMktCombineExchDialog::SetMainDlg(void* pMainwindow){
	m_pMainwindow = pMainwindow;
	SetContractTable();
	FirstUpdateExchange(ui->ComboBox_FirstExchange->currentText());
	SecondUpdateExchange(ui->ComboBox_SecondExchange->currentText());
}
void PopMktCombineExchDialog::SetContractTable(){
	ui->tableWidget_selected->setRowCount(0);
	//显示自定义组合合约
    for(int i = 0; i < ((MainWindow*)m_pMainwindow)->m_CombineSelfVector.size(); i++){
        MarketComExchInstrument oCombineSelf =  ((MainWindow*)m_pMainwindow)->m_CombineSelfVector[i];
        ui->tableWidget_selected->insertRow(i); //插入新行
        ui->tableWidget_selected->setItem(i,0,new QTableWidgetItem(oCombineSelf.m_strLeg1));
        ui->tableWidget_selected->setItem(i,1,new QTableWidgetItem(oCombineSelf.m_strLeg2));
    }
    ui->tableWidget_selected->resizeColumnsToContents();
}

void PopMktCombineExchDialog::FirstUpdateExchange(const QString & text){
	this->UpdateExchange(ui->tableWidget_first,text);
}
void PopMktCombineExchDialog::SecondUpdateExchange(const QString & text){
	this->UpdateExchange(ui->tableWidget_second,text);
}

void PopMktCombineExchDialog::UpdateExchange(QTableWidget * tablewiget,const QString & text){

	tablewiget->setRowCount(0);

	if(text == "郑州交易所" || text == "显示全部" ){
		int iRow = tablewiget->rowCount();
		for (QMap<QString,CThostFtdcInstrumentField>::iterator it = ((MainWindow*)m_pMainwindow)->m_CZCEInstrumentMap.begin();
			it != ((MainWindow*)m_pMainwindow)->m_CZCEInstrumentMap.end(); ++it ) {
				tablewiget->insertRow(iRow); //插入新行
				tablewiget->setItem(iRow,0,new QTableWidgetItem(it.key()));
				QString strInstrumentName = QString::fromLocal8Bit(it.value().InstrumentName);  
				tablewiget->setItem(iRow,1,new QTableWidgetItem(strInstrumentName));
				iRow++;
		}
	}
	if (text == "大连交易所" || text == "显示全部" ){
		int iRow = tablewiget->rowCount();
		for (QMap<QString,CThostFtdcInstrumentField>::iterator it = ((MainWindow*)m_pMainwindow)->m_DCEInstrumentMap.begin();
			it != ((MainWindow*)m_pMainwindow)->m_DCEInstrumentMap.end(); ++it ) {
				tablewiget->insertRow(iRow); //插入新行
				tablewiget->setItem(iRow,0,new QTableWidgetItem(it.key()));
				QString strInstrumentName = QString::fromLocal8Bit(it.value().InstrumentName);
				tablewiget->setItem(iRow,1,new QTableWidgetItem(strInstrumentName));
				iRow++;
		}
	} 
	if (text == "上海交易所" || text == "显示全部" ){
		int iRow = tablewiget->rowCount();
		for (QMap<QString,CThostFtdcInstrumentField>::iterator it = ((MainWindow*)m_pMainwindow)->m_SHFEInstrumentMap.begin();
			it != ((MainWindow*)m_pMainwindow)->m_SHFEInstrumentMap.end(); ++it ) {
				tablewiget->insertRow(iRow); //插入新行
				tablewiget->setItem(iRow,0,new QTableWidgetItem(it.key()));
				QString strInstrumentName = QString::fromLocal8Bit(it.value().InstrumentName);
				tablewiget->setItem(iRow,1,new QTableWidgetItem(strInstrumentName));
				iRow++;
		}
	}
	if (text == "金融交易所" || text == "显示全部" ){
		int iRow = tablewiget->rowCount();
		for (QMap<QString,CThostFtdcInstrumentField>::iterator it = ((MainWindow*)m_pMainwindow)->m_CFFEXInstrumentMap.begin();
			it != ((MainWindow*)m_pMainwindow)->m_CFFEXInstrumentMap.end(); ++it ) {
				tablewiget->insertRow(iRow); //插入新行
				tablewiget->setItem(iRow,0,new QTableWidgetItem(it.key()));
				QString strInstrumentName = QString::fromLocal8Bit(it.value().InstrumentName);
				tablewiget->setItem(iRow,1,new QTableWidgetItem(strInstrumentName));
				iRow++;
		}
	}
	//根据内容调整列宽,给Table填充数据之后再调用该函数进行Resize
    tablewiget->resizeColumnsToContents();
}
///确认按钮响应事件
void PopMktCombineExchDialog::OKButtonClicked(){
    //QMessageBox::information(NULL,QString("INFO"),QString("PopMktCombineExchDialog OK Button Clicked"));
    ((MainWindow*)m_pMainwindow)->m_CombineSelfVector.clear();
    int iRowCnt = ui->tableWidget_selected->rowCount();

    for(int i = 0; i < iRowCnt; i++){
        if(0 != ui->tableWidget_selected->item(i,0) && 0 != ui->tableWidget_selected->item(i,1)){
            MarketComExchInstrument oMarketComExchInstrument;
            oMarketComExchInstrument.m_strLeg1 =ui->tableWidget_selected->item(i,0)->text();
            oMarketComExchInstrument.m_strLeg2 =ui->tableWidget_selected->item(i,1)->text();
            ((MainWindow*)m_pMainwindow)->m_CombineSelfVector.push_back(oMarketComExchInstrument);
        }
    }

    emit UpdateCombineMarketdata();
    emit destroyWin();
}
///双击第一腿合约列表的响应事件
void PopMktCombineExchDialog::FirstDoubleClicked(QTableWidgetItem* pItem){
    //选中的行号
    int iRow = pItem->row();
    //合约代码
    QString strContractCode = ui->tableWidget_first->item(iRow,0)->text();

    //将选中的合约内容添加至组合合约列表的左列菜单
    int iRowCnt = ui->tableWidget_selected->rowCount();
    if(iRowCnt != 0){
        if(0 == ui->tableWidget_selected->item(iRowCnt-1,0)){
            ui->tableWidget_selected->setItem(iRowCnt-1,0,new QTableWidgetItem(strContractCode));
        }else{
            ui->tableWidget_selected->insertRow(iRowCnt);
            ui->tableWidget_selected->setItem(iRowCnt,0,new QTableWidgetItem(strContractCode));
        }
    }else{
        ui->tableWidget_selected->insertRow(iRowCnt);
        ui->tableWidget_selected->setItem(iRowCnt,0,new QTableWidgetItem(strContractCode));
    }
}
///双击第二腿合约列表的响应事件
void PopMktCombineExchDialog::SecondDoubleClicked(QTableWidgetItem* pItem){
    //选中的行号
    int iRow = pItem->row();
    //合约代码
    QString strContractCode = ui->tableWidget_second->item(iRow,0)->text();

    //将选中的合约内容添加至组合合约列表的左列菜单
    int iRowCnt = ui->tableWidget_selected->rowCount();
    if(iRowCnt != 0){
        if(0 == ui->tableWidget_selected->item(iRowCnt-1,1)){
            ui->tableWidget_selected->setItem(iRowCnt-1,1,new QTableWidgetItem(strContractCode));
        }else{
            ui->tableWidget_selected->insertRow(iRowCnt);
            ui->tableWidget_selected->setItem(iRowCnt,1,new QTableWidgetItem(strContractCode));
        }
    }else{
        ui->tableWidget_selected->insertRow(iRowCnt);
        ui->tableWidget_selected->setItem(iRowCnt,1,new QTableWidgetItem(strContractCode));
    }
}
///上移按钮单击响应事件
void PopMktCombineExchDialog::UpButtonClicked(){
    QList<QTableWidgetItem *>  ItemsSelected  = ui->tableWidget_selected->selectedItems();
    if(0 == ItemsSelected.size() ){//判断已选合约中是否有选中的Item
        QMessageBox::warning(NULL,QString("WARNNING"),QString("Please select a valid Item"));
    }else{
        //选中的行号
        int iRow = ItemsSelected.at(0)->row();
        if(0 != iRow ){
            ///上一行内容
            //第一腿
            QString strContractCode = ui->tableWidget_selected->item(iRow-1,0)->text();
            //第二腿
            QString strContractName = ui->tableWidget_selected->item(iRow-1,1)->text();

            ///选中行的内容
            //第一腿
            QString strContractCodeSelected = ui->tableWidget_selected->item(iRow,0)->text();
            //第二腿
            QString strContractNameSelected = ui->tableWidget_selected->item(iRow,1)->text();

            ui->tableWidget_selected->setItem(iRow -1 , 0 ,new QTableWidgetItem(strContractCodeSelected));
            ui->tableWidget_selected->setItem(iRow -1 , 1 ,new QTableWidgetItem(strContractNameSelected));
            ui->tableWidget_selected->setItem(iRow , 0 ,new QTableWidgetItem(strContractCode));
            ui->tableWidget_selected->setItem(iRow , 1 ,new QTableWidgetItem(strContractName));
            ui->tableWidget_selected->selectRow(iRow -1);
        }
    }
}
///下移按钮单击响应事件
void PopMktCombineExchDialog::DownButtonClicked(){
    QList<QTableWidgetItem *>  ItemsSelected  = ui->tableWidget_selected->selectedItems();
    if(0 == ItemsSelected.size() ){//判断已选合约中是否有选中的Item
        QMessageBox::warning(NULL,QString("WARNNING"),QString("Please select a valid Item"));
    }else{
        //行数
        int iCnt = ui->tableWidget_selected->rowCount();
        //选中的行号
        int iRow = ItemsSelected.at(0)->row();
        if(iCnt != iRow + 1  ){
            ///下一行内容
            //第一腿
            QString strContractCode = ui->tableWidget_selected->item(iRow+1,0)->text();
            //第二腿
            QString strContractName = ui->tableWidget_selected->item(iRow+1,1)->text();
            ///选中行的内容
            //第一腿
            QString strContractCodeSelected = ui->tableWidget_selected->item(iRow,0)->text();
            //第二腿
            QString strContractNameSelected = ui->tableWidget_selected->item(iRow,1)->text();

            ui->tableWidget_selected->setItem(iRow +1 , 0 ,new QTableWidgetItem(strContractCodeSelected));
            ui->tableWidget_selected->setItem(iRow +1 , 1 ,new QTableWidgetItem(strContractNameSelected));
            ui->tableWidget_selected->setItem(iRow , 0 ,new QTableWidgetItem(strContractCode));
            ui->tableWidget_selected->setItem(iRow , 1 ,new QTableWidgetItem(strContractName));
            ui->tableWidget_selected->selectRow(iRow +1);
        }
    }
}
///删除按钮单击响应事件
void PopMktCombineExchDialog::DelButtonClicked(){
    QList<QTableWidgetItem *>  ItemsSelected  = ui->tableWidget_selected->selectedItems();
    if(0 == ItemsSelected.size() ){//判断已选合约中是否有选中的Item
        QMessageBox::warning(NULL,QString("WARNNING"),QString("Please select a valid Item"));
    }else{
        //选中的行号
        int iRow = ItemsSelected.at(0)->row();
        //删除选中的行
        ui->tableWidget_selected->removeRow(iRow);
    }
}
