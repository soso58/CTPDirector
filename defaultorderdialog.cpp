#include"CommUtil/comfunc.h"
#include "defaultorderdialog.h"
#include "ui_defaultorderdialog.h"
#include<QMessageBox>


DefaultOrderDialog::DefaultOrderDialog(void* pMainwindow,QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DefaultOrderDialog)
{
    ui->setupUi(this);

    ui->tableWidget_contracts->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->tableWidget_contracts->verticalHeader()->setVisible(false);
    ui->tableWidget_contracts->setShowGrid(true);//显示表格线
    ui->tableWidget_contracts->setSelectionMode ( QAbstractItemView::SingleSelection); //设置选择模式，选择单行
    ui->tableWidget_contracts->setEditTriggers(QAbstractItemView::NoEditTriggers);//禁止编辑
    ui->tableWidget_contracts->horizontalHeader()->setStretchLastSection(true);//该命令只是将最后一行的列宽度等于Table剩下的宽度

    //添加按钮响应事件
    connect(ui->Button_Commit,SIGNAL(clicked()),this,SLOT(ButtonCommitClicked()));
    //删除按钮响应事件
    connect(ui->Button_Del,SIGNAL(clicked()),this,SLOT(ButtonDelClicked()));
    //单击合约列表响应事件
    connect(ui->tableWidget_contracts,SIGNAL(itemClicked(QTableWidgetItem*)),this,SLOT(ItemClicked(QTableWidgetItem*)));
    //双击合约列表下单响应事件
    connect(ui->tableWidget_contracts,SIGNAL(itemDoubleClicked(QTableWidgetItem*)),this,SLOT(ItemDoubleClicked(QTableWidgetItem*)));

    //test code
    /*ui->tableWidget_contracts->setRowCount(0);
    ui->tableWidget_contracts->insertRow(ui->tableWidget_contracts->rowCount());
    ui->tableWidget_contracts->setItem(0,0,new QTableWidgetItem(QString("Test11111111")));
    ui->tableWidget_contracts->setItem(0,1,new QTableWidgetItem(QString("Test1")));
    ui->tableWidget_contracts->insertRow(ui->tableWidget_contracts->rowCount());
    ui->tableWidget_contracts->setItem(1,0,new QTableWidgetItem(QString("Test22222222")));
    ui->tableWidget_contracts->setItem(1,1,new QTableWidgetItem(QString("Test2")));*/
    ui->tableWidget_contracts->resizeColumnsToContents();//根据内容调整列宽,给Table填充数据之后再调用该函数进行Resize
	SetMainDlg(pMainwindow);	
}

DefaultOrderDialog::~DefaultOrderDialog()
{
    delete ui;
}

void DefaultOrderDialog::SetMainDlg(void* pMainwindow){
	m_pMainwindow = pMainwindow;
	//SetContractTable();
	//FirstUpdateExchange(ui->ComboBox_FirstExchange->currentText());
	//SecondUpdateExchange(ui->ComboBox_SecondExchange->currentText());
}

void DefaultOrderDialog::OKButtonClicked(){
    QMessageBox::information(NULL,QString("INFO"),QString("DefaultOrderDialog OK Button Clicked"));
    emit destroyWin();
}
///添加按钮响应事件
void DefaultOrderDialog::ButtonCommitClicked(){
    QString strContract = ui->lineEdit_Contract->text();
    QString strMount = ui->lineEdit_Amount->text();

    if(strContract.isEmpty()){
        QMessageBox::warning(NULL,QString("WARNING"),QString("Please input a valid Contract"));
    }else if(strMount.isEmpty() || 0 != ComFunc::isDigitStr(strMount)){
        QMessageBox::warning(NULL,QString("WARNING"),QString("Please input a valid Mount"));
    }else{
        //判断此合约代码是否在交易所中存在
        //检索合约表中是否存在此合约 如果存在更改对应的Mount值
        int iRow = ui->tableWidget_contracts->rowCount();
        for(int i =0; i< iRow ; i++){
            QString strCode = ui->tableWidget_contracts->item(i,0)->text();
            if(strContract == strCode){
                ui->tableWidget_contracts->setItem(i,1,new QTableWidgetItem(strMount));
                return;
            }
        }
        //原先Table中没有的话 插入新的一行
        ui->tableWidget_contracts->insertRow(iRow);
        ui->tableWidget_contracts->setItem(iRow,0,new QTableWidgetItem(strContract));
        ui->tableWidget_contracts->setItem(iRow,1,new QTableWidgetItem(strMount));
    }
}
///删除按钮响应事件
void DefaultOrderDialog::ButtonDelClicked(){
    QList<QTableWidgetItem *>  ItemsSelected  = ui->tableWidget_contracts->selectedItems();
    if(0 == ItemsSelected.size() ){//判断已选合约中是否有选中的Item
        QMessageBox::warning(NULL,QString("WARNNING"),QString("Please select a valid Item"));
    }else{
        //选中的行号
        int iRow = ItemsSelected.at(0)->row();
        //删除选中的行
        ui->tableWidget_contracts->removeRow(iRow);
    }
}
///单击合约列表响应事件
void DefaultOrderDialog::ItemClicked(QTableWidgetItem *pItem){
    int iRow = pItem->row();
    QString strContract = ui->tableWidget_contracts->item(iRow,0)->text();
    QString strMount = ui->tableWidget_contracts->item(iRow,1)->text();
    ui->lineEdit_Contract->setText(strContract);
    ui->lineEdit_Amount->setText(strMount);
}
///双击合约列表下单响应事件
void DefaultOrderDialog::ItemDoubleClicked(QTableWidgetItem *pItem){
    if(Qt::Checked  == ui->checkBox->checkState()){
        QMessageBox::information(NULL,QString("INFO"),QString("Double Clicked"));
    }
}
