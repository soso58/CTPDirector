#-------------------------------------------------
#
# Project created by QtCreator 2015-05-18T13:05:26
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Director
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    mainorderwidget.cpp \
    mainpreentrustwidget.cpp \
    mainconditionwidget.cpp \
    maincombineentrustwidget.cpp \
    marketselfwidget.cpp \
    marketcomexch.cpp \
    popmktcombineexch.cpp \
    mainbalancewidget.cpp \
    mainholdwidget.cpp \
    mainentrustqueryhiswidget.cpp \
    mainbusinessqueryhiswidget.cpp \
    maincontractwidget.cpp \
    mainaverageentrustwidget.cpp \
    selfmarketdialog.cpp \
    popmktcombineexchdialog.cpp \
    mainpopdialog.cpp \
    combineexchdialog.cpp \
    defaultorderdialog.cpp \
    quickorderdialog.cpp \
    combineorderdialog.cpp \
    tableview.cpp \
    CtpInterface/ThostFtdcMdSpiImpl.cpp \
    CtpInterface/ThostFtdcTraderSpiImpl.cpp \
    CommUtil/comfunc.cpp \
    Login/accountitem.cpp \
    Login/dropshadowwidget.cpp \
    Login/logindlg.cpp \
    Login/msgbox.cpp \
    Login/pushbutton.cpp \
    Login/utilskin.cpp \
    conditiondialog.cpp \
    combinecontroldialog.cpp \
    singlecontroldialog.cpp \
    settlementinfodialog.cpp

HEADERS  += mainwindow.h \
    mainorderwidget.h \
    mainpreentrustwidget.h \
    mainconditionwidget.h \
    maincombineentrustwidget.h \
    marketselfwidget.h \
    marketcomexch.h \
    popmktcombineexch.h \
    mainbalancewidget.h \
    mainholdwidget.h \
    mainentrustqueryhiswidget.h \
    mainbusinessqueryhiswidget.h \
    maincontractwidget.h \
    mainaverageentrustwidget.h \
    selfmarketdialog.h \
    popmktcombineexchdialog.h \
    mainpopdialog.h \
    combineexchdialog.h \
    defaultorderdialog.h \
    quickorderdialog.h \
    combineorderdialog.h \
    tableview.h \
    CtpInterface/ThostFtdcMdApi.h \
    CtpInterface/ThostFtdcMdSpiImpl.h \
    CtpInterface/ThostFtdcTraderApi.h \
    CtpInterface/ThostFtdcTraderSpiImpl.h \
    CtpInterface/ThostFtdcUserApiDataType.h \
    CtpInterface/ThostFtdcUserApiStruct.h \
    CommUtil/comfunc.h \
    CommUtil/common.h \
    Login/accountitem.h \
    Login/dropshadowwidget.h \
    Login/logindlg.h \
    Login/msgbox.h \
    Login/pushbutton.h \
    Login/utilskin.h \
    conditiondialog.h \
    combinecontroldialog.h \
    CtpInterface/FUNDLibrary.h \
    CtpInterface/NetPacket.h \
    singlecontroldialog.h \
    settlementinfodialog.h

LIBS += -L$$PWD/ thostmduserapi.lib
LIBS += -L$$PWD/ thosttraderapi.lib
#LIBS += -L$$PWD/ FundClient.lib

FORMS    += \
    mainwindow.ui \
    selfmarketdialog.ui \
    popmktcombineexchdialog.ui \
    mainpopdialog.ui \
    combineexchdialog.ui \
    defaultorderdialog.ui \
    quickorderdialog.ui \
    combineorderdialog.ui \
    ConditionDialog.ui \
    CombineControlDlg.ui \
    SingleControlDlg.ui \
    settlementinfodialog.ui

RC_FILE = cofco.rc

RESOURCES += \
    ctptrader.qrc

OTHER_FILES += \
    Resources/CtpTrader.qss
