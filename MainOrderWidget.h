#ifndef MAINORDERWIDGET_H
#define MAINORDERWIDGET_H

#include <QWidget>
#include <qstandarditemmodel.h>
#include <qabstractitemmodel.h>
#include <string.h>
#include <qtableview.h>
#include <qheaderview.h>
#include <qgridlayout.h>
#include <QMenu>
#include <QAction>
#include "CtpInterface/ThostFtdcUserApiStruct.h"

class MainOrderWidget : public QWidget
{
    Q_OBJECT
public:
    explicit MainOrderWidget(QWidget *parent = 0);

signals:
    void SigDeleteOrder(QString strLocalOrderID);
    void SigDeleteAllOrders();
public slots:
    ///右键弹出菜单响应函数
	void RightClickedMenuPop(const QPoint& pos);
    ///撤销委托
    void DeleteOrder();
    ///撤销全部委托
    void DeleteAllOrders();
    ///显示可撤事件响应
    void ShowOrders();
    ///刷新事件响应
    void UpdateAllOrders();
    ///导出事件响应
    void OutputAllOrders();

public:
    void setupModel();
    void setupViews();
	void ShowRtnOrder(CThostFtdcOrderField *pOrder);

public:
    QTableView*				m_tableOrder;
    QAbstractItemModel*		m_modelOrder;
	//委托报单信息
	QMap<QString,CThostFtdcOrderField>      m_EntrustOrderMap;

private:
    QItemSelectionModel *	selectionModel;
    QGridLayout *			MainLayout;
	QMenu*					m_RightPopMenu;

    QAction*                m_deleteAction;
    QAction*                m_deleteAllAction;
    QAction*                m_ShowAction;
    QAction*                m_updateAction;
    QAction*                m_outputAction;
};

#endif // MAINORDERWIDGET_H
