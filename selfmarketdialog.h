#ifndef SELFMARKETDIALOG_H
#define SELFMARKETDIALOG_H

#include <QDialog>
#include <QMap>
#include "mainwindow.h"

namespace Ui {
class SelfMarketDialog;
}

class SelfMarketDialog : public QDialog
{
    Q_OBJECT

public:
    explicit SelfMarketDialog(void* pMainwindow,QWidget *parent = 0);
    ~SelfMarketDialog();

    void SetContractTable();
    void SetMainDlg(void* pMainwindow);

signals:
    void destroyWin();
    void SubscribeMarketData();
public slots:
    void UpdateExchange(const QString & text);
    void OKButtonClicked();
    //添加按钮响应事件
    void ButtonAddClicked();
    //删除按钮响应事件
    void ButtonDelClicked();
    //上移按钮响应事件
    void ButtonUpClicked();
    //下移按钮响应事件
    void ButtonDownClicked();
private:
    void* m_pMainwindow;
    Ui::SelfMarketDialog *ui;
};

#endif // SELFMARKETDIALOG_H
