﻿#ifndef MAINBUSINESSQUERYHISWIDGET_H
#define MAINBUSINESSQUERYHISWIDGET_H

#include <QWidget>
#include <qstandarditemmodel.h>
#include <qabstractitemmodel.h>
#include <string.h>
#include <qtableview.h>
#include <qheaderview.h>
#include <qgridlayout.h>
#include <QHBoxLayout>
#include <QLabel>
#include <QComboBox>
#include <QPushButton>
#include <QCheckBox>
#include <QRadioButton>
#include <QDateEdit>
#include <QMenu>
#include <QAction>
#include "CtpInterface/ThostFtdcUserApiStruct.h"

class MainBusinessQueryHisWidget : public QWidget
{
    Q_OBJECT
public:
    explicit MainBusinessQueryHisWidget(QWidget *parent = 0);

signals:
    void ReqQryTrade(QString strAccountID);

public slots:
    void radioBtnSlot1();
    void radioBtnSlot2();
    void QueryButtonClicked();
	void QueryHisStateChanged(int state);
	///邮件弹出菜单响应函数
	void RightClickedMenuPop(const QPoint& pos);

public:
    void setupModel();
    void setupViews();
    void ShowTradeDetails(CThostFtdcTradeField *pTrade);

public:
    QTableView*             m_tableBusinessQueryHis;
    QAbstractItemModel*     m_modelBusinessQueryHis;
    QAbstractItemModel*     m_modelBusinessQueryHisDetails;
    QComboBox*              m_accountCombox;

private:
    QItemSelectionModel*    selectionModel;
    QGridLayout*            MainLayout;
    QHBoxLayout*            hboxlayout,*hboxlayout1;
	QHBoxLayout*            m_DateBoxLayout;
    QPushButton*            m_querybutton;
    QPushButton*            m_exportbutton;
    QRadioButton*           m_RadioButton2;
    QRadioButton*           m_RadioButton1;
	QCheckBox*				m_QueryHisCheckbox;
	QDateEdit*				m_StartDate;
	QDateEdit*				m_EndDate;
	QLabel*					m_StartLabel;
	QLabel*					m_EndLabel;
	QMenu*					m_RightPopMenu;
};

#endif // MAINBUSINESSQUERYHISWIDGET_H
