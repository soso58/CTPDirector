﻿#include "mainpreentrustwidget.h"
#include<QMessageBox>

MainPreEntrustWidget::MainPreEntrustWidget(QWidget *parent) :
    QWidget(parent)
{
    setupModel();
    setupViews();
}

void MainPreEntrustWidget::setupModel()
{
    m_modelPreEntrust = new QStandardItemModel(0, 11, this);
    m_modelPreEntrust->setHeaderData(0, Qt::Horizontal, QStringLiteral("是否选中"));
    m_modelPreEntrust->setHeaderData(1, Qt::Horizontal, QStringLiteral("预委托号"));
    m_modelPreEntrust->setHeaderData(2, Qt::Horizontal, QStringLiteral("资金账号"));
    m_modelPreEntrust->setHeaderData(3, Qt::Horizontal, QStringLiteral("合约"));
    m_modelPreEntrust->setHeaderData(4, Qt::Horizontal, QStringLiteral("买卖"));
    m_modelPreEntrust->setHeaderData(5, Qt::Horizontal, QStringLiteral("开平"));
    m_modelPreEntrust->setHeaderData(6, Qt::Horizontal, QStringLiteral("价格"));
    m_modelPreEntrust->setHeaderData(7, Qt::Horizontal, QStringLiteral("委手"));
    m_modelPreEntrust->setHeaderData(8, Qt::Horizontal, QStringLiteral("状态"));
    m_modelPreEntrust->setHeaderData(9, Qt::Horizontal, QStringLiteral("委托时间"));
    m_modelPreEntrust->setHeaderData(10, Qt::Horizontal, QStringLiteral("套保类型"));
}

void MainPreEntrustWidget::setupViews()
{
    hboxlayout = new QHBoxLayout;
    m_SelectAll = new QPushButton(QStringLiteral("全部选中"));
    m_DeleteSelected = new QPushButton(QStringLiteral("撤销选中"));
    m_ClearSelected = new QPushButton(QStringLiteral("清除选中"));
    m_ShowCheckBox = new QCheckBox(QStringLiteral("显示可撤"));

    hboxlayout->addWidget(m_SelectAll);
    hboxlayout->addWidget(m_DeleteSelected);
    hboxlayout->addWidget(m_ClearSelected);
    hboxlayout->addStretch();
    hboxlayout->addStretch();
    hboxlayout->addWidget(m_ShowCheckBox);

    MainLayout = new QGridLayout(this);

    m_tablePreEntrust = new QTableView;
    m_tablePreEntrust->setAlternatingRowColors(true);
    QFont font = m_tablePreEntrust->horizontalHeader()->font();
    font.setBold(true);
    m_tablePreEntrust->horizontalHeader()->setFont(font);

    m_tablePreEntrust->setModel(m_modelPreEntrust);
    m_tablePreEntrust->setEditTriggers(QAbstractItemView::NoEditTriggers);
    m_tablePreEntrust->verticalHeader()->setVisible(false); //隐藏列表头
    m_tablePreEntrust->verticalHeader()->setFixedWidth(40);
    m_tablePreEntrust->setSelectionBehavior(QAbstractItemView::SelectRows);
    m_tablePreEntrust->setSelectionMode(QAbstractItemView::SingleSelection);

    m_tablePreEntrust->setColumnWidth(0, 60);
    m_tablePreEntrust->setColumnWidth(1, 80);
    m_tablePreEntrust->setColumnWidth(2, 80);
    m_tablePreEntrust->setColumnWidth(3, 80);
    m_tablePreEntrust->setColumnWidth(4, 80);
    m_tablePreEntrust->setColumnWidth(5, 80);
    m_tablePreEntrust->setColumnWidth(6, 80);
    m_tablePreEntrust->setColumnWidth(7, 80);
    m_tablePreEntrust->setColumnWidth(8, 80);
    m_tablePreEntrust->setColumnWidth(9, 80);
    m_tablePreEntrust->setColumnWidth(10,80);

    MainLayout->addWidget(m_tablePreEntrust,0,0,1,7);
    MainLayout->addLayout(hboxlayout,1,0,1,7);
    MainLayout->setMargin(20);

	//创建菜单、菜单项
	this->m_tablePreEntrust->setContextMenuPolicy(Qt::CustomContextMenu);
    m_RightPopMenu = new QMenu(this->m_tablePreEntrust);
    m_DeleteAction = new QAction(QStringLiteral("撤销"),this);
    m_outputAction = new QAction(QStringLiteral("导出"),this);
	
    m_RightPopMenu->addAction(m_DeleteAction);
	m_RightPopMenu->addAction(m_outputAction);
    //右键弹出菜单事件绑定
	connect(this->m_tablePreEntrust,SIGNAL(customContextMenuRequested(const QPoint&)),this,SLOT(RightClickedMenuPop(const QPoint&)));
	//删除预埋单事件绑定
    connect(m_DeleteAction,SIGNAL(triggered()),this,SLOT(RemoveParkedOrder()));
    //全部选中事件绑定
    connect(m_SelectAll,SIGNAL(clicked()),this,SLOT(SelectButtonClicked()));
    //撤销选中事件绑定
    connect(m_DeleteSelected,SIGNAL(clicked()),this,SLOT(DeleteSelectedButtonClicked()));
    //清除选中事件绑定
    connect(m_ClearSelected,SIGNAL(clicked()),this,SLOT(ClearSelectedButtonClicked()));

	//显示可撤CheckBox状态切换事件绑定
	connect(m_ShowCheckBox,SIGNAL(stateChanged(int)),this,SLOT(ShowStateChanged(int)));
}
///显示可撤事件绑定
void MainPreEntrustWidget::ShowStateChanged(int state){
	m_modelPreEntrust->removeRows(0,m_modelPreEntrust->rowCount());
	if (state == Qt::Unchecked){
		QMap<QString,CThostFtdcParkedOrderField>::Iterator	Mapiter;
		for (Mapiter= m_ParkedOrdersMap.begin(); Mapiter != m_ParkedOrdersMap.end();  Mapiter++)
		{
			ShowParkedOrders(&Mapiter.value());
		}
	}else if(state == Qt::Checked){
		QMap<QString,CThostFtdcParkedOrderField>::Iterator	Mapiter;
		for (Mapiter= m_ParkedOrdersMap.begin(); Mapiter != m_ParkedOrdersMap.end();  Mapiter++)
		{
			if (Mapiter->Status != THOST_FTDC_PAOS_Deleted)
			{
				ShowParkedOrders(&Mapiter.value());
			}
		}
	}
}

///全部选中事件绑定
void MainPreEntrustWidget::SelectButtonClicked(){
    int iRow = m_modelPreEntrust->rowCount();
    for (int i = 0; i < iRow ; i++){
        if(true == ((QStandardItemModel*) m_modelPreEntrust)->item(i,0)->isCheckable()){
            ((QStandardItemModel*) m_modelPreEntrust)->item(i,0)->setCheckState(Qt::Checked);
        }
    }
}
///撤销选中事件绑定
void MainPreEntrustWidget::DeleteSelectedButtonClicked(){
    CThostFtdcRemoveParkedOrderField pRemoveParkedOrder;

    int iRow = m_modelPreEntrust->rowCount();
    for (int i = 0; i < iRow ; i++){
        if(true == ((QStandardItemModel*) m_modelPreEntrust)->item(i,0)->isCheckable() &&
           Qt::Checked == ((QStandardItemModel*) m_modelPreEntrust)->item(i,0)->checkState()){
            QString strParkedOrderID = ((QStandardItemModel*) m_modelPreEntrust)->item(i,1)->text();//预埋报单编号
            if (true == m_ParkedOrdersMap.contains(strParkedOrderID)){
                if (m_ParkedOrdersMap[strParkedOrderID].Status != THOST_FTDC_PAOS_Deleted){
                    memset(&pRemoveParkedOrder, 0, sizeof(CThostFtdcRemoveParkedOrderField));
                    ///经纪公司代码
                    memcpy_s(pRemoveParkedOrder.BrokerID,sizeof(TThostFtdcBrokerIDType),m_ParkedOrdersMap[strParkedOrderID].BrokerID,sizeof(TThostFtdcBrokerIDType));
                    ///投资者代码
                    memcpy_s(pRemoveParkedOrder.InvestorID,sizeof(TThostFtdcInvestorIDType),m_ParkedOrdersMap[strParkedOrderID].InvestorID,sizeof(TThostFtdcInvestorIDType));
                    ///预埋报单编号
                    memcpy_s(pRemoveParkedOrder.ParkedOrderID,sizeof(TThostFtdcParkedOrderIDType),m_ParkedOrdersMap[strParkedOrderID].ParkedOrderID,sizeof(TThostFtdcParkedOrderIDType));

                    emit SigRemoveParkedOrder(&pRemoveParkedOrder);
                }
            }
        }
    }
}
///清除选中事件绑定
void MainPreEntrustWidget::ClearSelectedButtonClicked(){
    int iRow = m_modelPreEntrust->rowCount();
    for (int i = 0; i < iRow ; i++){
        if(true == ((QStandardItemModel*) m_modelPreEntrust)->item(i,0)->isCheckable()){
            ((QStandardItemModel*) m_modelPreEntrust)->item(i,0)->setCheckState(Qt::Unchecked);
        }
    }
}
//删除预埋单事件绑定
void MainPreEntrustWidget::RemoveParkedOrder(){
	CThostFtdcRemoveParkedOrderField pRemoveParkedOrder;
	memset(&pRemoveParkedOrder, 0, sizeof(CThostFtdcRemoveParkedOrderField));

	QModelIndex index = m_tablePreEntrust->currentIndex();
	if (true == index.isValid()){//选中预埋单
		int iRow = index.row();
		QString strParkedOrderID = index.sibling(iRow,1).data().toString();//预埋报单编号
		if (true == m_ParkedOrdersMap.contains(strParkedOrderID)){
			if (m_ParkedOrdersMap[strParkedOrderID].Status != THOST_FTDC_PAOS_Deleted){
				///经纪公司代码
				memcpy_s(pRemoveParkedOrder.BrokerID,sizeof(TThostFtdcBrokerIDType),m_ParkedOrdersMap[strParkedOrderID].BrokerID,sizeof(TThostFtdcBrokerIDType));
				///投资者代码
				memcpy_s(pRemoveParkedOrder.InvestorID,sizeof(TThostFtdcInvestorIDType),m_ParkedOrdersMap[strParkedOrderID].InvestorID,sizeof(TThostFtdcInvestorIDType));
				///预埋报单编号
				memcpy_s(pRemoveParkedOrder.ParkedOrderID,sizeof(TThostFtdcParkedOrderIDType),m_ParkedOrdersMap[strParkedOrderID].ParkedOrderID,sizeof(TThostFtdcParkedOrderIDType));

				emit SigRemoveParkedOrder(&pRemoveParkedOrder);
				return;
			}
		}
	}
	//未选中有效预埋报单
    QMessageBox::critical(this,QString("ERROR"),QStringLiteral("请选择有效预埋报单进行撤销!"));
}
///右键弹出菜单响应函数
void MainPreEntrustWidget::RightClickedMenuPop(const QPoint& pos)
{
	m_RightPopMenu->exec(QCursor::pos());
}

///删除预埋单响应
void MainPreEntrustWidget::RemoveParkedOrder(CThostFtdcRemoveParkedOrderField *pRemoveParkedOrder){
	int iRow = m_modelPreEntrust->rowCount();
	for (int i = 0; i < iRow ; i++){
		///预埋报单编号
		QString strParkedOrderID =  ((QStandardItemModel*) m_modelPreEntrust)->item(i,1)->text();
		if (0 == strcmp(strParkedOrderID.toStdString().c_str(),pRemoveParkedOrder->ParkedOrderID)){
			((QStandardItemModel*) m_modelPreEntrust)->setItem(i, 8, new QStandardItem(QStringLiteral("已删除")));
            ((QStandardItemModel*) m_modelPreEntrust)->item(i,0)->setCheckState(Qt::Unchecked);
            ((QStandardItemModel*) m_modelPreEntrust)->item(i,0)->setCheckable(false);
			m_ParkedOrdersMap[pRemoveParkedOrder->ParkedOrderID].Status = THOST_FTDC_PAOS_Deleted;
		}
	}
    m_tablePreEntrust->reset();
}

void MainPreEntrustWidget::ShowParkedOrders(CThostFtdcParkedOrderField *pParkedOrder){
	//将预埋单信息保存在Map中
	CThostFtdcParkedOrderField oOrder;
	memcpy_s(&oOrder,sizeof(CThostFtdcParkedOrderField),pParkedOrder,sizeof(CThostFtdcParkedOrderField));
	this->m_ParkedOrdersMap.insert(QString(oOrder.ParkedOrderID),oOrder);

	int iRow = m_modelPreEntrust->rowCount();
	int iIndex = iRow;
	for (int i = 0; i < iRow ; i++){
		///预埋报单编号
		QString strParkedOrderID =  ((QStandardItemModel*) m_modelPreEntrust)->item(i,1)->text();
		if (0 == strcmp(strParkedOrderID.toStdString().c_str(),pParkedOrder->ParkedOrderID)){
			iIndex = i;
			break;
		}
	}
	///是否选中
	QStandardItem* Item = new QStandardItem;
	if (pParkedOrder->Status != THOST_FTDC_PAOS_Deleted){
		Item->setCheckable(true);
	}	
	((QStandardItemModel*) m_modelPreEntrust)->setItem(iIndex, 0, Item);
	///预埋报单编号
	((QStandardItemModel*) m_modelPreEntrust)->setItem(iIndex, 1, new QStandardItem(pParkedOrder->ParkedOrderID));
	///资金账号
	((QStandardItemModel*) m_modelPreEntrust)->setItem(iIndex, 2, new QStandardItem(pParkedOrder->UserID));
	///合约代码
	((QStandardItemModel*) m_modelPreEntrust)->setItem(iIndex, 3, new QStandardItem(pParkedOrder->InstrumentID));
	///买卖方向
	if (pParkedOrder->Direction == THOST_FTDC_D_Buy){
		((QStandardItemModel*) m_modelPreEntrust)->setItem(iIndex, 4, new QStandardItem(QStringLiteral("买入")));
	} else if(pParkedOrder->Direction == THOST_FTDC_D_Sell){
		((QStandardItemModel*) m_modelPreEntrust)->setItem(iIndex, 4, new QStandardItem(QStringLiteral("卖出")));
	}
	//开平
	if (pParkedOrder->CombOffsetFlag[0] == '0'){//开仓
		((QStandardItemModel*) m_modelPreEntrust)->setItem(iIndex, 5, new QStandardItem(QStringLiteral("开仓")));
	} 
	else{//平仓
		((QStandardItemModel*) m_modelPreEntrust)->setItem(iIndex, 5, new QStandardItem(QStringLiteral("平仓")));
	}
	//委托价格
	((QStandardItemModel*) m_modelPreEntrust)->setItem(iIndex, 6, new QStandardItem(QString::number(pParkedOrder->LimitPrice)));
	//委托数量
	((QStandardItemModel*) m_modelPreEntrust)->setItem(iIndex, 7, new QStandardItem(QString::number(pParkedOrder->VolumeTotalOriginal)));
	//状态
	if (pParkedOrder->Status == THOST_FTDC_PAOS_NotSend)
	{
		((QStandardItemModel*) m_modelPreEntrust)->setItem(iIndex, 8, new QStandardItem(QStringLiteral("未发送")));
	} 
	else if (pParkedOrder->Status == THOST_FTDC_PAOS_Send)
	{
		((QStandardItemModel*) m_modelPreEntrust)->setItem(iIndex, 8, new QStandardItem(QStringLiteral("已发送")));
	} 
	else if(pParkedOrder->Status == THOST_FTDC_PAOS_Deleted)
	{
		((QStandardItemModel*) m_modelPreEntrust)->setItem(iIndex, 8, new QStandardItem(QStringLiteral("已删除")));
	}
	//委托时间
	((QStandardItemModel*) m_modelPreEntrust)->setItem(iIndex, 9, new QStandardItem(QString(pParkedOrder->GTDDate)));
	//套保标识
	/*if (pOrder->CombHedgeFlag[0] == THOST_FTDC_HF_Speculation){
		((QStandardItemModel*) m_modelPreEntrust)->setItem(iIndex, 10, new QStandardItem(QStringLiteral("投机")));
	}else if(pOrder->CombHedgeFlag[0] == THOST_FTDC_HF_Arbitrage){
		((QStandardItemModel*) m_modelPreEntrust)->setItem(iIndex, 10, new QStandardItem(QStringLiteral("套利")));
	}else if(pOrder->CombHedgeFlag[0] == THOST_FTDC_HF_Hedge){
		((QStandardItemModel*) m_modelPreEntrust)->setItem(iIndex, 10, new QStandardItem(QStringLiteral("套保")));
	}*/
}

