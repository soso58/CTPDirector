﻿#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <qsplitter.h>
#include "marketselfwidget.h"
#include "marketcomexch.h"
#include "mainorderwidget.h"
#include "popmktcombineexch.h"
#include <QTabWidget>
#include "mainpreentrustwidget.h"
#include "mainconditionwidget.h"
#include "maincombineentrustwidget.h"
#include "mainbalancewidget.h"
#include "mainholdwidget.h"
#include "mainentrustqueryhiswidget.h"
#include "mainbusinessqueryhiswidget.h"
#include "maincontractwidget.h"
#include "mainaverageentrustwidget.h"
#include <QAction>
#include "CtpInterface/ThostFtdcMdApi.h"
#include "CtpInterface/ThostFtdcTraderApi.h"
#include "CtpInterface/ThostFtdcMdSpiImpl.h"
#include "CtpInterface/ThostFtdcTraderSpiImpl.h"
#include "mainpopdialog.h"
#include "ui_mainpopdialog.h"
#include "selfmarketdialog.h"
#include "ui_selfmarketdialog.h"
#include "popmktcombineexchdialog.h"
#include "ui_popmktcombineexchdialog.h"
#include "combineexchdialog.h"
#include "ui_combineexchdialog.h"
#include "combineorderdialog.h"
#include "ui_combineorderdialog.h"
#include "defaultorderdialog.h"
#include "ui_defaultorderdialog.h"
#include "quickorderdialog.h"
#include "ui_quickorderdialog.h"
#include <QSettings>
#include <QMap>
#include "CommUtil/common.h"
#include "combinecontroldialog.h"
#include "singlecontroldialog.h"
#include"settlementinfodialog.h"

using namespace std;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow();
    ~MainWindow();
    void setupViews();
    void createMenus();
    void createActions();
    void RegisterSpi();

    void CreateMainPopDlg(int iIndex);

	//初始化交易服务器
    bool InitTradeObject(QString strUserName,QString strPassword,QString strBrokerID,QString strServerName);
	//初始化行情服务器
	bool InitMdObject(QString strUserName,QString strPassword,QString strBrokerID);

	void OnMdLogin(int iRetID, QString errInfo);
	void OnTdLogin(int iRetID, QString errInfo);
	//登陆窗口信息交互信号槽

    char* GetExchangeID(char*	InstrumentID);
	void ReadConfigFile();
	void WriteConfigFile(int iGroupIndex);

	void OnDepthMarketData(CThostFtdcDepthMarketDataField *pDepthMarketData);
	///订阅行情
	void SubscribeMarketData();

	///请求查询投资者结算结果响应
	void OnRspQrySettlementInfo(CThostFtdcSettlementInfoField *pSettlementInfo, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast);
	///投资者结算结果确认响应
    void OnRspSettlementInfoConfirm(QString ConfirmDateTime);

    ///请求查询合约响应
	void OnRspQryInstrument(CThostFtdcInstrumentField *pInstrument, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast);
    ///查询资金账户响应
    void OnRspQryTradingAccount(CThostFtdcTradingAccountField *pTradingAccount, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast);
    ///查询持仓响应
    void OnRspQryInvestorPosition(CThostFtdcInvestorPositionField *pInvestorPosition, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast);
	///请求查询报单响应
	void OnRspQryOrder(CThostFtdcOrderField *pOrder);
	///请求查询成交响应
	void OnRspQryTrade(CThostFtdcTradeField *pTrade);
	///预埋单录入请求响应
	void OnRspParkedOrderInsert(CThostFtdcParkedOrderField *pParkedOrder);

	///组合单显示
	void ShowCombineOrder(int icnt, void* pData);
	///均价单显示
	void ShowAverageOrder(int icnt, void* pData);

    void ShowStatusTip(QString str);
public slots:
	void SlotShowSettlementInfoDialog();
    void SetStatusTip(QString str);
    void QuotTabDoubleClicked(int index);
    void ShowStrategyTab();
	///组合单行情推送，并对组合单进行判断以进行下单
	void SlotCombineOrder(QString Leg1,QString Leg2,int buy_high_amount,QString csBuyPrice,int sale_low_amount,QString csSellPrice);

	///组合单请求
    //void AddNewCombineOrder(COMBINORDERINFO typeOrder);
    ///均价单请求
    //void AddNewAverageOrder(AVERAGEORDERINFO averageOrder);
    
	///撤销全部委托
    void DeleteAllOrders();
	///撤销委托
	void DeleteOrder(QString strLocalOrderID);
    ///预埋单请求
    void OnParkedOrderInsert(CThostFtdcParkedOrderField *pParkedOrder);
	
	///报单请求
	void OnOrderInsert(CThostFtdcInputOrderField* pInputOrder);
	///均价单报单请求
	void OnOrderInsert(CThostFtdcInputOrderField* pInputOrder,QString AverageNo);
	///组合单报单请求
	void OnCombineOrderInsert(CThostFtdcInputOrderField *pInputOrder,QString CombineNo);

    ///条件单请求
    void OnConditonOrderInsert(CThostFtdcInputOrderField* pOrder,ConditionType* pCondition);
	///报单录入请求响应
	void OnRspOrderInsert(CThostFtdcInputOrderField *pInputOrder, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast);
	///报单通知
	void OnRtnOrder(CThostFtdcOrderField *pOrder);
	///成交通知
	void OnRtnTrade(CThostFtdcTradeField *pTrade);

	///请求查询资金账户
	void ReqQryTradingAccount(QString strAccountID);
    ///请求查询投资者持仓
    void ReqQryInvestorPosition(QString strAccountID);
    ///请求查询报单
    void QueryOrder(QStringList strAttrs);
    ///请求查询成交
    void ReqQryTrade(QString strAccountID);

    ///更新选定的自定义组合行情信息
    void UpdateCombineMarketdata();
    ///更新选定的自定义行情信息
	void UpdateMarketdata();

    void destroyWin();
    void exit();
    ///自选行情设置
    void selfdialogshow();
    ///自定义组合行情
    void marketcomexchdialogshow();
    ///交易所组合行情
    void combineexchdialogshow();
    ///默认下单量设置
    void defaultorderdialogshow();
    ///快速下单设置
    void quickorderdialogshow();
    ///组合单参数设置
    void combineorderdialogshow();
    ///切换选项设置响应事件
    void slotItemPressed(QTreeWidgetItem *item, int column);
	///删除预埋单
	void RemoveParkedOrder(CThostFtdcRemoveParkedOrderField* pOrder);
    ///选择自选行情
    void SelectSelfContract(const QModelIndex & oIndex);
    ///选择自定义组合行情
    void SelectCombContract(const QModelIndex & oIndex);
signals:
    void SigSetStatusTip(QString str);
	void SigShowSettlementInfoDialog();
    
public:
	//CTP服务器的配置信息
	ServerInfo m_ServerInfo;
	//CTP服务器名称
	QString m_szServerName;
	//从通讯服务器获取的资金账号列表
    //vector<ACCOUNT_INFO>        m_vAccounts;
	QString						m_szBrokerId;

	//均价单Map
	QMap<QString,vector<int>>   m_MapAverageOrder;

    CThostFtdcMdApi*            m_pmdUserApi;
    CThostFtdcTraderApi*        m_pTradeUserApi;
    CThostFtdcMdSpiImpl*        m_pThostFtdcMdSpiImpl;
    CThostFtdcTraderSpiImpl*    m_pThostFtdcTraderSpiImpl;
	//郑州交易所
	QMap<QString,CThostFtdcInstrumentField> m_CZCEInstrumentMap;
	//上海交易所
	QMap<QString,CThostFtdcInstrumentField> m_SHFEInstrumentMap;
	//中国金融期货交易所
	QMap<QString,CThostFtdcInstrumentField> m_CFFEXInstrumentMap;
	//大连交易所
	QMap<QString,CThostFtdcInstrumentField> m_DCEInstrumentMap;

    //自选合约
    vector<CThostFtdcInstrumentField>       m_ContractVector;
    //自定义组合合约
    vector<MarketComExchInstrument>         m_CombineSelfVector;
    QMap<QString,CThostFtdcDepthMarketDataField> m_CombineSelfMap;
    //交易所组合合约
    vector<QString>                         m_CombineExchVector;
	//默认下单量信息
	QMap<QString,int>                       m_DefaultOrderCnt;

	MainPreEntrustWidget*			mainpreentrustwidget;

private:
	//组合单Map 存储组合单的详细信息
	QVector<STCombineInfo*>					m_CombineOrderVector;

	//组合单Map OrderRef-CombineNO  存储组合单和报单之间的所属关系
	QMap<int,QString>               m_CombineMap;
	//均价单Map OrderRef-AverageNO  存储均价单和报单之间的所属关系
	QMap<int,QString>               m_AverageMap;


	//订阅行情的合约数量和合约列表
	int m_iContractCount;
	char** m_Instrument;

    //持仓Vector
    vector<CThostFtdcInvestorPositionField> m_positionlist;

    long							m_lInstrumentCount;

    QTabWidget*						m_Quottab,*m_tabWidget;
    MarketSelfWidget*				marketselfwidget;
    MarketComexch*					marketcomexch;
    PopMktCombineExch*				popmktcombineexch;

    QDialog*                        m_ControlDlg;
    SingleControlDialog*            m_SingleControlDlg;//对应自选行情的控制模块
    CombineControlDialog*           m_CombineControlDlg;//对应自定义组合行情的控制模块
    MainOrderWidget*				mainorderwidget;
    
    MainConditionWidget*			mainconditionwidget;
    MainCombineEntrustWidget*		maincombineentrustwidget;
    MainBalanceWidget*				mainbalancewidget;
    MainHoldWidget*					mainholdwidget;
    MainEntrustQueryHisWidget*		mainentrustqueryhiswidget;
    MainBusinessQueryHisWidget*		mainbusinessqueryhiswidget;
    MainContractWidget*				maincontractwidget;
    MainAverageEntrustWidget*		mainaverageentrustwidget;
    SettlementInfoDialog*           m_SettlementInfoDialog;//客户结算单确认Dialog

    QStatusBar* m_pStatus;

    QMenu* fileMenu;//文件菜单
    QMenu* editMenu;//编辑菜单
    QMenu* m_strategyMenu;//策略菜单
    QMenu* helpMenu;//帮助菜单
    QMenu* formatMenu;

    QAction* newAct,* newAct1,* newAct2,* newAct3,* newAct4,* newAct5,* newAct6;//新建
    QAction* m_StrategyAct;//趋势策略
    QAction* openAct;//打开
    QAction* saveAct;//保存
    QAction* printAct;//打印
    QAction* exitAct;//退出
    QAction* boldAct;
    QAction* italicAct;

    QAction* aboutAct1,*aboutAct2;//关于
    QLabel* infoLabel;
    QDialog *m_MainPopDialog;
    QDialog *dialog2;
    QWidget* mainwidget;
    QGridLayout* mainlayout;
    QHBoxLayout* buttonlayout;
    QPushButton* confirmbutton;
    QPushButton* cancelbutton;

    PopMktCombineExchDialog* m_PopMktCombineExchDialog;
    CombineExchDialog* m_CombineExchDialog;
    DefaultOrderDialog* m_DefaultOrderDialog;
    QuickOrderDialog* m_QuickOrderDialog;
    CombineOrderDialog* m_CombineOrderDialog;
    QDialog* m_SelfMarketDialog;
};

#endif // MAINWINDOW_H
