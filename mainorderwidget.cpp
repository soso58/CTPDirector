﻿#include "MainOrderWidget.h"
#include <QMessageBox>

MainOrderWidget::MainOrderWidget(QWidget *parent) :
    QWidget(parent)
{
    setupModel();
    setupViews();
}

void MainOrderWidget::setupModel()
{
    m_modelOrder = new QStandardItemModel(0, 12, this);
    m_modelOrder->setHeaderData(0, Qt::Horizontal, QStringLiteral("资金账号"));
    m_modelOrder->setHeaderData(1, Qt::Horizontal, QStringLiteral("委托号"));
	m_modelOrder->setHeaderData(2, Qt::Horizontal, QStringLiteral("相关报单"));
    m_modelOrder->setHeaderData(3, Qt::Horizontal, QStringLiteral("合约"));
    m_modelOrder->setHeaderData(4, Qt::Horizontal, QStringLiteral("买卖"));
    m_modelOrder->setHeaderData(5, Qt::Horizontal, QStringLiteral("开平"));
    m_modelOrder->setHeaderData(6, Qt::Horizontal, QStringLiteral("委托价格"));
    m_modelOrder->setHeaderData(7, Qt::Horizontal, QStringLiteral("委手"));  //b/s
    m_modelOrder->setHeaderData(8, Qt::Horizontal, QStringLiteral("成手"));
    m_modelOrder->setHeaderData(9, Qt::Horizontal, QStringLiteral("状态"));
    m_modelOrder->setHeaderData(10, Qt::Horizontal, QStringLiteral("委托时间"));
    m_modelOrder->setHeaderData(11, Qt::Horizontal, QStringLiteral("套保标识"));
}

void MainOrderWidget::setupViews()
{
    MainLayout = new QGridLayout(this);

    m_tableOrder = new QTableView;
    m_tableOrder->setAlternatingRowColors(true);
    QFont font = m_tableOrder->horizontalHeader()->font();
    font.setBold(true);
    m_tableOrder->horizontalHeader()->setFont(font);
//    tableOrder->setStyleSheet("QTableView::item:selected { selection-color: rgb(0, 0, 0) }" "QTableView::item:selected { background-color: rgb(255, 255, 0) }"
//                "QTableView{background-color: rgb(0, 0, 0);" "alternate-background-color: rgb(41, 36, 33);}");

    m_tableOrder->setModel(m_modelOrder);
    m_tableOrder->setEditTriggers(QAbstractItemView::NoEditTriggers);
    m_tableOrder->verticalHeader()->setVisible(false); //隐藏列表头
    m_tableOrder->verticalHeader()->setFixedWidth(40);
    m_tableOrder->setSelectionBehavior(QAbstractItemView::SelectRows);
    m_tableOrder->setSelectionMode(QAbstractItemView::SingleSelection);

    m_tableOrder->setColumnWidth(0, 80);
    m_tableOrder->setColumnWidth(1, 80);
    m_tableOrder->setColumnWidth(2, 80);
    m_tableOrder->setColumnWidth(3, 80);
    m_tableOrder->setColumnWidth(4, 50);
    m_tableOrder->setColumnWidth(5, 70);
    m_tableOrder->setColumnWidth(6, 50);
    m_tableOrder->setColumnWidth(7, 80);
    m_tableOrder->setColumnWidth(8, 100);
    m_tableOrder->setColumnWidth(9, 80);
    m_tableOrder->setColumnWidth(10, 80);
	m_tableOrder->setColumnWidth(11, 80);

    MainLayout->addWidget(m_tableOrder,0,0);
    MainLayout->setMargin(20);

	//创建菜单、菜单项
	this->m_tableOrder->setContextMenuPolicy(Qt::CustomContextMenu);
	m_RightPopMenu = new QMenu(this->m_tableOrder);
    m_deleteAction = new QAction(QStringLiteral("撤销委托"),this);
    m_deleteAllAction = new QAction(QStringLiteral("全部撤销"),this);
    m_ShowAction = new QAction(QStringLiteral("显示可撤"),this);
    m_updateAction = new QAction(QStringLiteral("刷新"),this);
    m_outputAction = new QAction(QStringLiteral("导出"),this);
	m_ShowAction->setCheckable(true);
    m_RightPopMenu->addAction(m_deleteAction);
    m_RightPopMenu->addAction(m_deleteAllAction);
    m_RightPopMenu->addAction(m_ShowAction);
    m_RightPopMenu->addAction(m_updateAction);
    //m_RightPopMenu->addAction(m_outputAction);

    //右键弹出菜单事件绑定
	connect(this->m_tableOrder,SIGNAL(customContextMenuRequested(const QPoint&)),this,SLOT(RightClickedMenuPop(const QPoint&)));
    //撤销委托事件响应
    connect(m_deleteAction, SIGNAL(triggered()), this, SLOT(DeleteOrder()));
    //全部撤销事件响应
    connect(m_deleteAllAction, SIGNAL(triggered()), this, SLOT(DeleteAllOrders()));
    //显示可撤事件响应
    connect(m_ShowAction, SIGNAL(triggered()), this, SLOT(ShowOrders()));
    //刷新事件响应
    connect(m_updateAction, SIGNAL(triggered()), this, SLOT(UpdateAllOrders()));
    //导出事件响应
    connect(m_outputAction, SIGNAL(triggered()), this, SLOT(OutputAllOrders()));
}
///撤销委托
void MainOrderWidget::DeleteOrder(){
   QModelIndex index = m_tableOrder->currentIndex();
   if (true == index.isValid()){//选中委托
	   int iRow = index.row();
	   QString strOrderLocalID = index.sibling(iRow,1).data().toString();//委托报单编号
       emit SigDeleteOrder(strOrderLocalID);
   }else{//未选中有效委托
       QMessageBox::critical(NULL,QString("ERROR"),QStringLiteral("请选择有效委托进行撤销!"));
   }
}
///撤销全部委托
void MainOrderWidget::DeleteAllOrders(){
    emit SigDeleteAllOrders();
}
///显示可撤事件响应
void MainOrderWidget::ShowOrders(){
	QMap<QString,CThostFtdcOrderField>::iterator iter;
	if (true == m_ShowAction->isChecked()){
		m_modelOrder->removeRows(0,m_modelOrder->rowCount());
		for(iter = this->m_EntrustOrderMap.begin(); iter != this->m_EntrustOrderMap.end(); ++iter){
			CThostFtdcOrderField oOrder = iter.value();
			if( oOrder.OrderStatus == THOST_FTDC_OST_PartTradedQueueing ||//部分成交还在队列中
				oOrder.OrderStatus == THOST_FTDC_OST_NoTradeQueueing){//未成交还在队列中
				this->ShowRtnOrder(&oOrder);
			}
		}
	}else{
		m_modelOrder->removeRows(0,m_modelOrder->rowCount());
		for(iter = this->m_EntrustOrderMap.begin(); iter != this->m_EntrustOrderMap.end(); ++iter){
			CThostFtdcOrderField oOrder = iter.value();
			this->ShowRtnOrder(&oOrder);
		}
	}
}
///刷新事件响应
void MainOrderWidget::UpdateAllOrders(){
	/*QMap<QString,CThostFtdcOrderField>::iterator iter;
	m_modelOrder->removeRows(0,m_modelOrder->rowCount());
	for(iter = this->m_EntrustOrderMap.begin(); iter != this->m_EntrustOrderMap.end(); ++iter){
		CThostFtdcOrderField oOrder = iter.value();
		this->ShowRtnOrder(&oOrder);
	}*/
}
///导出事件响应
void MainOrderWidget::OutputAllOrders(){

}
///右键弹出菜单响应函数
void MainOrderWidget::RightClickedMenuPop(const QPoint& pos)
{
	m_RightPopMenu->exec(QCursor::pos());
}

void MainOrderWidget::ShowRtnOrder(CThostFtdcOrderField *pOrder){
	//将报单信息保存在Map中
	CThostFtdcOrderField oOrder;
	memcpy_s(&oOrder,sizeof(CThostFtdcOrderField),pOrder,sizeof(CThostFtdcOrderField));
	this->m_EntrustOrderMap.insert(QString(pOrder->OrderLocalID),oOrder);

	int iRow = m_modelOrder->rowCount();
	int iIndex = iRow;
	for (int i = 0; i < iRow ; i++){
		///本地报单编号
		QString strOrderLocalID =  ((QStandardItemModel*) m_modelOrder)->item(i,1)->text();
		if (0 == strcmp(strOrderLocalID.toStdString().c_str(),pOrder->OrderLocalID)){
			iIndex = i;
			break;
		}
	}
	///资金账号
	((QStandardItemModel*) m_modelOrder)->setItem(iIndex, 0, new QStandardItem(pOrder->UserID));
	///本地报单编号
	((QStandardItemModel*) m_modelOrder)->setItem(iIndex, 1, new QStandardItem(pOrder->OrderLocalID));
	///相关报单
	((QStandardItemModel*) m_modelOrder)->setItem(iIndex, 2, new QStandardItem(pOrder->RelativeOrderSysID));

	///合约代码
	((QStandardItemModel*) m_modelOrder)->setItem(iIndex, 3, new QStandardItem(pOrder->InstrumentID));
	///买卖方向
	if (pOrder->Direction == THOST_FTDC_D_Buy){
		((QStandardItemModel*) m_modelOrder)->setItem(iIndex, 4, new QStandardItem(QStringLiteral("买入")));
	} else if(pOrder->Direction == THOST_FTDC_D_Sell){
		((QStandardItemModel*) m_modelOrder)->setItem(iIndex, 4, new QStandardItem(QStringLiteral("卖出")));
	}
	//开平
	if (pOrder->CombOffsetFlag[0] == '0'){//开仓
		((QStandardItemModel*) m_modelOrder)->setItem(iIndex, 5, new QStandardItem(QStringLiteral("开仓")));
	} 
	else{//平仓
		((QStandardItemModel*) m_modelOrder)->setItem(iIndex, 5, new QStandardItem(QStringLiteral("平仓")));
	}
	//委托价格
	((QStandardItemModel*) m_modelOrder)->setItem(iIndex, 6, new QStandardItem(QString::number(pOrder->LimitPrice)));
	//委托数量
	((QStandardItemModel*) m_modelOrder)->setItem(iIndex, 7, new QStandardItem(QString::number(pOrder->VolumeTotalOriginal)));
	//成交数量
	((QStandardItemModel*) m_modelOrder)->setItem(iIndex, 8, new QStandardItem(QString::number(pOrder->VolumeTraded)));
	//状态
	((QStandardItemModel*) m_modelOrder)->setItem(iIndex, 9, new QStandardItem(QString::fromLocal8Bit(pOrder->StatusMsg)));
	//委托时间
	((QStandardItemModel*) m_modelOrder)->setItem(iIndex, 10, new QStandardItem(QString(pOrder->TradingDay)));
	//套保标识
	if (pOrder->CombHedgeFlag[0] == THOST_FTDC_HF_Speculation){
		((QStandardItemModel*) m_modelOrder)->setItem(iIndex, 11, new QStandardItem(QStringLiteral("投机")));
	}else if(pOrder->CombHedgeFlag[0] == THOST_FTDC_HF_Arbitrage){
		((QStandardItemModel*) m_modelOrder)->setItem(iIndex, 11, new QStandardItem(QStringLiteral("套利")));
	}else if(pOrder->CombHedgeFlag[0] == THOST_FTDC_HF_Hedge){
		((QStandardItemModel*) m_modelOrder)->setItem(iIndex, 11, new QStandardItem(QStringLiteral("套保")));
	}
}
