﻿#include "maincontractwidget.h"

MainContractWidget::MainContractWidget(QWidget *parent) :
    QWidget(parent)
{
    setupModel();
    setupViews();
}

void MainContractWidget::setupModel()
{
    modelContract = new QStandardItemModel(0, 10, this);
    modelContract->setHeaderData(0, Qt::Horizontal, QStringLiteral("交易所"));
    modelContract->setHeaderData(1, Qt::Horizontal, QStringLiteral("合约代码"));
    modelContract->setHeaderData(2, Qt::Horizontal, QStringLiteral("合约名称"));
    modelContract->setHeaderData(3, Qt::Horizontal, QStringLiteral("每手吨数"));
    modelContract->setHeaderData(4, Qt::Horizontal, QStringLiteral("最小价差"));
    modelContract->setHeaderData(5, Qt::Horizontal, QStringLiteral("最小下单手数"));
    modelContract->setHeaderData(6, Qt::Horizontal, QStringLiteral("最大下单手数"));
    modelContract->setHeaderData(7, Qt::Horizontal, QStringLiteral("开仓保证金"));
    modelContract->setHeaderData(8, Qt::Horizontal, QStringLiteral("合约类型"));
    modelContract->setHeaderData(9, Qt::Horizontal, QStringLiteral("合约数量乘数"));//合约数量乘数
}

void MainContractWidget::setupViews()
{
    MainLayout = new QGridLayout(this);

    tableContract = new QTableView;
    tableContract->setAlternatingRowColors(true);
    QFont font = tableContract->horizontalHeader()->font();
    font.setBold(true);
    tableContract->horizontalHeader()->setFont(font);
    tableContract->setModel(modelContract);
    tableContract->setEditTriggers(QAbstractItemView::NoEditTriggers);
    tableContract->verticalHeader()->setVisible(false); //隐藏列表头
    tableContract->verticalHeader()->setFixedWidth(40);
    tableContract->setSelectionBehavior(QAbstractItemView::SelectRows);
    tableContract->setSelectionMode(QAbstractItemView::SingleSelection);

    tableContract->setColumnWidth(0, 100);
    tableContract->setColumnWidth(1, 120);
    tableContract->setColumnWidth(2, 160);
    tableContract->setColumnWidth(3, 120);
    tableContract->setColumnWidth(4, 100);
    tableContract->setColumnWidth(5, 100);
    tableContract->setColumnWidth(6, 100);
    tableContract->setColumnWidth(7, 150);
    tableContract->setColumnWidth(8, 100);
    tableContract->setColumnWidth(9, 100);//合约数量乘数

    MainLayout->addWidget(tableContract,0,0);
    MainLayout->setMargin(20);
}

void MainContractWidget::AddRows(QMap<QString,CThostFtdcInstrumentField> & oInstrumentMap){

	int iCount = modelContract->rowCount();
	
	for (QMap<QString,CThostFtdcInstrumentField>::iterator it = oInstrumentMap.begin(); it != oInstrumentMap.end(); ++it ) {
		
		//交易所
		((QStandardItemModel*) modelContract)->setItem(iCount, 0, new QStandardItem(it->ExchangeID));
		//合约代码
		((QStandardItemModel*) modelContract)->setItem(iCount, 1, new QStandardItem(it->InstrumentID));
        //合约名称
        QString strInstrumentName = QString::fromLocal8Bit(it->InstrumentName);
        ((QStandardItemModel*) modelContract)->setItem(iCount, 2, new QStandardItem(strInstrumentName));
        //每手吨数
        ((QStandardItemModel*) modelContract)->setItem(iCount, 3, new QStandardItem(QString::number(it->VolumeMultiple)));
        //最小价差
        ((QStandardItemModel*) modelContract)->setItem(iCount, 4, new QStandardItem(QString::number(it->PriceTick)));
        //最小下单手数
        ((QStandardItemModel*) modelContract)->setItem(iCount, 5, new QStandardItem(QString::number(it->MinMarketOrderVolume)));
        //最大下单手数
        ((QStandardItemModel*) modelContract)->setItem(iCount, 6, new QStandardItem(QString::number(it->MaxMarketOrderVolume)));
        //开仓保证金
        if(it->LongMarginRatio > 1.79e+308){//期货期权保证金率为double最大值
            ((QStandardItemModel*) modelContract)->setItem(iCount, 7, new QStandardItem(QStringLiteral("-")));
        }else{
            ((QStandardItemModel*) modelContract)->setItem(iCount, 7, new QStandardItem(QString::number(it->LongMarginRatio)));
		}
        //合约类型
        if(it->ProductClass == THOST_FTDC_PC_Futures){///期货
            ((QStandardItemModel*) modelContract)->setItem(iCount, 8, new QStandardItem(QStringLiteral("期货")));
        }else if(it->ProductClass == THOST_FTDC_PC_Options){///期货期权
            ((QStandardItemModel*) modelContract)->setItem(iCount, 8, new QStandardItem(QStringLiteral("期货期权")));
        }else if(it->ProductClass == THOST_FTDC_PC_Combination){///组合
            ((QStandardItemModel*) modelContract)->setItem(iCount, 8, new QStandardItem(QStringLiteral("组合")));
        }else if(it->ProductClass == THOST_FTDC_PC_Spot){///即期
            ((QStandardItemModel*) modelContract)->setItem(iCount, 8, new QStandardItem(QStringLiteral("即期")));
        }else if(it->ProductClass == THOST_FTDC_PC_EFP){///期转现
            ((QStandardItemModel*) modelContract)->setItem(iCount, 8, new QStandardItem(QStringLiteral("期转现")));
        }else if(it->ProductClass == THOST_FTDC_PC_SpotOption){///现货期权
            ((QStandardItemModel*) modelContract)->setItem(iCount, 8, new QStandardItem(QStringLiteral("现货期权")));
        }else{
            ((QStandardItemModel*) modelContract)->setItem(iCount, 8, new QStandardItem(QStringLiteral("未知")));
        }
		iCount++;
    }
}

void MainContractWidget::AddRow(CThostFtdcInstrumentField* oInstrument){

    int iCount = modelContract->rowCount();
        //交易所
        ((QStandardItemModel*) modelContract)->setItem(iCount, 0, new QStandardItem(oInstrument->ExchangeID));
        //合约代码
        ((QStandardItemModel*) modelContract)->setItem(iCount, 1, new QStandardItem(oInstrument->InstrumentID));
        //合约名称
        QString strInstrumentName = QString::fromLocal8Bit(oInstrument->InstrumentName);
        ((QStandardItemModel*) modelContract)->setItem(iCount, 2, new QStandardItem(strInstrumentName));
        //每手吨数
        ((QStandardItemModel*) modelContract)->setItem(iCount, 3, new QStandardItem(QString::number(oInstrument->VolumeMultiple)));
        //最小价差
        ((QStandardItemModel*) modelContract)->setItem(iCount, 4, new QStandardItem(QString::number(oInstrument->PriceTick)));
        //最小下单手数
        ((QStandardItemModel*) modelContract)->setItem(iCount, 5, new QStandardItem(QString::number(oInstrument->MinMarketOrderVolume)));
        //最大下单手数
        ((QStandardItemModel*) modelContract)->setItem(iCount, 6, new QStandardItem(QString::number(oInstrument->MaxMarketOrderVolume)));
        //开仓保证金
        if(oInstrument->LongMarginRatio > 1.79e+308){//期货期权保证金率为double最大值
            ((QStandardItemModel*) modelContract)->setItem(iCount, 7, new QStandardItem(QStringLiteral("-")));
        }else{
            ((QStandardItemModel*) modelContract)->setItem(iCount, 7, new QStandardItem(QString::number(oInstrument->LongMarginRatio)));
        }
        //合约类型
        if(oInstrument->ProductClass == THOST_FTDC_PC_Futures){///期货
            ((QStandardItemModel*) modelContract)->setItem(iCount, 8, new QStandardItem(QStringLiteral("期货")));
        }else if(oInstrument->ProductClass == THOST_FTDC_PC_Options){///期货期权
            ((QStandardItemModel*) modelContract)->setItem(iCount, 8, new QStandardItem(QStringLiteral("期货期权")));
        }else if(oInstrument->ProductClass == THOST_FTDC_PC_Combination){///组合
            ((QStandardItemModel*) modelContract)->setItem(iCount, 8, new QStandardItem(QStringLiteral("组合")));
        }else if(oInstrument->ProductClass == THOST_FTDC_PC_Spot){///即期
            ((QStandardItemModel*) modelContract)->setItem(iCount, 8, new QStandardItem(QStringLiteral("即期")));
        }else if(oInstrument->ProductClass == THOST_FTDC_PC_EFP){///期转现
            ((QStandardItemModel*) modelContract)->setItem(iCount, 8, new QStandardItem(QStringLiteral("期转现")));
        }else if(oInstrument->ProductClass == THOST_FTDC_PC_SpotOption){///现货期权
            ((QStandardItemModel*) modelContract)->setItem(iCount, 8, new QStandardItem(QStringLiteral("现货期权")));
        }else{
            ((QStandardItemModel*) modelContract)->setItem(iCount, 8, new QStandardItem(QStringLiteral("未知")));
        }
        ((QStandardItemModel*) modelContract)->setItem(iCount, 9, new QStandardItem(QString::number(oInstrument->VolumeMultiple)));
}
