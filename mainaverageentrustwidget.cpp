﻿#include "mainaverageentrustwidget.h"
#include "CommUtil/comfunc.h"
#include<QMessageBox>
#include <qdebug>

MainAverageEntrustWidget::MainAverageEntrustWidget(QWidget *parent) :
    QWidget(parent)
{
    setupViews();
	connect(this,SIGNAL(SigStartTimer(QString ,int)),this,SLOT(SlotStartTimer(QString ,int)));
}

void MainAverageEntrustWidget::setupViews()
{
    layout = new QGridLayout;
    QLabel* accountLabel = new QLabel(QStringLiteral("资金账号"));
    m_accountCombox = new QComboBox;

    QLabel* contractLabel = new QLabel(QStringLiteral("合约"));
    m_contractEdit = new QLineEdit;

    QLabel* bsLabel = new QLabel(QStringLiteral("买卖"));
    m_bsCombox = new QComboBox;
    m_bsCombox->insertItem(0,QStringLiteral("买入"));
    m_bsCombox->insertItem(1,QStringLiteral("卖出"));

    QLabel* ocLabel = new QLabel(QStringLiteral("开平"));
    m_ocCombox = new QComboBox;
    m_ocCombox->insertItem(0,QStringLiteral("开仓"));
    m_ocCombox->insertItem(1,QStringLiteral("平仓"));
    m_ocCombox->insertItem(2,QStringLiteral("平今仓"));

    QLabel* handsLabel = new QLabel(QStringLiteral("每次下单手数"));
    m_HandsSpinBox = new QSpinBox;                       //创建SpinBox
    m_HandsSpinBox->setRange(0, 10000);                              //设置spinBox的值范围
    QLabel* totalhandsLabel = new QLabel(QStringLiteral("总手数"));
    m_TotalHandsSpinBox = new QSpinBox;                       //创建SpinBox
    m_TotalHandsSpinBox->setRange(0, 10000);                              //设置spinBox的值范围
    QLabel* timeLabel = new QLabel(QStringLiteral("时间间隔(秒)"));
    m_timeEdit = new QLineEdit;
    m_OrderTypeCheckbox = new QCheckBox(QStringLiteral("套保"));
    m_ConfirmButton = new QPushButton(QStringLiteral("确定"));

    accountLabel->setFixedWidth(80);
    m_accountCombox->setFixedWidth(100);
    contractLabel->setFixedWidth(80);
    m_contractEdit->setFixedWidth(100);
    bsLabel->setFixedWidth(80);
    m_bsCombox->setFixedWidth(100);
    ocLabel->setFixedWidth(80);
    m_ocCombox->setFixedWidth(100);
    handsLabel->setFixedWidth(80);
    m_HandsSpinBox->setFixedWidth(100);
    totalhandsLabel->setFixedWidth(80);
    m_TotalHandsSpinBox->setFixedWidth(100);
    timeLabel->setFixedWidth(80);
    m_timeEdit->setFixedWidth(100);
    m_OrderTypeCheckbox->setFixedWidth(80);
    m_ConfirmButton->setFixedWidth(80);

    layout->addWidget(accountLabel,0,0,1,1);
    layout->addWidget(m_accountCombox,1,0,1,1);
    layout->addWidget(contractLabel,0,1,1,1);
    layout->addWidget(m_contractEdit,1,1,1,1);
    layout->addWidget(bsLabel,0,2,1,1);
    layout->addWidget(m_bsCombox,1,2,1,1);
    layout->addWidget(ocLabel,0,3,1,1);
    layout->addWidget(m_ocCombox,1,3,1,1);
    layout->addWidget(handsLabel,0,4,1,1);
    layout->addWidget(m_HandsSpinBox,1,4,1,1);
    layout->addWidget(totalhandsLabel,0,5,1,1);
    layout->addWidget(m_TotalHandsSpinBox,1,5,1,1);
    layout->addWidget(timeLabel,0,6,1,1);
    layout->addWidget(m_timeEdit,1,6,1,1);
    layout->addWidget(m_OrderTypeCheckbox,1,7,1,1);
    layout->addWidget(m_ConfirmButton,1,8,1,1);

    m_treeWidget = new QTreeWidget;
    m_treeWidget->setAlternatingRowColors(true);

    //设定头项名称
    m_treeWidget->headerItem()->setText(0,QStringLiteral("均价单号"));
    m_treeWidget->headerItem()->setText(1,QStringLiteral("资金账号"));
    m_treeWidget->headerItem()->setText(2,QStringLiteral("合约"));
    m_treeWidget->headerItem()->setText(3,QStringLiteral("买卖"));
    m_treeWidget->headerItem()->setText(4,QStringLiteral("开平"));
    m_treeWidget->headerItem()->setText(5,QStringLiteral("套保标识"));
    m_treeWidget->headerItem()->setText(6,QStringLiteral("每次手数"));
    m_treeWidget->headerItem()->setText(7,QStringLiteral("总手数"));
    m_treeWidget->headerItem()->setText(8,QStringLiteral("时间间隔(秒)"));
    m_treeWidget->headerItem()->setText(9,QStringLiteral("状态"));
    m_treeWidget->headerItem()->setText(10,QStringLiteral("启用标识"));
    m_treeWidget->headerItem()->setText(11,QStringLiteral("委托时间"));
    m_treeWidget->headerItem()->setText(12,QStringLiteral("发送数量"));
    m_treeWidget->headerItem()->setText(13,QStringLiteral("成交数量"));
    m_treeWidget->headerItem()->setText(14,QStringLiteral("成交价格"));

    m_treeWidget->header()->setSectionsClickable(true);
    m_treeWidget->header()->setSectionsMovable(true);
    QFont font = m_treeWidget->header()->font();
    font.setBold(true);
    m_treeWidget->header()->setFont(font);

    m_treeWidget->setColumnCount(14);
    m_treeWidget->setColumnWidth(0,100);
    m_treeWidget->setColumnWidth(1,100);
    m_treeWidget->setColumnWidth(2,100);
    m_treeWidget->setColumnWidth(3,120);
    m_treeWidget->setColumnWidth(4,100);
    m_treeWidget->setColumnWidth(5,100);
    m_treeWidget->setColumnWidth(6,100);
    m_treeWidget->setColumnWidth(7,100);
    m_treeWidget->setColumnWidth(8,100);
    m_treeWidget->setColumnWidth(9,100);
    m_treeWidget->setColumnWidth(10,100);
    m_treeWidget->setColumnWidth(11,100);
    m_treeWidget->setColumnWidth(12,100);
    m_treeWidget->setColumnWidth(13,100);
    m_treeWidget->setColumnWidth(14,100);

    MainLayout = new QGridLayout(this);
    MainLayout->addLayout(layout,0,0);
    MainLayout->addWidget(m_treeWidget,1,0);
    MainLayout->setMargin(20);

	//创建菜单、菜单项
	/*this->m_tableBalance->setContextMenuPolicy(Qt::CustomContextMenu);
	m_RightPopMenu = new QMenu(this->m_tableBalance);
	QAction* updateAction = new QAction(QStringLiteral("刷新"),this);
	QAction* outputAction = new QAction(QStringLiteral("导出"),this); 
	m_RightPopMenu->addAction(updateAction);
	m_RightPopMenu->addAction(outputAction);
    //右键弹出菜单事件绑定
	connect(this->m_tableBalance,SIGNAL(customContextMenuRequested(const QPoint&)),this,SLOT(RightClickedMenuPop(const QPoint&)));*/
    connect(this->m_ConfirmButton,SIGNAL(clicked()),this,SLOT(OKButtonClicked()));
}
void MainAverageEntrustWidget::OKButtonClicked(){
    if(m_accountCombox->currentText() == ""){
        QMessageBox::critical(NULL,QString("ERROR"),QStringLiteral("请选择有效资金账号!"));
        return;
    }
    if(m_contractEdit->text() == ""){
        QMessageBox::critical(NULL,QString("ERROR"),QStringLiteral("请输入有效合约代码!"));
        return;
    }
    if(m_HandsSpinBox->text().toInt() == 0 || m_TotalHandsSpinBox->text().toInt() == 0){
        QMessageBox::critical(NULL,QString("ERROR"),QStringLiteral("请输入正确的手数信息!"));
        return;
    }
    if(m_timeEdit->text().toInt() == 0 ){
        QMessageBox::critical(NULL,QString("ERROR"),QStringLiteral("请输入正确的时间间隔!"));
        return;
    }

    /*AVERAGEORDERINFO typeOrder;
	strcpy_s(typeOrder.AccountID, m_accountCombox->currentText().toStdString().c_str());
	typeOrder.AveragePrice= 0;
	typeOrder.AverageStatus= 1;
	
	if (m_bsCombox->currentIndex() == 0){
		typeOrder.BS= 1;//买入
	}else{
		typeOrder.BS= 2;//卖出
	}
	strcpy_s(typeOrder.ContractID, m_contractEdit->text().toStdString().c_str());
	typeOrder.PerSendHands = m_HandsSpinBox->text().toInt();
	typeOrder.EnableStatus= 1;
	typeOrder.EntrustAmount= 0;
	typeOrder.iMsgType= 1;        //均价单

	if (m_ocCombox->currentIndex() == 0){
		typeOrder.OC = 1;//开仓
	}else if(m_ocCombox->currentIndex() == 1){
		typeOrder.OC = 2;//平仓
	}else{
		typeOrder.OC = 4;//平今仓
	}

	typeOrder.SendAmount= 0;
	typeOrder.TimeSpan= m_timeEdit->text().toInt();
	typeOrder.TotalAcount= m_TotalHandsSpinBox->text().toInt();
	typeOrder.TradeAmount= 0;
	if (m_OrderTypeCheckbox->isChecked()){
		typeOrder.Entrusttype = 0;//套保
	}else{
		typeOrder.Entrusttype = 1;//投机
	}
	strcpy_s(typeOrder.UserID, m_accountCombox->currentText().toStdString().c_str());

    emit NewAverageOrder(typeOrder);*/
}

void MainAverageEntrustWidget::SelectContract(const QModelIndex & index){
    int iRow = index.row();
    //合约
    QModelIndex oContract = index.sibling(iRow,0);
    QString strContract = oContract.data().toString();
    this->m_contractEdit->setText(strContract);

    this->m_HandsSpinBox->setValue(0);
    this->m_TotalHandsSpinBox->setValue(0);
    this->m_timeEdit->setText("0");
}
void MainAverageEntrustWidget::ShowAverageOrder(QString strBatchNo, CThostFtdcOrderField *pOrder){

	int iTopCnt = m_treeWidget->topLevelItemCount();
	for(int i = 0; i < iTopCnt; i ++){
		QTreeWidgetItem* Temp = m_treeWidget->topLevelItem(i);
		QString BatchNo    = Temp->text(0);
		if(strBatchNo == BatchNo){//判断均价单号是否一致
			int iChildCnt = Temp->childCount();
			for(int j = 0; j< iChildCnt; j++){
				QTreeWidgetItem* child = Temp->child(j);
				QString strOrderRef    = child->text(1);
				if(QString(pOrder->OrderRef) == strOrderRef ){//判断OrderRef是否一致
					child->setText(9,QString::fromLocal8Bit(pOrder->StatusMsg));
					return;
				}
			}
			QStringList columItemList;
			QTreeWidgetItem *child;
			columItemList<<QString(pOrder->UserID)<<QString(pOrder->OrderRef)<<QString(pOrder->OrderLocalID)<<QString(pOrder->InstrumentID);

			///买卖方向
			columItemList<<ComFunc::GetBSName(pOrder->Direction);
			//开平
			columItemList<<ComFunc::GetOCName(pOrder->CombOffsetFlag[0] );
			//委托价格
			columItemList<<QString::number(pOrder->LimitPrice);
			//委托数量
			columItemList<<QString::number(pOrder->VolumeTotalOriginal);
			//成交数量
			columItemList<<QString::number(pOrder->VolumeTraded);
			//状态
			columItemList<<QString::fromLocal8Bit(pOrder->StatusMsg);
			//委托时间
			columItemList<<QString(pOrder->TradingDay);
			//套保标识
			columItemList<<ComFunc::GetHedgeType(pOrder->CombHedgeFlag[0] );

			child = new QTreeWidgetItem(columItemList);
			Temp->addChild(child);
		}
	}
}


void MainAverageEntrustWidget::ShowAverageOrder(int icnt, void* pData){
	//CString str, sStatus, sAccount;
    /*AVERAGEORDERINFO* pOrderInfo = (AVERAGEORDERINFO*)pData;
	for(int i = 0; i < icnt; i++)
	{
		//--存储到队列中--		
		QString strKey = QString::number(pOrderInfo[i].OrderID); 
		STAverageInfo stAverage;
		stAverage.var0_AverageNumber= QString::number(pOrderInfo[i].OrderID); 
		stAverage.var10_sStatus= ComFunc::GetCofcoStatusName(pOrderInfo[i].AverageStatus); 
		stAverage.var11_sRunStatus= "开启";	
		stAverage.var1_szAccount= QString(pOrderInfo[i].AccountID); 
		stAverage.var2_sContractCode= QString(pOrderInfo[i].ContractID); 
		stAverage.var3_sBsName= ComFunc::GetBSName( pOrderInfo[i].BS ); 
		stAverage.var4_sOcName= ComFunc::GetOCName( pOrderInfo[i].OC ); 		
		stAverage.var8_sHedge= ComFunc::GetHedgeType(pOrderInfo[i].Entrusttype-1);  //投机套保，特殊处理一下		
		//stAverage.var15_sEntrustTime= CComFun::FormatCS(pOrderInfo[i].EntrustTime);   //取本地时间
		stAverage.var15_sEntrustTime= ComFunc::GetCurrTime(); 
		stAverage.var16_sGapStatus= "开始";
		stAverage.var17_sEntrustType= "限价";
		stAverage.bRspBusiness= TRUE;
		stAverage.bRspEntrust= TRUE;
		stAverage.bRspOrder=TRUE;

		stAverage.EachHand= pOrderInfo[i].PerSendHands; 
		stAverage.TotalHand= pOrderInfo[i].TotalAcount; 
		stAverage.TimeSpin= pOrderInfo[i].TimeSpan; 
		stAverage.SendHand= pOrderInfo[i].SendAmount; 
		stAverage.BusinessHand= pOrderInfo[i].TradeAmount; 
		stAverage.BussPrice= pOrderInfo[i].AveragePrice; 

		stAverage.UpPrice= 0; 
		stAverage.DownPrice= 0;

		m_MapAverageOrder[strKey] = stAverage;
		//显示在控件上	
		QStringList Items;
		Items<<stAverage.var0_AverageNumber<<stAverage.var1_szAccount<<stAverage.var2_sContractCode<<stAverage.var3_sBsName<<stAverage.var4_sOcName;
		QTreeWidgetItem* A = new QTreeWidgetItem(Items);
		m_treeWidget->addTopLevelItem(A);  
		
		//开启定时器
		//SetIfTimer(strKey);    暂不开启定时器  使用顺序下单模式
		CThostFtdcInputOrderField* OrderInfo = new CThostFtdcInputOrderField;
		memset(OrderInfo, 0, sizeof(CThostFtdcInputOrderField));
		///合约代码
		strncpy(OrderInfo->InstrumentID, stAverage.var2_sContractCode.toStdString().c_str(), sizeof(TThostFtdcInstrumentIDType));
		///买卖方向
		if(stAverage.var3_sBsName == "买入"  ){
			OrderInfo->Direction = THOST_FTDC_D_Buy;///买入
		}else {
			OrderInfo->Direction = THOST_FTDC_D_Sell;///卖出
		}
		if(stAverage.var4_sOcName == "开仓"){//开仓
			OrderInfo->CombOffsetFlag[0]=THOST_FTDC_OF_Open;
		}else if(stAverage.var4_sOcName == "平仓"){//平仓
			OrderInfo->CombOffsetFlag[0]=THOST_FTDC_OF_Close;
		}else{//平今仓
			OrderInfo->CombOffsetFlag[0]=THOST_FTDC_OF_CloseToday;
		}
		///组合投机套保标志 //0投机；1套保
		if (stAverage.var8_sHedge == "套保"){
			OrderInfo->CombHedgeFlag[0]=THOST_FTDC_CIDT_Hedge	;
		} else{
			OrderInfo->CombHedgeFlag[0]=THOST_FTDC_CIDT_Speculation	;
		}
		///数量
		OrderInfo->VolumeTotalOriginal = stAverage.EachHand;
		///成交量类型 任意数量
		OrderInfo->VolumeCondition = THOST_FTDC_VC_AV;
		///触发条件:立即
		OrderInfo->ContingentCondition = THOST_FTDC_CC_Immediately;

		///最小成交量 1
		OrderInfo->MinVolume = 1;
		///强平原因 非强平
		OrderInfo->ForceCloseReason = THOST_FTDC_FCC_NotForceClose;
		///自动挂起标志 否
		OrderInfo->IsAutoSuspend = 0;
		///用户强平标志 否
		OrderInfo->UserForceClose = 0;		
		///报单价格条件类型：任意价
		OrderInfo->OrderPriceType = THOST_FTDC_OPT_AnyPrice;
		///价格
		OrderInfo->LimitPrice = 0;
		///有效期类型：立即完成，否则撤销
		OrderInfo->TimeCondition =   THOST_FTDC_TC_IOC;

		//循环下单        
		for(int i = 0; i < stAverage.TotalHand ; i ++){
			//报单操作
			emit OnOrderInsert(OrderInfo,stAverage.var0_AverageNumber);
			Sleep(stAverage.TimeSpin);
		}
    }*/
}

void MainAverageEntrustWidget::SetIfTimer(QString AverageNo)
{
	QMap<QString, STAverageInfo>::iterator itrAvg;	
	itrAvg = m_MapAverageOrder.find(AverageNo);
	if (itrAvg != m_MapAverageOrder.end())
	{
		int iTime= itrAvg->TimeSpin*1000;
		emit SigStartTimer(AverageNo,iTime);
	}
}

void MainAverageEntrustWidget::timerEvent(QTimerEvent *e)
{
	int timerid = e->timerId();
	QString AverageNo = m_MapTimerID[timerid];
	
	QMap<QString, STAverageInfo>::iterator itrAvg;	

	itrAvg = m_MapAverageOrder.find(AverageNo);
	if (itrAvg != m_MapAverageOrder.end()){
		//获取交易所信息
		/*CString sExchange;
		sExchange= theApp.m_pDataService->GetContractExch(itrAvg->second.var2_sContractCode);	
		if (sExchange=="")   
		{
			CComFun::ShowMessage("获取不到交易所信息，下单失败1！");
			return;
		}
		CString csLog;
		csLog.Format(_T("Timer:[Gapstatus=%s; bRspBusiness=%d, bRspEntrust=%d, sEntrustType=%s"),
			itrAvg->second.var16_sGapStatus.GetString()
			, itrAvg->second.bRspBusiness
			, itrAvg->second.bRspEntrust
			, itrAvg->second.var17_sEntrustType.GetString()
			);
		theApp.LogMessage(csLog);*/
		//--判断条件-----
		//if ( theApp.ChectIfOpenMarket(sExchange) ){
			if (itrAvg->var16_sGapStatus=="开始"){
				SendOneOrderOfAvg(AverageNo);
			}else if (itrAvg->var16_sGapStatus=="委托"){
				if ((itrAvg->bRspBusiness) && (itrAvg->bRspEntrust) && (itrAvg->bRspOrder)){
					if (itrAvg->var17_sEntrustType=="限价"){
						itrAvg->var16_sGapStatus="开始";
					}else{
						SendOneOrderOfAvg(AverageNo);
					}
				}else if ((!itrAvg->bRspBusiness) && (itrAvg->bRspEntrust) && (itrAvg->bRspOrder)){
					//--委托状态，撤单，下单
					//WithdrawEntrustDataFromMap(itrAvg->var1_szAccount, itrAvg->var18_sEntrustNo);
					SendOneOrderOfAvg(AverageNo);
				}
			}
		//}			
	}
}

void MainAverageEntrustWidget::SlotStartTimer(QString AverageNo,int iTimeSpan){
	int timerID = QObject::startTimer(iTimeSpan,Qt::CoarseTimer);

	m_MapTimerID.insert(timerID,AverageNo);//将TimerID与均价单的编号存入Map中，建立对应关系
}

void MainAverageEntrustWidget::SendOneOrderOfAvg(QString AverageNo, double dPrice){
    /*QMap<QString, STAverageInfo>::iterator itrAvg;
	itrAvg = m_MapAverageOrder.find(AverageNo);
	if (itrAvg != m_MapAverageOrder.end())
	{
		//--先更新状态---
        /*itrAvg->var16_sGapStatus="委托";
		itrAvg->var18_sEntrustNo="";
		itrAvg->bRspOrder=FALSE;
		itrAvg->bRspEntrust=FALSE;
		itrAvg->bRspBusiness=FALSE;

		if (dPrice > 0.1)    //限价
			itrAvg->var17_sEntrustType="限价";
		else
            itrAvg->var17_sEntrustType="市价";

//----下面委托-----
		//判断交易所信息
		//CString sExchange;
		//sExchange= theApp.m_pDataService->GetContractExch(itrAvg->var2_sContractCode);	
		//if (sExchange==""){
			//CComFun::ShowMessage("获取不到交易所信息，下单失败2！");
			//return;
		//}

		CThostFtdcInputOrderField OrderInfo;
		memset(&OrderInfo, 0, sizeof(CThostFtdcInputOrderField));

		///合约代码
		strncpy(OrderInfo.InstrumentID, itrAvg->var2_sContractCode.toStdString().c_str(), sizeof(TThostFtdcInstrumentIDType));
		///买卖方向
		if(itrAvg->var3_sBsName == "买入"  ){
			OrderInfo.Direction = THOST_FTDC_D_Buy;///买入
		}else {
			OrderInfo.Direction = THOST_FTDC_D_Sell;///卖出
		}
		if(itrAvg->var4_sOcName == "开仓"){//开仓
			OrderInfo.CombOffsetFlag[0]='0';
		}else if(itrAvg->var4_sOcName == "平仓"){//平仓
			OrderInfo.CombOffsetFlag[0]='1';
		}else{//平今仓
			OrderInfo.CombOffsetFlag[0]='3';
		}
		///组合投机套保标志 //0投机；1套保
		if (itrAvg->var8_sHedge == "套保"){
			OrderInfo.CombHedgeFlag[0]='1'	;
		} else{
			OrderInfo.CombHedgeFlag[0]='0'	;
		}
		///数量
		OrderInfo.VolumeTotalOriginal = itrAvg->EachHand;
		///成交量类型 任意数量
		OrderInfo.VolumeCondition = THOST_FTDC_VC_AV;
		///触发条件:立即
		OrderInfo.ContingentCondition = THOST_FTDC_CC_Immediately;

		///最小成交量 1
		OrderInfo.MinVolume = 1;
		///强平原因 非强平
		OrderInfo.ForceCloseReason = THOST_FTDC_FCC_NotForceClose;
		///自动挂起标志 否
		OrderInfo.IsAutoSuspend = 0;
		///用户强平标志 否
		OrderInfo.UserForceClose = 0;		

		if (dPrice > 0.1)  //限价
		{
			//取得最小变动价
			double dLessPrice = 0.1;
			//double dLessPrice= theApp.m_pDataService->GetContractLestPrice(itrAvg->second.var2_sContractCode);
			double dtruePrice;
			if (itrAvg->var3_sBsName=="买入")
			{
				dtruePrice= dPrice+5*dLessPrice;
				if (dtruePrice> itrAvg->UpPrice )
				{
					dtruePrice= itrAvg->UpPrice;
				}				
			}
			else
			{
				dtruePrice= dPrice-5*dLessPrice;
				if (dtruePrice< itrAvg->DownPrice )
				{
					dtruePrice= itrAvg->DownPrice;
				}				
			}
			///报单价格条件类型：限价
			OrderInfo.OrderPriceType = THOST_FTDC_OPT_LimitPrice;
			///价格
			OrderInfo.LimitPrice = dtruePrice;
			///有效期类型：当日有效
			OrderInfo.TimeCondition =   THOST_FTDC_TC_GFD;
		}else{   //市价
			//if ((sExchange=="F3") || (sExchange=="F4") ){
                if (itrAvg->var3_sBsName=="买入"){
					if (itrAvg->UpPrice< 0.1 ){
						//一直未收到最新报价，本次不下单
						itrAvg->var16_sGapStatus="开始";
						itrAvg->var18_sEntrustNo="";
						itrAvg->bRspOrder=TRUE;
						itrAvg->bRspEntrust=TRUE;
						itrAvg->bRspBusiness=TRUE;
						itrAvg->var17_sEntrustType="限价";
						return ;
					}
					//csPrice = QString::number(itrAvg->UpPrice)  ;
				}else{
					if (itrAvg->DownPrice  < 0.1 ){
						//csMsg.Format(_T("合约[%s]一直未收到最新报价，本次不下单"), itrAvg->second.var2_sContractCode.GetString());
						itrAvg->var16_sGapStatus="开始";
						itrAvg->var18_sEntrustNo="";
						itrAvg->bRspOrder=TRUE;
						itrAvg->bRspEntrust=TRUE;
						itrAvg->bRspBusiness=TRUE;
						itrAvg->var17_sEntrustType="限价";
						return;
                    }
					//csPrice = QString::number(itrAvg->DownPrice)  ;	
					
				}
				///报单价格条件类型：任意价
				OrderInfo.OrderPriceType = THOST_FTDC_OPT_AnyPrice;
				///价格
				OrderInfo.LimitPrice = 0;
				///有效期类型：立即完成，否则撤销
				OrderInfo.TimeCondition =   THOST_FTDC_TC_IOC;
			//}
			//else{
				//csPrice = "0";	
			//}			
		}
        //emit OnOrderInsert(&OrderInfo,AverageNo);
		//int nKeyID = theApp.ExecQuerryHS(param, MSG_TYPE_NEW_SINGLE_ORDER); 
		//if (nKeyID> 0)
		//{
			//theApp.AddPostPath(nKeyID, this);
			//STResultInfo stResult;
			//stResult.account= itrAvg->second.var1_szAccount;
			//stResult.client_name= AverageNo;   //此处保存均价号
			//stResult.iHsType= HS_ENTRUST;
			//m_mapHsResult[nKeyID]= stResult;	
		//}

    }*/
}
