﻿#include "PopMktCombineExch.h"

PopMktCombineExch::PopMktCombineExch(QWidget *parent) :
    QWidget(parent)
{
    setupModel();
    setupViews();
}

void PopMktCombineExch::setupModel()
{
    modelPopMktCombineExch = new QStandardItemModel(0, 11, this);
    modelPopMktCombineExch->setHeaderData(0, Qt::Horizontal, QStringLiteral("组合代码"));
    modelPopMktCombineExch->setHeaderData(1, Qt::Horizontal, QStringLiteral("买量"));
    modelPopMktCombineExch->setHeaderData(2, Qt::Horizontal, QStringLiteral("买价"));
    modelPopMktCombineExch->setHeaderData(3, Qt::Horizontal, QStringLiteral("卖价"));
    modelPopMktCombineExch->setHeaderData(4, Qt::Horizontal, QStringLiteral("卖量"));
    modelPopMktCombineExch->setHeaderData(5, Qt::Horizontal, QStringLiteral("最高"));
    modelPopMktCombineExch->setHeaderData(6, Qt::Horizontal, QStringLiteral("最低"));
    modelPopMktCombineExch->setHeaderData(7, Qt::Horizontal, QStringLiteral("涨停板"));
    modelPopMktCombineExch->setHeaderData(8, Qt::Horizontal, QStringLiteral("跌停板"));
    modelPopMktCombineExch->setHeaderData(9, Qt::Horizontal, QStringLiteral("全部买量"));
    modelPopMktCombineExch->setHeaderData(10, Qt::Horizontal, QStringLiteral("全部卖量"));
}

void PopMktCombineExch::setupViews()
{
    MainLayout = new QGridLayout(this);

	QHBoxLayout* layout = new QHBoxLayout;
	QLabel* StrategyLabel = new QLabel(QStringLiteral("策略名称"));
	QComboBox* m_StrategyCombox = new QComboBox;

	QLabel* contractLabel = new QLabel(QStringLiteral("合约名称"));
	QLineEdit* m_contractEdit = new QLineEdit;

	QLabel* handsLabel = new QLabel(QStringLiteral("每次下单手数"));
	QSpinBox* m_HandsSpinBox = new QSpinBox;                       //创建SpinBox
	m_HandsSpinBox->setRange(0, 10000);                              //设置spinBox的值范围

	QPushButton* m_ConfirmButton = new QPushButton(QStringLiteral("确定"));

	StrategyLabel->setFixedWidth(60);
	m_StrategyCombox->setFixedWidth(80);
	contractLabel->setFixedWidth(60);
	m_contractEdit->setFixedWidth(80);
	handsLabel->setFixedWidth(80);
	m_HandsSpinBox->setFixedWidth(80);
	m_ConfirmButton->setFixedWidth(80);

	layout->addWidget(StrategyLabel);
	layout->addWidget(m_StrategyCombox);
	layout->addWidget(contractLabel);
	layout->addWidget(m_contractEdit);
	layout->addWidget(handsLabel);
	layout->addWidget(m_HandsSpinBox);
	layout->addWidget(m_ConfirmButton);
	layout->addStretch(6);

    tablePopMktCombineExch = new QTableView;
    tablePopMktCombineExch->setAlternatingRowColors(true);
    QFont font = tablePopMktCombineExch->horizontalHeader()->font();
    font.setBold(true);
    tablePopMktCombineExch->horizontalHeader()->setFont(font);
    tablePopMktCombineExch->setModel(modelPopMktCombineExch);
    tablePopMktCombineExch->setEditTriggers(QAbstractItemView::NoEditTriggers);
    tablePopMktCombineExch->verticalHeader()->setVisible(false); //隐藏列表头
    tablePopMktCombineExch->verticalHeader()->setFixedWidth(40);
    tablePopMktCombineExch->setSelectionBehavior(QAbstractItemView::SelectRows);
    tablePopMktCombineExch->setSelectionMode(QAbstractItemView::SingleSelection);

    tablePopMktCombineExch->setColumnWidth(0, 100);
    tablePopMktCombineExch->setColumnWidth(1, 120);
    tablePopMktCombineExch->setColumnWidth(2, 160);
    tablePopMktCombineExch->setColumnWidth(3, 120);
    tablePopMktCombineExch->setColumnWidth(4, 50);
    tablePopMktCombineExch->setColumnWidth(5, 70);
    tablePopMktCombineExch->setColumnWidth(6, 50);
    tablePopMktCombineExch->setColumnWidth(7, 150);
    tablePopMktCombineExch->setColumnWidth(8, 100);
    tablePopMktCombineExch->setColumnWidth(9, 85);
    tablePopMktCombineExch->setColumnWidth(10, 50);

	MainLayout->addLayout(layout,0,0);
    MainLayout->addWidget(tablePopMktCombineExch,1,0);
    MainLayout->setMargin(20);

	//创建菜单、菜单项
	this->tablePopMktCombineExch->setContextMenuPolicy(Qt::CustomContextMenu);
	m_RightPopMenu = new QMenu(this->tablePopMktCombineExch);
	m_deleteOrderAction = new QAction(QStringLiteral("撤单"),this); 
	m_updateAction = new QAction(QStringLiteral("刷新"),this); 	
	m_outputAction = new QAction(QStringLiteral("导出"),this); 
	m_RightPopMenu->addAction(m_deleteOrderAction);
	m_RightPopMenu->addAction(m_updateAction);	
	m_RightPopMenu->addAction(m_outputAction);

	//右键弹出菜单事件绑定
	connect(this->tablePopMktCombineExch,SIGNAL(customContextMenuRequested(const QPoint&)),this,SLOT(RightClickedMenuPop(const QPoint&)));
}
/************************************************************************/
/*右键弹出菜单响应函数                                                  */
/************************************************************************/
void PopMktCombineExch::RightClickedMenuPop(const QPoint& pos)
{
	m_RightPopMenu->exec(QCursor::pos());
}