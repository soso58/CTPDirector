#include "selfmarketdialog.h"
#include "ui_selfmarketdialog.h"
#include<QMessageBox>

SelfMarketDialog::SelfMarketDialog(void* pMainwindow,QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SelfMarketDialog)
{
    ui->setupUi(this);
    ui->comboBox_Exchange->addItem("郑州交易所");
    ui->comboBox_Exchange->addItem("大连交易所");
    ui->comboBox_Exchange->addItem("上海交易所");
    ui->comboBox_Exchange->addItem("金融交易所");
	ui->comboBox_Exchange->addItem("显示全部");

	ui->tableWidget_Contracts->setSelectionBehavior(QAbstractItemView::SelectRows);
	ui->tableWidget_Contracts->verticalHeader()->setVisible(false);
    ui->tableWidget_Contracts->setShowGrid(true);//显示表格线
    ui->tableWidget_Contracts->setSelectionMode ( QAbstractItemView::SingleSelection); //设置选择模式，选择单行
    ui->tableWidget_Contracts->setEditTriggers(QAbstractItemView::NoEditTriggers);//禁止编辑
    //ui->tableWidget_Contracts->horizontalHeader()->setStretchLastSection(true);//该命令只是将最后一行的列宽度等于Table剩下的宽度

    ui->tableWidget_Selected->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->tableWidget_Selected->verticalHeader()->setVisible(false);
    ui->tableWidget_Selected->setShowGrid(true);//显示表格线
    ui->tableWidget_Selected->setSelectionMode ( QAbstractItemView::SingleSelection); //设置选择模式，选择单行
    ui->tableWidget_Selected->setEditTriggers(QAbstractItemView::NoEditTriggers);//禁止编辑
    ui->tableWidget_Selected->horizontalHeader()->setStretchLastSection(true);//该命令只是将最后一行的列宽度等于Table剩下的宽度

    connect(ui->comboBox_Exchange,SIGNAL(currentTextChanged(const QString &)),this,SLOT(UpdateExchange(const QString &)));
	//添加按钮响应事件
    connect(ui->Button_Add,SIGNAL(clicked()),this,SLOT(ButtonAddClicked()));
    //删除按钮响应事件
    connect(ui->Button_Del,SIGNAL(clicked()),this,SLOT(ButtonDelClicked()));
	//上移按钮响应事件
	connect(ui->Button_Up,SIGNAL(clicked()),this,SLOT(ButtonUpClicked()));
    //下移按钮响应事件
    connect(ui->Button_Down,SIGNAL(clicked()),this,SLOT(ButtonDownClicked()));

    SetMainDlg(pMainwindow);
}

SelfMarketDialog::~SelfMarketDialog()
{
    delete ui;
}

void SelfMarketDialog::SetMainDlg(void* pMainwindow){
	m_pMainwindow = pMainwindow;
    SetContractTable();
    UpdateExchange(ui->comboBox_Exchange->currentText());
}

void SelfMarketDialog::SetContractTable(){
	ui->tableWidget_Selected->setRowCount(0);
    int iCnt = ((MainWindow*)m_pMainwindow)->m_ContractVector.size();
    for(int i = 0; i < iCnt; i++){
        CThostFtdcInstrumentField oInstrument = ((MainWindow*)m_pMainwindow)->m_ContractVector[i];
        ui->tableWidget_Selected->insertRow(i); //插入新行
        ui->tableWidget_Selected->setItem(i,0,new QTableWidgetItem(oInstrument.InstrumentID));
        ui->tableWidget_Selected->setItem(i,1,new QTableWidgetItem(oInstrument.InstrumentName));
    }
    //根据内容调整列宽,给Table填充数据之后再调用该函数进行Resize
    ui->tableWidget_Selected->resizeColumnsToContents();
}

void SelfMarketDialog::UpdateExchange(const QString & text){

	ui->tableWidget_Contracts->setRowCount(0);

    if(text == "郑州交易所" || text == "显示全部" ){
        int iRow = ui->tableWidget_Contracts->rowCount();
        for (QMap<QString,CThostFtdcInstrumentField>::iterator it = ((MainWindow*)m_pMainwindow)->m_CZCEInstrumentMap.begin();
             it != ((MainWindow*)m_pMainwindow)->m_CZCEInstrumentMap.end(); ++it ) {
            ui->tableWidget_Contracts->insertRow(iRow); //插入新行
            ui->tableWidget_Contracts->setItem(iRow,0,new QTableWidgetItem(it.key()));
			QString strInstrumentName = QString::fromLocal8Bit(it.value().InstrumentName);  
            ui->tableWidget_Contracts->setItem(iRow,1,new QTableWidgetItem(strInstrumentName));
			iRow++;
        }
    }
	if (text == "大连交易所" || text == "显示全部" ){
		int iRow = ui->tableWidget_Contracts->rowCount();
		for (QMap<QString,CThostFtdcInstrumentField>::iterator it = ((MainWindow*)m_pMainwindow)->m_DCEInstrumentMap.begin();
			it != ((MainWindow*)m_pMainwindow)->m_DCEInstrumentMap.end(); ++it ) {
				ui->tableWidget_Contracts->insertRow(iRow); //插入新行
				ui->tableWidget_Contracts->setItem(iRow,0,new QTableWidgetItem(it.key()));
                QString strInstrumentName = QString::fromLocal8Bit(it.value().InstrumentName);
                ui->tableWidget_Contracts->setItem(iRow,1,new QTableWidgetItem(strInstrumentName));
				iRow++;
		}
    } 
    if (text == "上海交易所" || text == "显示全部" ){
		int iRow = ui->tableWidget_Contracts->rowCount();
		for (QMap<QString,CThostFtdcInstrumentField>::iterator it = ((MainWindow*)m_pMainwindow)->m_SHFEInstrumentMap.begin();
			it != ((MainWindow*)m_pMainwindow)->m_SHFEInstrumentMap.end(); ++it ) {
				ui->tableWidget_Contracts->insertRow(iRow); //插入新行
				ui->tableWidget_Contracts->setItem(iRow,0,new QTableWidgetItem(it.key()));
                QString strInstrumentName = QString::fromLocal8Bit(it.value().InstrumentName);
                ui->tableWidget_Contracts->setItem(iRow,1,new QTableWidgetItem(strInstrumentName));
				iRow++;
		}
    }
	if (text == "金融交易所" || text == "显示全部" ){
		int iRow = ui->tableWidget_Contracts->rowCount();
		for (QMap<QString,CThostFtdcInstrumentField>::iterator it = ((MainWindow*)m_pMainwindow)->m_CFFEXInstrumentMap.begin();
			it != ((MainWindow*)m_pMainwindow)->m_CFFEXInstrumentMap.end(); ++it ) {
				ui->tableWidget_Contracts->insertRow(iRow); //插入新行
				ui->tableWidget_Contracts->setItem(iRow,0,new QTableWidgetItem(it.key()));
                QString strInstrumentName = QString::fromLocal8Bit(it.value().InstrumentName);
                ui->tableWidget_Contracts->setItem(iRow,1,new QTableWidgetItem(strInstrumentName));
				iRow++;
		}
    }
    //根据内容调整列宽,给Table填充数据之后再调用该函数进行Resize
    ui->tableWidget_Contracts->resizeColumnsToContents();
}
///确认按钮响应事件
void SelfMarketDialog::OKButtonClicked(){
    //QMessageBox::information(NULL,QString("INFO"),QString("SelfMarketDialog OK Button Clicked"));

    ((MainWindow*)m_pMainwindow)->m_ContractVector.clear();
    int iRowCnt = ui->tableWidget_Selected->rowCount();

    for(int i = 0; i < iRowCnt; i++){
        QString valueID =ui->tableWidget_Selected->item(i,0)->text();
        QString valueName =ui->tableWidget_Selected->item(i,1)->text();
        if(valueID != ""){
            CThostFtdcInstrumentField oInstrument;
            strncpy_s(oInstrument.InstrumentID,sizeof(TThostFtdcInstrumentIDType),valueID.toStdString().c_str(),sizeof(TThostFtdcInstrumentIDType));
            strncpy_s(oInstrument.InstrumentName,sizeof(TThostFtdcInstrumentNameType),valueName.trimmed().toStdString().c_str(),sizeof(TThostFtdcInstrumentNameType));
            ((MainWindow*)m_pMainwindow)->m_ContractVector.push_back(oInstrument);
        }
    }

    emit SubscribeMarketData();
    emit destroyWin();
}
///添加按钮响应事件
void SelfMarketDialog::ButtonAddClicked(){
	QList<QTableWidgetItem *>  ItemsSelected  = ui->tableWidget_Contracts->selectedItems();
    if(0 == ItemsSelected.size() ){//判断是否有选中的Item
        QMessageBox::warning(NULL,QString("WARNNING"),QString("Please select a valid Item"));
    }else{
		//选中的行号
        int iRow = ItemsSelected.at(0)->row();
		//合约代码
        QString strContractCode = ui->tableWidget_Contracts->item(iRow,0)->text();
		//合约名称
        QString strContractName = ui->tableWidget_Contracts->item(iRow,1)->text();

        //判断该合约是否已被选择
        int iRowCnt = ui->tableWidget_Selected->rowCount();
        for(int i = 0; i < iRowCnt; i++){
            QString strContractCodeSeclected = ui->tableWidget_Selected->item(i,0)->text();
            if(strContractCode == strContractCodeSeclected){
                return;
            }
        }
        //将选中的合约内容添加至已选合约列表中
        ui->tableWidget_Selected->insertRow(iRowCnt);
        ui->tableWidget_Selected->setItem(iRowCnt,0,new QTableWidgetItem(strContractCode));
        ui->tableWidget_Selected->setItem(iRowCnt,1,new QTableWidgetItem(strContractName));
    }
}
///删除按钮响应事件
void SelfMarketDialog::ButtonDelClicked(){
    QList<QTableWidgetItem *>  ItemsSelected  = ui->tableWidget_Selected->selectedItems();
    if(0 == ItemsSelected.size() ){//判断已选合约中是否有选中的Item
        QMessageBox::warning(NULL,QString("WARNNING"),QString("Please select a valid Item"));
    }else{
        //选中的行号
        int iRow = ItemsSelected.at(0)->row();
        //删除选中的行
        ui->tableWidget_Selected->removeRow(iRow);
    }
}
///上移按钮响应事件
void SelfMarketDialog::ButtonUpClicked(){
    QList<QTableWidgetItem *>  ItemsSelected  = ui->tableWidget_Selected->selectedItems();
    if(0 == ItemsSelected.size() ){//判断已选合约中是否有选中的Item
        QMessageBox::warning(NULL,QString("WARNNING"),QString("Please select a valid Item"));
    }else{
        //选中的行号
        int iRow = ItemsSelected.at(0)->row();
        if(0 != iRow ){
            ///上一行内容
            //合约代码
            QString strContractCode = ui->tableWidget_Selected->item(iRow-1,0)->text();
            //合约名称
            QString strContractName = ui->tableWidget_Selected->item(iRow-1,1)->text();

            ///选中行的内容
            //合约代码
            QString strContractCodeSelected = ui->tableWidget_Selected->item(iRow,0)->text();
            //合约名称
            QString strContractNameSelected = ui->tableWidget_Selected->item(iRow,1)->text();

            ui->tableWidget_Selected->setItem(iRow -1 , 0 ,new QTableWidgetItem(strContractCodeSelected));
            ui->tableWidget_Selected->setItem(iRow -1 , 1 ,new QTableWidgetItem(strContractNameSelected));
            ui->tableWidget_Selected->setItem(iRow , 0 ,new QTableWidgetItem(strContractCode));
            ui->tableWidget_Selected->setItem(iRow , 1 ,new QTableWidgetItem(strContractName));
            ui->tableWidget_Selected->selectRow(iRow -1);
        }
    }
}
///下移按钮响应事件
void SelfMarketDialog::ButtonDownClicked(){
    QList<QTableWidgetItem *>  ItemsSelected  = ui->tableWidget_Selected->selectedItems();
    if(0 == ItemsSelected.size() ){//判断已选合约中是否有选中的Item
        QMessageBox::warning(NULL,QString("WARNNING"),QString("Please select a valid Item"));
    }else{
        //行数
        int iCnt = ui->tableWidget_Selected->rowCount();
        //选中的行号
        int iRow = ItemsSelected.at(0)->row();
        if(iCnt != iRow + 1  ){
            ///下一行内容
            //合约代码
            QString strContractCode = ui->tableWidget_Selected->item(iRow+1,0)->text();
            //合约名称
            QString strContractName = ui->tableWidget_Selected->item(iRow+1,1)->text();
            ///选中行的内容
            //合约代码
            QString strContractCodeSelected = ui->tableWidget_Selected->item(iRow,0)->text();
            //合约名称
            QString strContractNameSelected = ui->tableWidget_Selected->item(iRow,1)->text();

            ui->tableWidget_Selected->setItem(iRow +1 , 0 ,new QTableWidgetItem(strContractCodeSelected));
            ui->tableWidget_Selected->setItem(iRow +1 , 1 ,new QTableWidgetItem(strContractNameSelected));
            ui->tableWidget_Selected->setItem(iRow , 0 ,new QTableWidgetItem(strContractCode));
            ui->tableWidget_Selected->setItem(iRow , 1 ,new QTableWidgetItem(strContractName));
            ui->tableWidget_Selected->selectRow(iRow +1);
        }
    }
}
