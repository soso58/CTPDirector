#ifndef COMBINEORDERDIALOG_H
#define COMBINEORDERDIALOG_H

#include <QDialog>
#include <QTableWidgetItem>

namespace Ui {
class CombineOrderDialog;
}

class CombineOrderDialog : public QDialog
{
    Q_OBJECT

public:
    explicit CombineOrderDialog(QWidget *parent = 0);
    ~CombineOrderDialog();
signals:
    void destroyWin();
public slots:
    void OKButtonClicked();
    void ButtonCommitClicked();
    void ButtonDelClicked();
    void ItemClicked(QTableWidgetItem* pItem);

private:
    Ui::CombineOrderDialog *ui;
};

#endif // COMBINEORDERDIALOG_H
