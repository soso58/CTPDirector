/************************************
*************************************
       自定义组合行情显示页面
*************************************
*************************************/

#ifndef MARKETCOMEXCH_H
#define MARKETCOMEXCH_H

#include <QWidget>
#include <qstandarditemmodel.h>
#include <qabstractitemmodel.h>
#include <string.h>
#include <qtableview.h>
#include <qheaderview.h>
#include <qgridlayout.h>
#include"CtpInterface/ThostFtdcUserApiStruct.h"
#include"tableview.h"

class MarketComexch : public QWidget
{
    Q_OBJECT
public:
    explicit MarketComexch(QWidget *parent = 0);

signals:
    void UpdateRow(int iIndex,QStringList row_list);
	void SigCombineOrder(QString Leg1,QString Leg2,int buy_high_amount,QString csBuyPrice,int sale_low_amount,QString csSellPrice);

public slots:
    void ShowDepthMarketData(CThostFtdcDepthMarketDataField *pFirstLeg,CThostFtdcDepthMarketDataField *pSecondLeg);

public:
    void setupModel();
    void setupViews();


public:
    TableView*  m_tableView;
    QItemDelegate* m_MarketselfDelegate;

private:
    //QItemSelectionModel *selectionModel;
    QGridLayout *MainLayout;
};

#endif // MARKETCOMEXCH_H
