﻿#include "settlementinfodialog.h"
#include "ui_settlementinfodialog.h"

SettlementInfoDialog::SettlementInfoDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SettlementInfoDialog)
{
    ui->setupUi(this);
    ui->m_textEdit->setReadOnly(true);
    ui->m_textEdit->setWordWrapMode(QTextOption::NoWrap);
    setWindowTitle(QStringLiteral("投资者结算结果"));
    connect(ui->m_OKButton,SIGNAL(clicked()),this,SLOT(SlotOKButtonClicked()));
    connect(ui->m_CancelButton,SIGNAL(clicked()),this,SLOT(SlotCancelButtonClicked()));
}

SettlementInfoDialog::~SettlementInfoDialog()
{
    delete ui;
}
void SettlementInfoDialog::SlotOKButtonClicked(){
    this->accept();
    this->close();
}
void SettlementInfoDialog::SlotCancelButtonClicked(){
    this->reject();
    this->close();
}
void SettlementInfoDialog::OutputContent(QString content){
    //ui->m_textEdit->insertPlainText(content);
    ui->m_textEdit->append(content);
    ui->m_textEdit->moveCursor(QTextCursor::Start);
}
