﻿#include "mainbalancewidget.h"

MainBalanceWidget::MainBalanceWidget(QWidget *parent) :
    QWidget(parent)
{
    setupModel();
    setupViews();
}

void MainBalanceWidget::setupModel()
{
    m_modelBalance = new QStandardItemModel(0, 10, this);
    m_modelBalance->setHeaderData(0, Qt::Horizontal, QStringLiteral("资金账号"));
    m_modelBalance->setHeaderData(1, Qt::Horizontal, QStringLiteral("期货结算准备金"));
    m_modelBalance->setHeaderData(2, Qt::Horizontal, QStringLiteral("可用资金"));
    m_modelBalance->setHeaderData(3, Qt::Horizontal, QStringLiteral("可取资金"));
    m_modelBalance->setHeaderData(4, Qt::Horizontal, QStringLiteral("交易日"));
    m_modelBalance->setHeaderData(5, Qt::Horizontal, QStringLiteral("结算编号"));
    m_modelBalance->setHeaderData(6, Qt::Horizontal, QStringLiteral("信用额度"));
    m_modelBalance->setHeaderData(7, Qt::Horizontal, QStringLiteral("质押金额"));
    m_modelBalance->setHeaderData(8, Qt::Horizontal, QStringLiteral("交易所保证金"));
    m_modelBalance->setHeaderData(9, Qt::Horizontal, QStringLiteral("币种代码"));
}

void MainBalanceWidget::setupViews()
{
    m_hboxlayout = new QHBoxLayout;
    QLabel* accountLabel = new QLabel(QStringLiteral("资金账号"));
    m_accountCombox = new QComboBox;
    m_querybutton = new QPushButton(QStringLiteral("查询"));
    m_exportbutton = new QPushButton(QStringLiteral("导出"));
    accountLabel->setFixedWidth(80);
    m_accountCombox->setFixedWidth(100);
    m_querybutton->setFixedWidth(80);
    m_exportbutton->setFixedWidth(80);

    m_hboxlayout->addWidget(accountLabel);
    m_hboxlayout->addWidget(m_accountCombox);
    m_hboxlayout->addWidget(m_querybutton);
    m_hboxlayout->addWidget(m_exportbutton);
    m_hboxlayout->addStretch();
    m_hboxlayout->addStretch();

    m_MainLayout = new QGridLayout(this);

    m_tableBalance = new QTableView;
    m_tableBalance->setAlternatingRowColors(true);
    QFont font = m_tableBalance->horizontalHeader()->font();
    font.setBold(true);
    m_tableBalance->horizontalHeader()->setFont(font);
//    tableOrder->setStyleSheet("QTableView::item:selected { selection-color: rgb(0, 0, 0) }" "QTableView::item:selected { background-color: rgb(255, 255, 0) }"
//                "QTableView{background-color: rgb(0, 0, 0);" "alternate-background-color: rgb(41, 36, 33);}");

    m_tableBalance->setModel(m_modelBalance);
    m_tableBalance->setEditTriggers(QAbstractItemView::NoEditTriggers);
    m_tableBalance->verticalHeader()->setVisible(false); //隐藏列表头
    m_tableBalance->verticalHeader()->setFixedWidth(40);
    m_tableBalance->setSelectionBehavior(QAbstractItemView::SelectRows);
    m_tableBalance->setSelectionMode(QAbstractItemView::SingleSelection);

    m_tableBalance->setColumnWidth(0, 100);
    m_tableBalance->setColumnWidth(1, 100);
    m_tableBalance->setColumnWidth(2, 100);
    m_tableBalance->setColumnWidth(3, 120);
    m_tableBalance->setColumnWidth(4, 100);
    m_tableBalance->setColumnWidth(5, 100);
    m_tableBalance->setColumnWidth(6, 100);
    m_tableBalance->setColumnWidth(7, 100);
    m_tableBalance->setColumnWidth(8, 100);
    m_tableBalance->setColumnWidth(9, 100);
    m_tableBalance->setColumnWidth(10, 100);

    m_MainLayout->addLayout(m_hboxlayout,0,0);
    m_MainLayout->addWidget(m_tableBalance,1,0);
    m_MainLayout->setMargin(20);

	//创建菜单、菜单项
	this->m_tableBalance->setContextMenuPolicy(Qt::CustomContextMenu);
	m_RightPopMenu = new QMenu(this->m_tableBalance);
    m_updateAction = new QAction(QStringLiteral("刷新"),this);
    m_outputAction = new QAction(QStringLiteral("导出"),this);
    m_RightPopMenu->addAction(m_updateAction);
    m_RightPopMenu->addAction(m_outputAction);
    //右键弹出菜单事件绑定
	connect(this->m_tableBalance,SIGNAL(customContextMenuRequested(const QPoint&)),this,SLOT(RightClickedMenuPop(const QPoint&)));
	//查询按钮事件绑定
	connect(m_querybutton,SIGNAL(clicked()),this,SLOT(QueryButtonClicked()));
    //刷新事件绑定
    connect(m_updateAction,SIGNAL(triggered()),this,SLOT(QueryButtonClicked()));
    //导出事件绑定
    connect(m_outputAction,SIGNAL(triggered()),this,SLOT(QueryButtonClicked()));
}
///右键弹出菜单响应函数
void MainBalanceWidget::RightClickedMenuPop(const QPoint& pos)
{
	m_RightPopMenu->exec(QCursor::pos());
}
void MainBalanceWidget::QueryButtonClicked(){
    m_modelBalance->removeRows(0,m_modelBalance->rowCount());
	emit ReqQryTradingAccount(m_accountCombox->currentText());
}
void MainBalanceWidget::ShowTradingAccount(CThostFtdcTradingAccountField *pTradingAccount){
	int iRow = m_modelBalance->rowCount();
	int iIndex = iRow;
	for (int i = 0; i < iRow ; i++){
		QString strAccountID =  ((QStandardItemModel*) m_modelBalance)->item(i,0)->text();
		if (0 == strcmp(strAccountID.toStdString().c_str(),pTradingAccount->AccountID)){
			iIndex = i;
			break;
		}
	}

	///资金账号
	((QStandardItemModel*) m_modelBalance)->setItem(iIndex, 0, new QStandardItem(pTradingAccount->AccountID));
	///期货结算准备金
	((QStandardItemModel*) m_modelBalance)->setItem(iIndex, 1, new QStandardItem(QString::number(pTradingAccount->Balance,'f')));
	///可用资金
	((QStandardItemModel*) m_modelBalance)->setItem(iIndex, 2, new QStandardItem(QString::number(pTradingAccount->Available,'f')));
	///可取资金
	((QStandardItemModel*) m_modelBalance)->setItem(iIndex, 3, new QStandardItem(QString::number(pTradingAccount->WithdrawQuota,'f')));
	///交易日
	((QStandardItemModel*) m_modelBalance)->setItem(iIndex, 4, new QStandardItem(pTradingAccount->TradingDay));
	///结算编号
	((QStandardItemModel*) m_modelBalance)->setItem(iIndex, 5, new QStandardItem(QString::number(pTradingAccount->SettlementID)));
	///信用额度
	((QStandardItemModel*) m_modelBalance)->setItem(iIndex, 6, new QStandardItem(QString::number(pTradingAccount->Credit,'f')));
	///质押金额
	((QStandardItemModel*) m_modelBalance)->setItem(iIndex, 7, new QStandardItem(QString::number(pTradingAccount->Mortgage,'f')));
	///交易所保证金
	((QStandardItemModel*) m_modelBalance)->setItem(iIndex, 8, new QStandardItem(QString::number(pTradingAccount->ExchangeMargin,'f')));
	///币种代码
	((QStandardItemModel*) m_modelBalance)->setItem(iIndex, 9, new QStandardItem(pTradingAccount->CurrencyID));
}
