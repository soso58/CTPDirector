#ifndef MAINCOMBINEENTRUSTWIDGET_H
#define MAINCOMBINEENTRUSTWIDGET_H

#include <QWidget>
#include <string.h>
#include <qtableview.h>
#include <qheaderview.h>
#include <qgridlayout.h>
#include <QTreeWidget.h>
#include "CommUtil/common.h"
#include "CommUtil/comfunc.h"
#include "CtpInterface/ThostFtdcUserApiStruct.h"

class MainCombineEntrustWidget : public QWidget
{
    Q_OBJECT
public:
    explicit MainCombineEntrustWidget(QWidget *parent = 0);

signals:

public slots:
public:
    void setupViews();
	void ShowCombineOrder(int icnt, void* pData);
	void ShowCombineOrder(QString strBatchNo, CThostFtdcOrderField *pOrder);
	void ShowCombineOrder(QString strBatchNo, CThostFtdcTradeField *pTrade);
	

private:
	//��ϵ�Map
	QMap<QString, STCombineInfo> m_MapCombineOrder;

	QTreeWidget* m_treeWidget;

    QGridLayout *MainLayout;
};

#endif // MAINCOMBINEENTRUSTWIDGET_H
