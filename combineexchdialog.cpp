#include "combineexchdialog.h"
#include "ui_combineexchdialog.h"
#include<QMessageBox>
#include"mainwindow.h"

CombineExchDialog::CombineExchDialog(void* pMainwindow,QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CombineExchDialog)
{
    ui->setupUi(this);

    ui->comboBox_Exchange->addItem("大连交易所");
    ui->comboBox_Exchange->addItem("郑州交易所");
	ui->comboBox_Exchange->addItem("显示全部");

    ui->tableWidget_contracts->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->tableWidget_contracts->verticalHeader()->setVisible(false);
    ui->tableWidget_contracts->setShowGrid(true);//显示表格线
    ui->tableWidget_contracts->setSelectionMode ( QAbstractItemView::SingleSelection); //设置选择模式，选择单行
    ui->tableWidget_contracts->setEditTriggers(QAbstractItemView::NoEditTriggers);//禁止编辑
    ui->tableWidget_contracts->horizontalHeader()->setStretchLastSection(true);//该命令只是将最后一行的列宽度等于Table剩下的宽度

    ui->tableWidget_selected->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->tableWidget_selected->verticalHeader()->setVisible(false);
    ui->tableWidget_selected->setShowGrid(true);//显示表格线
    ui->tableWidget_selected->setSelectionMode ( QAbstractItemView::SingleSelection); //设置选择模式，选择单行
    ui->tableWidget_selected->setEditTriggers(QAbstractItemView::NoEditTriggers);//禁止编辑
    ui->tableWidget_selected->horizontalHeader()->setStretchLastSection(true);//该命令只是将最后一行的列宽度等于Table剩下的宽度

	connect(ui->comboBox_Exchange,SIGNAL(currentTextChanged(const QString &)),this,SLOT(UpdateExchange(const QString &)));
    //添加按钮响应事件
    connect(ui->Button_Add,SIGNAL(clicked()),this,SLOT(ButtonAddClicked()));
    //删除按钮响应事件
    connect(ui->Button_Del,SIGNAL(clicked()),this,SLOT(ButtonDelClicked()));
    //上移按钮响应事件
    connect(ui->Button_Up,SIGNAL(clicked()),this,SLOT(ButtonUpClicked()));
    //下移按钮响应事件
    connect(ui->Button_Down,SIGNAL(clicked()),this,SLOT(ButtonDownClicked()));

    SetMainDlg(pMainwindow);
}

CombineExchDialog::~CombineExchDialog()
{
    delete ui;
}

void CombineExchDialog::SetMainDlg(void* pMainwindow){
    m_pMainwindow = pMainwindow;
    SetContractTable();
    UpdateExchange(ui->comboBox_Exchange->currentText());
}
void CombineExchDialog::UpdateExchange(const QString & text){

	ui->tableWidget_contracts->setRowCount(0);

	if(text == "郑州交易所" || text == "显示全部" ){
		int iRow = ui->tableWidget_contracts->rowCount();
		for (QMap<QString,CThostFtdcInstrumentField>::iterator it = ((MainWindow*)m_pMainwindow)->m_CZCEInstrumentMap.begin();
			it != ((MainWindow*)m_pMainwindow)->m_CZCEInstrumentMap.end(); ++it ) {
				ui->tableWidget_contracts->insertRow(iRow); //插入新行
				ui->tableWidget_contracts->setItem(iRow,0,new QTableWidgetItem(it.key()));
				QString strInstrumentName = QString::fromLocal8Bit(it.value().InstrumentName);  
				ui->tableWidget_contracts->setItem(iRow,1,new QTableWidgetItem(strInstrumentName));
				iRow++;
		}
	}
	if (text == "大连交易所" || text == "显示全部" ){
		int iRow = ui->tableWidget_contracts->rowCount();
		for (QMap<QString,CThostFtdcInstrumentField>::iterator it = ((MainWindow*)m_pMainwindow)->m_DCEInstrumentMap.begin();
			it != ((MainWindow*)m_pMainwindow)->m_DCEInstrumentMap.end(); ++it ) {
				ui->tableWidget_contracts->insertRow(iRow); //插入新行
				ui->tableWidget_contracts->setItem(iRow,0,new QTableWidgetItem(it.key()));
				QString strInstrumentName = QString::fromLocal8Bit(it.value().InstrumentName);
				ui->tableWidget_contracts->setItem(iRow,1,new QTableWidgetItem(strInstrumentName));
				iRow++;
		}
	}
	//根据内容调整列宽,给Table填充数据之后再调用该函数进行Resize
	//ui->tableWidget_Contracts->resizeColumnsToContents();
}
void CombineExchDialog::SetContractTable(){
	ui->tableWidget_selected->setRowCount(0);
    //显示交易所组合合约
    for(int i = 0; i < ((MainWindow*)m_pMainwindow)->m_CombineExchVector.size(); i++){
        QString strCombineExch =  ((MainWindow*)m_pMainwindow)->m_CombineExchVector[i];
        if("" != strCombineExch){
            ui->tableWidget_selected->insertRow(i); //插入新行
            ui->tableWidget_selected->setItem(i,0,new QTableWidgetItem(strCombineExch));
        }
    }
}
void CombineExchDialog::OKButtonClicked(){
    QMessageBox::information(NULL,QString("INFO"),QString("CombineExchDialog OK Button Clicked"));
    emit destroyWin();
}
//添加按钮响应事件
void CombineExchDialog::ButtonAddClicked(){
    QList<QTableWidgetItem *>  ItemsSelected  = ui->tableWidget_contracts->selectedItems();
    if(0 == ItemsSelected.size() ){//判断是否有选中的Item
        QMessageBox::warning(NULL,QString("WARNNING"),QString("Please select a valid Item"));
    }else{
        //选中的行号
        int iRow = ItemsSelected.at(0)->row();
        //合约代码
        QString strContractCode = ui->tableWidget_contracts->item(iRow,0)->text();

        //将选中的合约内容添加至已选合约列表中
        int iRowCnt = ui->tableWidget_selected->rowCount();
        ui->tableWidget_selected->insertRow(iRowCnt);
        ui->tableWidget_selected->setItem(iRowCnt,0,new QTableWidgetItem(strContractCode));
    }
}
//删除按钮响应事件
void CombineExchDialog::ButtonDelClicked(){
    QList<QTableWidgetItem *>  ItemsSelected  = ui->tableWidget_selected->selectedItems();
    if(0 == ItemsSelected.size() ){//判断已选合约中是否有选中的Item
        QMessageBox::warning(NULL,QString("WARNNING"),QString("Please select a valid Item"));
    }else{
        //选中的行号
        int iRow = ItemsSelected.at(0)->row();
        //删除选中的行
        ui->tableWidget_selected->removeRow(iRow);
    }
}
//上移按钮响应事件
void CombineExchDialog::ButtonUpClicked(){
    QList<QTableWidgetItem *>  ItemsSelected  = ui->tableWidget_selected->selectedItems();
    if(0 == ItemsSelected.size() ){//判断已选合约中是否有选中的Item
        QMessageBox::warning(NULL,QString("WARNNING"),QString("Please select a valid Item"));
    }else{
        //选中的行号
        int iRow = ItemsSelected.at(0)->row();
        if(0 != iRow ){
            ///上一行内容
            //合约代码
            QString strContractCode = ui->tableWidget_selected->item(iRow-1,0)->text();
            ///选中行的内容
            //合约代码
            QString strContractCodeSelected = ui->tableWidget_selected->item(iRow,0)->text();

            ui->tableWidget_selected->setItem(iRow -1 , 0 ,new QTableWidgetItem(strContractCodeSelected));
            ui->tableWidget_selected->setItem(iRow , 0 ,new QTableWidgetItem(strContractCode));
            ui->tableWidget_selected->selectRow(iRow -1);
        }
    }
}
//下移按钮响应事件
void CombineExchDialog::ButtonDownClicked(){
    QList<QTableWidgetItem *>  ItemsSelected  = ui->tableWidget_selected->selectedItems();
    if(0 == ItemsSelected.size() ){//判断已选合约中是否有选中的Item
        QMessageBox::warning(NULL,QString("WARNNING"),QString("Please select a valid Item"));
    }else{
        //行数
        int iCnt = ui->tableWidget_selected->rowCount();
        //选中的行号
        int iRow = ItemsSelected.at(0)->row();
        if(iCnt != iRow + 1  ){
            ///下一行内容
            //合约代码
            QString strContractCode = ui->tableWidget_selected->item(iRow+1,0)->text();
            ///选中行的内容
            //合约代码
            QString strContractCodeSelected = ui->tableWidget_selected->item(iRow,0)->text();

            ui->tableWidget_selected->setItem(iRow +1 , 0 ,new QTableWidgetItem(strContractCodeSelected));
            ui->tableWidget_selected->setItem(iRow , 0 ,new QTableWidgetItem(strContractCode));
            ui->tableWidget_selected->selectRow(iRow +1);
        }
    }
}
