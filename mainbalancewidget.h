﻿#ifndef MAINBALANCEWIDGET_H
#define MAINBALANCEWIDGET_H

#include <QWidget>
#include <qstandarditemmodel.h>
#include <qabstractitemmodel.h>
#include <string.h>
#include <qtableview.h>
#include <qheaderview.h>
#include <qgridlayout.h>
#include <QHBoxLayout>
#include <QLabel>
#include <QComboBox>
#include <QPushButton>
#include <QMenu>
#include <QAction>
#include "CtpInterface/ThostFtdcUserApiStruct.h"

class MainBalanceWidget : public QWidget
{
    Q_OBJECT
public:
    explicit MainBalanceWidget(QWidget *parent = 0);

signals:
	void ReqQryTradingAccount(QString);
public slots:
	void QueryButtonClicked();
	///邮件弹出菜单响应函数
	void RightClickedMenuPop(const QPoint& pos);

public:
    void setupModel();
    void setupViews();
	void ShowTradingAccount(CThostFtdcTradingAccountField *pTradingAccount);

public:
    QTableView*             m_tableBalance;
    QAbstractItemModel*     m_modelBalance;
    QItemSelectionModel*    m_selectionModel;
    QComboBox*              m_accountCombox;
    QPushButton*            m_querybutton;
    QPushButton*            m_exportbutton;

    QGridLayout*            m_MainLayout;
    QHBoxLayout*            m_hboxlayout;

	QMenu*					m_RightPopMenu;
    QAction*                m_updateAction;
    QAction*                m_outputAction;
};

#endif // MAINBALANCEWIDGET_H
