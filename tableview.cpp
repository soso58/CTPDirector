#include "tableview.h"
#include <QApplication>
/******************************************************
 * 自定义组合行情显示的代理
 * MarketComExchDelegate
 ******************************************************/
MarketComExchDelegate::MarketComExchDelegate(QObject *parent)
    : QItemDelegate(parent)
{
}

void MarketComExchDelegate::paint(QPainter* painter, const QStyleOptionViewItem& option, const QModelIndex& index) const
{
    switch(index.column()){
    default:
        QStyleOptionViewItem  viewOption(option);
        //高亮显示与普通显示时的前景色一致（即选中行和为选中时候的文字颜色一样）
        viewOption.palette.setColor(QPalette::HighlightedText, index.data(Qt::TextColorRole).value<QColor>());
        return QItemDelegate::paint (painter, viewOption, index);
    }
}
/******************************************************
 * 自选行情显示的代理
 * MarketSelfDelegate
 ******************************************************/
MarketSelfDelegate::MarketSelfDelegate(QObject *parent)
    : QItemDelegate(parent)
{
}

void MarketSelfDelegate::paint(QPainter* painter, const QStyleOptionViewItem& option, const QModelIndex& index) const
{
    switch(index.column()){
    case 1:
    case 2:
    case 4:
    case 8:
    case 9:{
        QModelIndex oIndex = index.child(index.row(),17);
        int iFlg = index.model()->data(oIndex, Qt::DisplayRole).toInt();
        QStyleOptionViewItem  viewOption(option);
        if (1 == iFlg){
            viewOption.palette.setColor(QPalette::Text, QColor(255,0,0));
            viewOption.palette.setColor(QPalette::HighlightedText, QColor(255,0,0));
        }else{
            viewOption.palette.setColor(QPalette::Text, QColor(0,255,0));
            viewOption.palette.setColor(QPalette::HighlightedText, QColor(0,255,0));
        }
        //viewOption.palette.setColor(QPalette::Highlight, QColor(125,255,255));
        return QItemDelegate::paint (painter, viewOption, index);
    }
    case 3:{
        QStyleOptionViewItem  viewOption(option);
        viewOption.palette.setColor(QPalette::Base, QColor(0,255,0));
        viewOption.palette.setColor(QPalette::HighlightedText, index.data(Qt::TextColorRole).value<QColor>());
        return QItemDelegate::paint (painter, viewOption, index);
    }
    default:
        QStyleOptionViewItem  viewOption(option);
        //高亮显示与普通显示时的前景色一致（即选中行和为选中时候的文字颜色一样）
        viewOption.palette.setColor(QPalette::HighlightedText, index.data(Qt::TextColorRole).value<QColor>());
        return QItemDelegate::paint (painter, viewOption, index);
    }
}

/********************TableModel********************/
TableModel::TableModel(QObject *parent)
    : QAbstractTableModel(parent), arr_row_list(NULL)
{
}

TableModel::~TableModel(void)
{
    arr_row_list = NULL;
}

void TableModel::setHorizontalHeaderList(QStringList horizontalHeaderList)
{
    horizontal_header_list = horizontalHeaderList;
}

void TableModel::setVerticalHeaderList(QStringList verticalHeaderList)
{
    vertical_header_list = verticalHeaderList;
}

int TableModel::rowCount(const QModelIndex &parent) const
{
    if(vertical_header_list.size() > 0)
        return vertical_header_list.size();

    if(NULL == arr_row_list)
        return 0;
    else
        return arr_row_list->size();
}

int TableModel::columnCount(const QModelIndex &parent) const
{
    if(horizontal_header_list.size() > 0)
        return horizontal_header_list.size();

    if(NULL == arr_row_list)
        return 0;
    else if(arr_row_list->size() < 1)
        return 0;
    else
        return arr_row_list->at(0).size();
}

QVariant TableModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if(NULL == arr_row_list)
        return QVariant();

    if(arr_row_list->size() < 1)
        return QVariant();

    if (role == Qt::TextAlignmentRole)
    {
        return int(Qt::AlignLeft | Qt::AlignVCenter);
    }
    else if (role == Qt::DisplayRole)
    {
        if(index.row() >= arr_row_list->size())
            return QVariant();
        if(index.column() >= arr_row_list->at(0).size())
            return QVariant();
        return arr_row_list->at(index.row()).at(index.column());
	}

    return QVariant();
}

QVariant TableModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if(role==Qt::DisplayRole)
    {
        if(orientation==Qt::Horizontal) // 水平表头
        {
            if(horizontal_header_list.size() > section)
                return horizontal_header_list[section];
            else
                return QVariant();
        }
        else
        {
            if(vertical_header_list.size() > section)
                return vertical_header_list[section]; // 垂直表头
            else
                return QVariant();
        }
    }
    return QVariant();
}

Qt::ItemFlags TableModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return Qt::NoItemFlags;

    Qt::ItemFlags flag = QAbstractItemModel::flags(index);

    // flag|=Qt::ItemIsEditable // 设置单元格可编辑,此处注释,单元格无法被编辑
    return flag;
}

void TableModel::setModalDatas(QList< QStringList > *rowlist)
{
    arr_row_list = rowlist;
}

void TableModel::refrushModel()
{
    beginResetModel();
    endResetModel();

    emit updateCount(this->rowCount(QModelIndex()));
}
void TableModel::UpdateData(int i)
{
	if (i<0) return;
	// 根据指定行列，得到index
	QModelIndex t1 = index(i, 0); 
	QModelIndex t2 = index(i, 17);
	emit dataChanged(t1, t2); // view good 最关键的刷新数据，不会取消所选项
}

/********************TableView********************/
TableView::TableView(QItemDelegate *delegate,QWidget *parent)
    : QTableView(parent)
{
    this->setAlternatingRowColors(false);//设置背景颜色交替出现
	this->setSelectionBehavior(QAbstractItemView::SelectRows);
    //this->horizontalHeader()->setStretchLastSection(true);//使最后一列填充剩余的空间
	this->horizontalHeader()->setHighlightSections(false); 
	this->verticalHeader()->setVisible(false);
	this->setShowGrid(false);
	this->setEditTriggers(QAbstractItemView::NoEditTriggers);
	this->setSelectionMode(QAbstractItemView::ExtendedSelection);

    model = new TableModel();
    this->setModel(model);
    model->setModalDatas(&grid_data_list);

    m_delegate = delegate;
    this->setItemDelegate(m_delegate);

    connect(model, &TableModel::updateCount, this, &TableView::updateCount);
}

TableView::~TableView(void)
{
    if(m_delegate) {
        delete m_delegate;
        m_delegate = NULL;
    }

    if(model) {
        delete model;
        model = NULL;
    }
    grid_data_list.clear();
}

void TableView::addRow(QStringList rowList)
{
    grid_data_list.append(rowList);
    model->refrushModel();
}

void TableView::remove()
{
    QModelIndexList model_index_list = this->selectedIndexes();
    int model_count = model_index_list.count();
    if(model_count <= 0)
        return;

    QList<int> list_row;
    for(int i=model_count-1; i>=0; i--)
    {
        QModelIndex model_index = model_index_list.at(i);
        int row = model_index.row();
        if(!list_row.contains(row))
            list_row.append(row);
    }

    if(list_row.isEmpty())
        return;

    qSort(list_row);

    for(int i=list_row.count()-1; i>=0; i--)
    {
        grid_data_list.removeAt(list_row.at(i));
    }

    model->refrushModel();
}

void TableView::clear()
{
    grid_data_list.clear();
    model->refrushModel();
}

int TableView::rowCount()
{
    return model->rowCount(QModelIndex());
}

void TableView::initHeader(QStringList header)
{
    model->setHorizontalHeaderList(header);
}

void TableView::UpdateRow(int iIndex,QStringList row_list)
{
    int row_count = this->rowCount();
	if(iIndex > row_count){
		return;
	}

    grid_data_list.replace(iIndex, row_list);

	model->UpdateData(iIndex);
}
