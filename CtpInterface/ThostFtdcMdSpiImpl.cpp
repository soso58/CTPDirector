#include "ThostFtdcMdSpiImpl.h"
#include <iostream>
#include <QMessageBox>
#include "../mainwindow.h"
#include <QDebug>

CThostFtdcMdSpiImpl::CThostFtdcMdSpiImpl(void)
{
	//参数初始化
	m_iRequestId = 0;
}

CThostFtdcMdSpiImpl::~CThostFtdcMdSpiImpl(void)
{
}

//设置主对话框指针
void CThostFtdcMdSpiImpl::SetMainDlg(void *lpDlg)
{
	m_lpMainDlg = lpDlg;
}

int CThostFtdcMdSpiImpl::GetRequestId()//获取请求函数的ID号
{
	m_iRequestId ++;
	return m_iRequestId;
}

void CThostFtdcMdSpiImpl::SetUserApi(CThostFtdcMdApi *pUserApi){
    m_pUserApi = pUserApi;
}

void  CThostFtdcMdSpiImpl::SetUserAndPassword(QString sBrokerid ,QString sUser,QString sPassword)
{
       m_accountid=sUser;
       m_password=sPassword;
       m_brokerid=sBrokerid;
}

///当客户端与交易后台建立起通信连接时（还未登录前），该方法被调用。
void CThostFtdcMdSpiImpl::OnFrontConnected()
{
	//((CtpTrader*)m_lpMainDlg)->OnMdFrontConnect();

    CThostFtdcReqUserLoginField logInfo;
    memset(&logInfo, 0, sizeof(CThostFtdcReqUserLoginField));
    strncpy(logInfo.BrokerID, m_brokerid.toStdString().c_str(), sizeof(TThostFtdcBrokerIDType));
    strncpy(logInfo.UserID,   m_accountid.toStdString().c_str(), sizeof(TThostFtdcUserIDType));
    strncpy(logInfo.Password, m_password.toStdString().c_str(), sizeof(TThostFtdcPasswordType));

    int iMdRet = m_pUserApi->ReqUserLogin(&logInfo, this->GetRequestId());
}

///当客户端与交易后台通信连接断开时，该方法被调用。当发生这个情况后，API会自动重新连接，客户端可不做处理。
///@param nReason 错误原因    
///        0x1001 网络读失败  
///        0x1002 网络写失败  
///        0x2001 接收心跳超时
///        0x2002 发送心跳失败
///        0x2003 收到错误报文
void CThostFtdcMdSpiImpl::OnFrontDisconnected(int nReason)
{
    //((CtpTrader*)m_lpMainDlg)->OnFrontConnectDisconn(nReason);
    //qDebug("OnFrontDisconnected")<<nReason;
    qDebug()<<nReason;
}
///心跳超时警告。当长时间未收到报文时，该方法被调用。
///@param nTimeLapse 距离上次接收报文的时间
void CThostFtdcMdSpiImpl::OnHeartBeatWarning(int nTimeLapse)
{

}
///登录请求响应
void CThostFtdcMdSpiImpl::OnRspUserLogin(CThostFtdcRspUserLoginField *pRspUserLogin, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
{
	std::string errStr(pRspInfo->ErrorMsg);
	((MainWindow*)m_lpMainDlg)->OnMdLogin(pRspInfo->ErrorID, QString::fromStdString(errStr));
}
///登出请求响应
void CThostFtdcMdSpiImpl::OnRspUserLogout(CThostFtdcUserLogoutField *pUserLogout, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
{
}
///错误应答
void CThostFtdcMdSpiImpl::OnRspError(CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
{
	qDebug("Md err occured");
    //((MainWindow*)m_lpMainDlg)->OnMdErrOccur(pRspInfo->ErrorID);
}
///订阅行情应答
void CThostFtdcMdSpiImpl::OnRspSubMarketData(CThostFtdcSpecificInstrumentField *pSpecificInstrument, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
{
	if (0 == pRspInfo->ErrorID){
        ((MainWindow*)m_lpMainDlg)->ShowStatusTip("订阅行情成功!");
	}else{
        ((MainWindow*)m_lpMainDlg)->ShowStatusTip("订阅行情失败!");
	}
}
///取消订阅行情应答
void CThostFtdcMdSpiImpl::OnRspUnSubMarketData(CThostFtdcSpecificInstrumentField *pSpecificInstrument, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
{
}
///订阅询价应答
void CThostFtdcMdSpiImpl::OnRspSubForQuoteRsp(CThostFtdcSpecificInstrumentField *pSpecificInstrument, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
{
}
///取消订阅询价应答
void CThostFtdcMdSpiImpl::OnRspUnSubForQuoteRsp(CThostFtdcSpecificInstrumentField *pSpecificInstrument, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
{
}
///深度行情通知
void CThostFtdcMdSpiImpl::OnRtnDepthMarketData(CThostFtdcDepthMarketDataField *pDepthMarketData)
{
	((MainWindow*)m_lpMainDlg)->OnDepthMarketData(pDepthMarketData);
}
///询价通知
void CThostFtdcMdSpiImpl::OnRtnForQuoteRsp(CThostFtdcForQuoteRspField *pForQuoteRsp)
{
}
