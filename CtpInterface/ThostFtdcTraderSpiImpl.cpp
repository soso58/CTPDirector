﻿#include "ThostFtdcTraderSpiImpl.h"
#include "../MainWindow.h"
#include"../Login/logindlg.h"
#include <QDebug>
#include <QTime>
#include <QStatusBar>
#include<Windows.h>

CThostFtdcTraderSpiImpl::CThostFtdcTraderSpiImpl(void)
{
	//参数初始化
	m_iRequestId = 0;
}


CThostFtdcTraderSpiImpl::~CThostFtdcTraderSpiImpl(void)
{
}

//设置主对话框指针
void CThostFtdcTraderSpiImpl::SetMainDlg(void *lpDlg)
{
	m_lpMainDlg = lpDlg;
}
//设置登录对话框指针
void CThostFtdcTraderSpiImpl::SetLoginDlg(void *lpDlg)
{
    m_lpLoginDlg = lpDlg;
}

int CThostFtdcTraderSpiImpl::GetRequestId()//获取请求函数的ID号
{
	m_iRequestId ++;
	return m_iRequestId;
}

void CThostFtdcTraderSpiImpl::SetUserApi(CThostFtdcTraderApi *pUserApi){
    m_pUserApi = pUserApi;
}

void  CThostFtdcTraderSpiImpl::SetUserAndPassword(QString sBrokerid ,QString sUser,QString sPassword)
{
       m_accountid=sUser;
       m_password=sPassword;
       m_brokerid=sBrokerid;
}


///当客户端与交易后台建立起通信连接时（还未登录前），该方法被调用。
void CThostFtdcTraderSpiImpl::OnFrontConnected()
{
    CThostFtdcReqUserLoginField logInfo;
    memset(&logInfo, 0, sizeof(CThostFtdcReqUserLoginField));
    strncpy(logInfo.BrokerID, m_brokerid.toStdString().c_str(), sizeof(TThostFtdcBrokerIDType));
    strncpy(logInfo.UserID,   m_accountid.toStdString().c_str(), sizeof(TThostFtdcUserIDType));
    strncpy(logInfo.Password, m_password.toStdString().c_str(), sizeof(TThostFtdcPasswordType));

    //((MainWindow*)m_lpMainDlg)->setStatusTip(QString("检验交易服务器资金账号与密码"));
    m_pUserApi->ReqUserLogin(&logInfo, this->GetRequestId());
}

///当客户端与交易后台通信连接断开时，该方法被调用。当发生这个情况后，API会自动重新连接，客户端可不做处理。
///@param nReason 错误原因
///        0x1001 网络读失败
///        0x1002 网络写失败
///        0x2001 接收心跳超时
///        0x2002 发送心跳失败
///        0x2003 收到错误报文
void CThostFtdcTraderSpiImpl::OnFrontDisconnected(int nReason)
{
	//emit SigFrontDisconnected(nReason);
}

///心跳超时警告。当长时间未收到报文时，该方法被调用。
///@param nTimeLapse 距离上次接收报文的时间
void CThostFtdcTraderSpiImpl::OnHeartBeatWarning(int nTimeLapse)
{
	//emit SigHeartBeatWarning(nTimeLapse);
}

///客户端认证响应
void CThostFtdcTraderSpiImpl::OnRspAuthenticate(CThostFtdcRspAuthenticateField *pRspAuthenticateField, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast) 
{
	//emit SigAuthenticate(pRspAuthenticateField, pRspInfo,nRequestID,bIsLast);
}


///登录请求响应
void CThostFtdcTraderSpiImpl::OnRspUserLogin(CThostFtdcRspUserLoginField *pRspUserLogin, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast) 
{
    ((CLoginDlg*)m_lpLoginDlg)->OnRspUserLogin(pRspInfo);
    if (0 == pRspInfo->ErrorID ){
        memcpy_s(&m_logininfo,sizeof(CThostFtdcRspUserLoginField),pRspUserLogin,sizeof(CThostFtdcRspUserLoginField));

        ((MainWindow*)m_lpMainDlg)->OnTdLogin(pRspInfo->ErrorID, QString(pRspInfo->ErrorMsg));
    }
}

///登出请求响应
void CThostFtdcTraderSpiImpl::OnRspUserLogout(CThostFtdcUserLogoutField *pUserLogout, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast) 
{
	//emit SigUserLogout(pUserLogout, pRspInfo,nRequestID,bIsLast);
}

///用户口令更新请求响应
void CThostFtdcTraderSpiImpl::OnRspUserPasswordUpdate(CThostFtdcUserPasswordUpdateField *pUserPasswordUpdate, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast) 
{
	//emit SigUserPasswordUpdate(pUserPasswordUpdate, pRspInfo,nRequestID,bIsLast);
}

///资金账户口令更新请求响应
void CThostFtdcTraderSpiImpl::OnRspTradingAccountPasswordUpdate(CThostFtdcTradingAccountPasswordUpdateField *pTradingAccountPasswordUpdate, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast) 
{
	//emit SigTradingAccountPasswordUpdate(pTradingAccountPasswordUpdate, pRspInfo,nRequestID,bIsLast);

}

///预埋单录入请求响应
void CThostFtdcTraderSpiImpl::OnRspParkedOrderInsert(CThostFtdcParkedOrderField *pParkedOrder, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast) 
{
	qDebug()<<"OnRspParkedOrderInsert";
	if (0 != pRspInfo->ErrorID){
		QString sMsg = QString::number(pRspInfo->ErrorID) +":"+ QString::fromLocal8Bit(pRspInfo->ErrorMsg);
        ((MainWindow*)m_lpMainDlg)->ShowStatusTip(sMsg);
	}else if(NULL != pParkedOrder){
		((MainWindow*)m_lpMainDlg)->OnRspParkedOrderInsert(pParkedOrder);
	}
}

///预埋撤单录入请求响应
void CThostFtdcTraderSpiImpl::OnRspParkedOrderAction(CThostFtdcParkedOrderActionField *pParkedOrderAction, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast) 
{
	qDebug()<<"OnRspParkedOrderAction";
}

///报单操作请求响应
void CThostFtdcTraderSpiImpl::OnRspOrderAction(CThostFtdcInputOrderActionField *pInputOrderAction, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast) 
{
	//emit SigOrderAction(pInputOrderAction, pRspInfo,nRequestID,bIsLast);
	qDebug()<<"OnRspOrderAction";
    ((MainWindow*)m_lpMainDlg)->ShowStatusTip(QString::fromLocal8Bit(pRspInfo->ErrorMsg));
}

///查询最大报单数量响应
void CThostFtdcTraderSpiImpl::OnRspQueryMaxOrderVolume(CThostFtdcQueryMaxOrderVolumeField *pQueryMaxOrderVolume, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast) 
{
	//emit SigQueryMaxOrderVolume(pQueryMaxOrderVolume, pRspInfo,nRequestID,bIsLast);
}

///投资者结算结果确认响应
void CThostFtdcTraderSpiImpl::OnRspSettlementInfoConfirm(CThostFtdcSettlementInfoConfirmField *pSettlementInfoConfirm, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast) 
{
    if (0 != pRspInfo->ErrorID){
		QString sMsg = QString::number(pRspInfo->ErrorID) +":"+ QString::fromLocal8Bit(pRspInfo->ErrorMsg);
        ((MainWindow*)m_lpMainDlg)->ShowStatusTip(sMsg);
    }else{
        //查询合约信息  查询合约信息在登录完成之后进行
		//((MainWindow*)m_lpMainDlg)->RequestAllInstrument();
		qDebug()<<QStringLiteral("客户结算单确认成功,确认时间：")<<QString(pSettlementInfoConfirm->ConfirmDate)<<" "<<QString(pSettlementInfoConfirm->ConfirmTime);
		((MainWindow*)m_lpMainDlg)->OnRspSettlementInfoConfirm(QString(pSettlementInfoConfirm->ConfirmDate) + " " + QString(pSettlementInfoConfirm->ConfirmTime));
	}
}

///删除预埋单响应
void CThostFtdcTraderSpiImpl::OnRspRemoveParkedOrder(CThostFtdcRemoveParkedOrderField *pRemoveParkedOrder, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast) 
{
	qDebug()<<"OnRspRemoveParkedOrder";
	if (0 != pRspInfo->ErrorID){
		QString sMsg = QString::number(pRspInfo->ErrorID) +":"+ QString::fromLocal8Bit(pRspInfo->ErrorMsg);
        ((MainWindow*)m_lpMainDlg)->ShowStatusTip(sMsg);
	}else{
		if (NULL != pRemoveParkedOrder)
		{
			((MainWindow*)m_lpMainDlg)->mainpreentrustwidget->RemoveParkedOrder(pRemoveParkedOrder);
		}
	}

}

///删除预埋撤单响应
void CThostFtdcTraderSpiImpl::OnRspRemoveParkedOrderAction(CThostFtdcRemoveParkedOrderActionField *pRemoveParkedOrderAction, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast) 
{
	qDebug()<<"OnRspRemoveParkedOrderAction";
}

///执行宣告录入请求响应
void CThostFtdcTraderSpiImpl::OnRspExecOrderInsert(CThostFtdcInputExecOrderField *pInputExecOrder, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast) 
{
	//emit SigExecOrderInsert(pInputExecOrder, pRspInfo,nRequestID,bIsLast);
}

///执行宣告操作请求响应
void CThostFtdcTraderSpiImpl::OnRspExecOrderAction(CThostFtdcInputExecOrderActionField *pInputExecOrderAction, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast) 
{
	//emit SigExecOrderAction(pInputExecOrderAction, pRspInfo,nRequestID,bIsLast);
}

///询价录入请求响应
void CThostFtdcTraderSpiImpl::OnRspForQuoteInsert(CThostFtdcInputForQuoteField *pInputForQuote, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast) 
{
	//emit SigForQuoteInsert(pInputForQuote, pRspInfo,nRequestID,bIsLast);
}

///报价录入请求响应
void CThostFtdcTraderSpiImpl::OnRspQuoteInsert(CThostFtdcInputQuoteField *pInputQuote, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast) 
{
	//emit SigQuoteInsert(pInputQuote, pRspInfo,nRequestID,bIsLast);
}

///报价操作请求响应
void CThostFtdcTraderSpiImpl::OnRspQuoteAction(CThostFtdcInputQuoteActionField *pInputQuoteAction, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast) 
{
	//emit SigQuoteAction(pInputQuoteAction, pRspInfo,nRequestID,bIsLast);
}

///申请组合录入请求响应
void CThostFtdcTraderSpiImpl::OnRspCombActionInsert(CThostFtdcInputCombActionField *pInputCombAction, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast) 
{
	//emit SigCombActionInsert(pInputCombAction, pRspInfo,nRequestID,bIsLast);
}

///请求查询报单响应
void CThostFtdcTraderSpiImpl::OnRspQryOrder(CThostFtdcOrderField *pOrder, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast) 
{
	if(pOrder != NULL){
		((MainWindow*)m_lpMainDlg)->OnRspQryOrder(pOrder);
	}
	qDebug()<<"OnRspQryOrder";
    if (bIsLast == true){
        ((MainWindow*)m_lpMainDlg)->ShowStatusTip(QStringLiteral("请求查询报单成功!"));
	}
}

///请求查询成交响应
void CThostFtdcTraderSpiImpl::OnRspQryTrade(CThostFtdcTradeField *pTrade, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast) 
{
    if(pTrade != NULL){
        ((MainWindow*)m_lpMainDlg)->OnRspQryTrade(pTrade);
    }
    if (bIsLast == true){
        qDebug()<<"OnRspQryTrade";
        ((MainWindow*)m_lpMainDlg)->ShowStatusTip(QStringLiteral("请求查询成交成功!"));
    }
}

///请求查询投资者持仓响应
void CThostFtdcTraderSpiImpl::OnRspQryInvestorPosition(CThostFtdcInvestorPositionField *pInvestorPosition, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast) 
{
	((MainWindow*)m_lpMainDlg)->OnRspQryInvestorPosition(pInvestorPosition,pRspInfo,nRequestID,bIsLast);  
}

///请求查询资金账户响应
void CThostFtdcTraderSpiImpl::OnRspQryTradingAccount(CThostFtdcTradingAccountField *pTradingAccount, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast) 
{
    ((MainWindow*)m_lpMainDlg)->OnRspQryTradingAccount(pTradingAccount,pRspInfo,nRequestID,bIsLast);
}

///请求查询投资者响应
void CThostFtdcTraderSpiImpl::OnRspQryInvestor(CThostFtdcInvestorField *pInvestor, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast) 
{
	//emit SigQryInvestor(pInvestor, pRspInfo,nRequestID,bIsLast);
}

///请求查询交易编码响应
void CThostFtdcTraderSpiImpl::OnRspQryTradingCode(CThostFtdcTradingCodeField *pTradingCode, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast) 
{
	//emit SigQryTradingCode(pTradingCode, pRspInfo,nRequestID,bIsLast);
}

///请求查询合约保证金率响应
void CThostFtdcTraderSpiImpl::OnRspQryInstrumentMarginRate(CThostFtdcInstrumentMarginRateField *pInstrumentMarginRate, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast) 
{
	//emit SigQryInstrumentMarginRate(pInstrumentMarginRate, pRspInfo,nRequestID,bIsLast);
}

///请求查询合约手续费率响应
void CThostFtdcTraderSpiImpl::OnRspQryInstrumentCommissionRate(CThostFtdcInstrumentCommissionRateField *pInstrumentCommissionRate, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast) 
{
	//emit SigQryInstrumentCommissionRate(pInstrumentCommissionRate, pRspInfo,nRequestID,bIsLast);
}

///请求查询交易所响应
void CThostFtdcTraderSpiImpl::OnRspQryExchange(CThostFtdcExchangeField *pExchange, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast) 
{
	//emit SigQryExchange(pExchange, pRspInfo,nRequestID,bIsLast);
}

///请求查询产品响应
void CThostFtdcTraderSpiImpl::OnRspQryProduct(CThostFtdcProductField *pProduct, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast) 
{
	//emit SigQryProduct(pProduct, pRspInfo,nRequestID,bIsLast);
}

///请求查询合约响应
void CThostFtdcTraderSpiImpl::OnRspQryInstrument(CThostFtdcInstrumentField *pInstrument, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast) 
{
	((MainWindow*)m_lpMainDlg)->OnRspQryInstrument(pInstrument, pRspInfo, nRequestID, bIsLast);
    if(true == bIsLast){
        ((CLoginDlg*)m_lpLoginDlg)->OnRspQryInstrument();
    }
}

///请求查询行情响应
void CThostFtdcTraderSpiImpl::OnRspQryDepthMarketData(CThostFtdcDepthMarketDataField *pDepthMarketData, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast) 
{
	//emit SigQryDepthMarketData(pDepthMarketData, pRspInfo,nRequestID,bIsLast);
}

///请求查询投资者结算结果响应
void CThostFtdcTraderSpiImpl::OnRspQrySettlementInfo(CThostFtdcSettlementInfoField *pSettlementInfo, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast) 
{
	((MainWindow*)m_lpMainDlg)->OnRspQrySettlementInfo(pSettlementInfo, pRspInfo, nRequestID, bIsLast);
}

///请求查询转帐银行响应
void CThostFtdcTraderSpiImpl::OnRspQryTransferBank(CThostFtdcTransferBankField *pTransferBank, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast) 
{
	//emit SigQryTransferBank(pTransferBank, pRspInfo,nRequestID,bIsLast);
}

///请求查询投资者持仓明细响应
void CThostFtdcTraderSpiImpl::OnRspQryInvestorPositionDetail(CThostFtdcInvestorPositionDetailField *pInvestorPositionDetail, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast) 
{
	//emit SigQryInvestorPositionDetail(pInvestorPositionDetail, pRspInfo,nRequestID,bIsLast);
}

///请求查询客户通知响应
void CThostFtdcTraderSpiImpl::OnRspQryNotice(CThostFtdcNoticeField *pNotice, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast) 
{
	//emit SigQryNotice(pNotice, pRspInfo,nRequestID,bIsLast);
}

///请求查询结算信息确认响应
void CThostFtdcTraderSpiImpl::OnRspQrySettlementInfoConfirm(CThostFtdcSettlementInfoConfirmField *pSettlementInfoConfirm, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast) 
{
    qDebug()<<QStringLiteral("请求查询结算信息确认响应");
    if (pSettlementInfoConfirm == NULL){//结算信息未确认
        Sleep(1000);
        //取上一个交易日的日期  这里只判断周末的情况
        QDate oDate = QDate::currentDate().addDays(-1);
        int idayOfWeek = oDate.dayOfWeek();
        if (idayOfWeek == 6){//周六的情况 往前推移一天
            oDate = oDate.addDays(-1);
        }else if(idayOfWeek == 7){//周日的情况 往前推移两天
            oDate = oDate.addDays(-2);
        }
        QString strDate = oDate.toString("yyyyMMdd");
        //请求查询投资者结算结果
        CThostFtdcQrySettlementInfoField oQrySettlementInfo;
        strncpy_s(oQrySettlementInfo.BrokerID,this->m_logininfo.BrokerID,sizeof(oQrySettlementInfo.BrokerID));///经纪公司代码
        strncpy_s(oQrySettlementInfo.InvestorID,this->m_logininfo.UserID,sizeof(oQrySettlementInfo.InvestorID));///投资者代码
        strncpy_s(oQrySettlementInfo.TradingDay,strDate.toStdString().c_str(),sizeof(oQrySettlementInfo.TradingDay));///交易日
        this->m_pUserApi->ReqQrySettlementInfo(&oQrySettlementInfo,this->GetRequestId());
    } else{ //结算信息已确认
        //客户结算单确认成功
        qDebug()<<QStringLiteral("客户结算单已确认成功,确认时间：")<<QString(pSettlementInfoConfirm->ConfirmDate)<<" "<<QString(pSettlementInfoConfirm->ConfirmTime);
        ((MainWindow*)m_lpMainDlg)->OnRspSettlementInfoConfirm(QString(pSettlementInfoConfirm->ConfirmDate) + " " + QString(pSettlementInfoConfirm->ConfirmTime));
    }
}

///请求查询投资者持仓明细响应
void CThostFtdcTraderSpiImpl::OnRspQryInvestorPositionCombineDetail(CThostFtdcInvestorPositionCombineDetailField *pInvestorPositionCombineDetail, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast) 
{
	//emit SigQryInvestorPositionCombineDetail(pInvestorPositionCombineDetail, pRspInfo,nRequestID,bIsLast);
}

///查询保证金监管系统经纪公司资金账户密钥响应
void CThostFtdcTraderSpiImpl::OnRspQryCFMMCTradingAccountKey(CThostFtdcCFMMCTradingAccountKeyField *pCFMMCTradingAccountKey, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast) 
{
	//emit SigQryCFMMCTradingAccountKey(pCFMMCTradingAccountKey, pRspInfo,nRequestID,bIsLast);
}

///请求查询仓单折抵信息响应
void CThostFtdcTraderSpiImpl::OnRspQryEWarrantOffset(CThostFtdcEWarrantOffsetField *pEWarrantOffset, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast) 
{
	//emit SigQryEWarrantOffset(pEWarrantOffset, pRspInfo,nRequestID,bIsLast);
}

///请求查询投资者品种/跨品种保证金响应
void CThostFtdcTraderSpiImpl::OnRspQryInvestorProductGroupMargin(CThostFtdcInvestorProductGroupMarginField *pInvestorProductGroupMargin, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast) 
{
	//emit SigQryInvestorProductGroupMargin(pInvestorProductGroupMargin, pRspInfo,nRequestID,bIsLast);
}

///请求查询交易所保证金率响应
void CThostFtdcTraderSpiImpl::OnRspQryExchangeMarginRate(CThostFtdcExchangeMarginRateField *pExchangeMarginRate, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast) 
{
	//emit SigQryExchangeMarginRate(pExchangeMarginRate, pRspInfo,nRequestID,bIsLast);
}

///请求查询交易所调整保证金率响应
void CThostFtdcTraderSpiImpl::OnRspQryExchangeMarginRateAdjust(CThostFtdcExchangeMarginRateAdjustField *pExchangeMarginRateAdjust, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast) 
{
	//emit SigQryExchangeMarginRateAdjust(pExchangeMarginRateAdjust, pRspInfo,nRequestID,bIsLast);
}

///请求查询汇率响应
void CThostFtdcTraderSpiImpl::OnRspQryExchangeRate(CThostFtdcExchangeRateField *pExchangeRate, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast) 
{
	//emit SigQryExchangeRate(pExchangeRate, pRspInfo,nRequestID,bIsLast);
}

///请求查询二级代理操作员银期权限响应
void CThostFtdcTraderSpiImpl::OnRspQrySecAgentACIDMap(CThostFtdcSecAgentACIDMapField *pSecAgentACIDMap, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast) 
{
	//emit SigQrySecAgentACIDMap(pSecAgentACIDMap, pRspInfo,nRequestID,bIsLast);
}

///请求查询产品报价汇率
void CThostFtdcTraderSpiImpl::OnRspQryProductExchRate(CThostFtdcProductExchRateField *pProductExchRate, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast) 
{
	//emit SigQryProductExchRate(pProductExchRate, pRspInfo,nRequestID,bIsLast);
}

///请求查询期权交易成本响应
void CThostFtdcTraderSpiImpl::OnRspQryOptionInstrTradeCost(CThostFtdcOptionInstrTradeCostField *pOptionInstrTradeCost, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast) 
{
	//emit SigQryOptionInstrTradeCost(pOptionInstrTradeCost, pRspInfo,nRequestID,bIsLast);
}

///请求查询期权合约手续费响应
void CThostFtdcTraderSpiImpl::OnRspQryOptionInstrCommRate(CThostFtdcOptionInstrCommRateField *pOptionInstrCommRate, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast) 
{
	//emit SigQryOptionInstrCommRate(pOptionInstrCommRate, pRspInfo,nRequestID,bIsLast);
}

///请求查询执行宣告响应
void CThostFtdcTraderSpiImpl::OnRspQryExecOrder(CThostFtdcExecOrderField *pExecOrder, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast) 
{
	//emit SigQryExecOrder(pExecOrder, pRspInfo,nRequestID,bIsLast);
}

///请求查询询价响应
void CThostFtdcTraderSpiImpl::OnRspQryForQuote(CThostFtdcForQuoteField *pForQuote, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast) 
{
	//emit SigQryForQuote(pForQuote, pRspInfo,nRequestID,bIsLast);
}

///请求查询报价响应
void CThostFtdcTraderSpiImpl::OnRspQryQuote(CThostFtdcQuoteField *pQuote, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast) 
{
	//emit SigQryQuote(pQuote, pRspInfo,nRequestID,bIsLast);
}

///请求查询组合合约安全系数响应
void CThostFtdcTraderSpiImpl::OnRspQryCombInstrumentGuard(CThostFtdcCombInstrumentGuardField *pCombInstrumentGuard, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast) 
{
	//emit SigQryCombInstrumentGuard(pCombInstrumentGuard, pRspInfo,nRequestID,bIsLast);
}

///请求查询申请组合响应
void CThostFtdcTraderSpiImpl::OnRspQryCombAction(CThostFtdcCombActionField *pCombAction, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast) 
{
	//emit SigQryCombAction(pCombAction, pRspInfo,nRequestID,bIsLast);
}

///请求查询转帐流水响应
void CThostFtdcTraderSpiImpl::OnRspQryTransferSerial(CThostFtdcTransferSerialField *pTransferSerial, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast) 
{
	//emit SigQryTransferSerial(pTransferSerial, pRspInfo,nRequestID,bIsLast);
}

///请求查询银期签约关系响应
void CThostFtdcTraderSpiImpl::OnRspQryAccountregister(CThostFtdcAccountregisterField *pAccountregister, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast) 
{
	//emit SigQryAccountregister(pAccountregister, pRspInfo,nRequestID,bIsLast);
}

///错误应答
void CThostFtdcTraderSpiImpl::OnRspError(CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast) 
{
	//emit SigError(pRspInfo,nRequestID,bIsLast);
}

///报单录入请求响应
void CThostFtdcTraderSpiImpl::OnRspOrderInsert(CThostFtdcInputOrderField *pInputOrder, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast) 
{
	qDebug() << "OnRspOrderInsert:"<<pRspInfo->ErrorID<<"-"<<QString::fromLocal8Bit(pRspInfo->ErrorMsg).toStdString().c_str();
    ((MainWindow*)m_lpMainDlg)->ShowStatusTip(QStringLiteral("错误代码:")+ QString::number(pRspInfo->ErrorID)+QStringLiteral(" 错误信息:")+QString::fromLocal8Bit(pRspInfo->ErrorMsg));
}

///报单通知
void CThostFtdcTraderSpiImpl::OnRtnOrder(CThostFtdcOrderField *pOrder) 
{
	qDebug() <<"OnRtnOrder";
	if (pOrder != NULL){
		((MainWindow*)m_lpMainDlg)->OnRtnOrder(pOrder);
	}
}

///成交通知
void CThostFtdcTraderSpiImpl::OnRtnTrade(CThostFtdcTradeField *pTrade) 
{
	qDebug() << "OnRtnTrade";
	if (pTrade != NULL){
		((MainWindow*)m_lpMainDlg)->OnRtnTrade(pTrade);
	}
}

///报单录入错误回报
void CThostFtdcTraderSpiImpl::OnErrRtnOrderInsert(CThostFtdcInputOrderField *pInputOrder, CThostFtdcRspInfoField *pRspInfo) 
{
	qDebug() << "OnErrRtnOrderInsert";
}

///报单操作错误回报
void CThostFtdcTraderSpiImpl::OnErrRtnOrderAction(CThostFtdcOrderActionField *pOrderAction, CThostFtdcRspInfoField *pRspInfo) 
{
	//emit SigErrOrderAction(pOrderAction, pRspInfo);
}

///合约交易状态通知
void CThostFtdcTraderSpiImpl::OnRtnInstrumentStatus(CThostFtdcInstrumentStatusField *pInstrumentStatus) 
{
	//emit SigInstrumentStatus(pInstrumentStatus);
}

///交易通知
void CThostFtdcTraderSpiImpl::OnRtnTradingNotice(CThostFtdcTradingNoticeInfoField *pTradingNoticeInfo) 
{
	//emit SigTradingNotice(pTradingNoticeInfo);
}

///提示条件单校验错误
void CThostFtdcTraderSpiImpl::OnRtnErrorConditionalOrder(CThostFtdcErrorConditionalOrderField *pErrorConditionalOrder) 
{
	//emit SigErrorConditionalOrder(pErrorConditionalOrder);
}

///执行宣告通知
void CThostFtdcTraderSpiImpl::OnRtnExecOrder(CThostFtdcExecOrderField *pExecOrder) 
{
	//emit SigExecOrder(pExecOrder);
}

///执行宣告录入错误回报
void CThostFtdcTraderSpiImpl::OnErrRtnExecOrderInsert(CThostFtdcInputExecOrderField *pInputExecOrder, CThostFtdcRspInfoField *pRspInfo) 
{
	//emit SigErrExecOrderInsert(pInputExecOrder, pRspInfo);
}

///执行宣告操作错误回报
void CThostFtdcTraderSpiImpl::OnErrRtnExecOrderAction(CThostFtdcExecOrderActionField *pExecOrderAction, CThostFtdcRspInfoField *pRspInfo) 
{
	//emit SigErrExecOrderAction(pExecOrderAction, pRspInfo);
}

///询价录入错误回报
void CThostFtdcTraderSpiImpl::OnErrRtnForQuoteInsert(CThostFtdcInputForQuoteField *pInputForQuote, CThostFtdcRspInfoField *pRspInfo) 
{
	//emit SigErrForQuoteInsert(pInputForQuote, pRspInfo);
}

///报价通知
void CThostFtdcTraderSpiImpl::OnRtnQuote(CThostFtdcQuoteField *pQuote) 
{
	//emit SigQuote(pQuote);
}

///报价录入错误回报
void CThostFtdcTraderSpiImpl::OnErrRtnQuoteInsert(CThostFtdcInputQuoteField *pInputQuote, CThostFtdcRspInfoField *pRspInfo) 
{
	//emit SigErrQuoteInsert(pInputQuote, pRspInfo);
}

///报价操作错误回报
void CThostFtdcTraderSpiImpl::OnErrRtnQuoteAction(CThostFtdcQuoteActionField *pQuoteAction, CThostFtdcRspInfoField *pRspInfo) 
{
	//emit SigErrQuoteAction(pQuoteAction, pRspInfo);
}

///询价通知
void CThostFtdcTraderSpiImpl::OnRtnForQuoteRsp(CThostFtdcForQuoteRspField *pForQuoteRsp) 
{
	//emit SigForQuoteRsp(pForQuoteRsp);
}

///保证金监控中心用户令牌
void CThostFtdcTraderSpiImpl::OnRtnCFMMCTradingAccountToken(CThostFtdcCFMMCTradingAccountTokenField *pCFMMCTradingAccountToken) 
{
	//emit SigCFMMCTradingAccountToken(pCFMMCTradingAccountToken);
}

///申请组合通知
void CThostFtdcTraderSpiImpl::OnRtnCombAction(CThostFtdcCombActionField *pCombAction) 
{
	//emit SigCombAction(pCombAction);
}

///申请组合录入错误回报
void CThostFtdcTraderSpiImpl::OnErrRtnCombActionInsert(CThostFtdcInputCombActionField *pInputCombAction, CThostFtdcRspInfoField *pRspInfo) 
{
	//emit SigErrCombActionInsert(pInputCombAction, pRspInfo);
}

///请求查询签约银行响应
void CThostFtdcTraderSpiImpl::OnRspQryContractBank(CThostFtdcContractBankField *pContractBank, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast) 
{
	//emit SigQryContractBank(pContractBank, pRspInfo,nRequestID,bIsLast);
}

///请求查询预埋单响应
void CThostFtdcTraderSpiImpl::OnRspQryParkedOrder(CThostFtdcParkedOrderField *pParkedOrder, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast) 
{
	//emit SigQryParkedOrder(pParkedOrder, pRspInfo,nRequestID,bIsLast);
}

///请求查询预埋撤单响应
void CThostFtdcTraderSpiImpl::OnRspQryParkedOrderAction(CThostFtdcParkedOrderActionField *pParkedOrderAction, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast) 
{
	//emit SigQryParkedOrderAction(pParkedOrderAction, pRspInfo,nRequestID,bIsLast);
}

///请求查询交易通知响应
void CThostFtdcTraderSpiImpl::OnRspQryTradingNotice(CThostFtdcTradingNoticeField *pTradingNotice, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast) 
{
	//emit SigQryTradingNotice(pTradingNotice, pRspInfo,nRequestID,bIsLast);
}

///请求查询经纪公司交易参数响应
void CThostFtdcTraderSpiImpl::OnRspQryBrokerTradingParams(CThostFtdcBrokerTradingParamsField *pBrokerTradingParams, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast) 
{
	//emit SigQryBrokerTradingParams(pBrokerTradingParams, pRspInfo,nRequestID,bIsLast);
}

///请求查询经纪公司交易算法响应
void CThostFtdcTraderSpiImpl::OnRspQryBrokerTradingAlgos(CThostFtdcBrokerTradingAlgosField *pBrokerTradingAlgos, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast) 
{
	//emit SigQryBrokerTradingAlgos(pBrokerTradingAlgos, pRspInfo,nRequestID,bIsLast);
}

///请求查询监控中心用户令牌
void CThostFtdcTraderSpiImpl::OnRspQueryCFMMCTradingAccountToken(CThostFtdcQueryCFMMCTradingAccountTokenField *pQueryCFMMCTradingAccountToken, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast) 
{
	//emit SigQueryCFMMCTradingAccountToken(pQueryCFMMCTradingAccountToken, pRspInfo,nRequestID,bIsLast);
}

///银行发起银行资金转期货通知
void CThostFtdcTraderSpiImpl::OnRtnFromBankToFutureByBank(CThostFtdcRspTransferField *pRspTransfer) 
{
	//emit SigFromBankToFutureByBank(pRspTransfer);
}

///银行发起期货资金转银行通知
void CThostFtdcTraderSpiImpl::OnRtnFromFutureToBankByBank(CThostFtdcRspTransferField *pRspTransfer) 
{
	//emit SigFromFutureToBankByBank(pRspTransfer);
}

///银行发起冲正银行转期货通知
void CThostFtdcTraderSpiImpl::OnRtnRepealFromBankToFutureByBank(CThostFtdcRspRepealField *pRspRepeal) 
{
	//emit SigRepealFromBankToFutureByBank(pRspRepeal);
}

///银行发起冲正期货转银行通知
void CThostFtdcTraderSpiImpl::OnRtnRepealFromFutureToBankByBank(CThostFtdcRspRepealField *pRspRepeal) 
{
	//emit SigRepealFromFutureToBankByBank(pRspRepeal);
}

///期货发起银行资金转期货通知
void CThostFtdcTraderSpiImpl::OnRtnFromBankToFutureByFuture(CThostFtdcRspTransferField *pRspTransfer) 
{
	//emit SigFromBankToFutureByFuture(pRspTransfer);
}

///期货发起期货资金转银行通知
void CThostFtdcTraderSpiImpl::OnRtnFromFutureToBankByFuture(CThostFtdcRspTransferField *pRspTransfer) 
{
	//emit SigFromFutureToBankByFuture(pRspTransfer);
}

///系统运行时期货端手工发起冲正银行转期货请求，银行处理完毕后报盘发回的通知
void CThostFtdcTraderSpiImpl::OnRtnRepealFromBankToFutureByFutureManual(CThostFtdcRspRepealField *pRspRepeal) 
{
	//emit SigRepealFromBankToFutureByFutureManual(pRspRepeal);
}

///系统运行时期货端手工发起冲正期货转银行请求，银行处理完毕后报盘发回的通知
void CThostFtdcTraderSpiImpl::OnRtnRepealFromFutureToBankByFutureManual(CThostFtdcRspRepealField *pRspRepeal) 
{
	//emit SigRepealFromFutureToBankByFutureManual(pRspRepeal);
}

///期货发起查询银行余额通知
void CThostFtdcTraderSpiImpl::OnRtnQueryBankBalanceByFuture(CThostFtdcNotifyQueryAccountField *pNotifyQueryAccount) 
{
	//emit SigQueryBankBalanceByFuture(pNotifyQueryAccount);
}

///期货发起银行资金转期货错误回报
void CThostFtdcTraderSpiImpl::OnErrRtnBankToFutureByFuture(CThostFtdcReqTransferField *pReqTransfer, CThostFtdcRspInfoField *pRspInfo) 
{
	//emit SigErrBankToFutureByFuture(pReqTransfer, pRspInfo);
}

///期货发起期货资金转银行错误回报
void CThostFtdcTraderSpiImpl::OnErrRtnFutureToBankByFuture(CThostFtdcReqTransferField *pReqTransfer, CThostFtdcRspInfoField *pRspInfo) 
{
	//emit SigErrFutureToBankByFuture(pReqTransfer, pRspInfo);
}

///系统运行时期货端手工发起冲正银行转期货错误回报
void CThostFtdcTraderSpiImpl::OnErrRtnRepealBankToFutureByFutureManual(CThostFtdcReqRepealField *pReqRepeal, CThostFtdcRspInfoField *pRspInfo) 
{
	//emit SigErrRepealBankToFutureByFutureManual(pReqRepeal, pRspInfo);
}

///系统运行时期货端手工发起冲正期货转银行错误回报
void CThostFtdcTraderSpiImpl::OnErrRtnRepealFutureToBankByFutureManual(CThostFtdcReqRepealField *pReqRepeal, CThostFtdcRspInfoField *pRspInfo) 
{
	//emit SigErrRepealFutureToBankByFutureManual(pReqRepeal, pRspInfo);
}

///期货发起查询银行余额错误回报
void CThostFtdcTraderSpiImpl::OnErrRtnQueryBankBalanceByFuture(CThostFtdcReqQueryAccountField *pReqQueryAccount, CThostFtdcRspInfoField *pRspInfo) 
{
	//emit SigErrQueryBankBalanceByFuture(pReqQueryAccount, pRspInfo);
}

///期货发起冲正银行转期货请求，银行处理完毕后报盘发回的通知
void CThostFtdcTraderSpiImpl::OnRtnRepealFromBankToFutureByFuture(CThostFtdcRspRepealField *pRspRepeal) 
{
	//emit SigRepealFromBankToFutureByFuture(pRspRepeal) ;
}

///期货发起冲正期货转银行请求，银行处理完毕后报盘发回的通知
void CThostFtdcTraderSpiImpl::OnRtnRepealFromFutureToBankByFuture(CThostFtdcRspRepealField *pRspRepeal) 
{
	//emit SigRepealFromFutureToBankByFuture(pRspRepeal);
}

///期货发起银行资金转期货应答
void CThostFtdcTraderSpiImpl::OnRspFromBankToFutureByFuture(CThostFtdcReqTransferField *pReqTransfer, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast) 
{
	//emit SigFromBankToFutureByFuture(pReqTransfer, pRspInfo,nRequestID,bIsLast);
}

///期货发起期货资金转银行应答
void CThostFtdcTraderSpiImpl::OnRspFromFutureToBankByFuture(CThostFtdcReqTransferField *pReqTransfer, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast) 
{
	//emit SigFromFutureToBankByFuture(pReqTransfer, pRspInfo,nRequestID,bIsLast);
}

///期货发起查询银行余额应答
void CThostFtdcTraderSpiImpl::OnRspQueryBankAccountMoneyByFuture(CThostFtdcReqQueryAccountField *pReqQueryAccount, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast) 
{
	//emit SigQueryBankAccountMoneyByFuture(pReqQueryAccount, pRspInfo,nRequestID,bIsLast);
}

///银行发起银期开户通知
void CThostFtdcTraderSpiImpl::OnRtnOpenAccountByBank(CThostFtdcOpenAccountField *pOpenAccount) 
{
	//emit SigOpenAccountByBank(pOpenAccount);
}

///银行发起银期销户通知
void CThostFtdcTraderSpiImpl::OnRtnCancelAccountByBank(CThostFtdcCancelAccountField *pCancelAccount) 
{
	//emit SigCancelAccountByBank(pCancelAccount);
}

///银行发起变更银行账号通知
void CThostFtdcTraderSpiImpl::OnRtnChangeAccountByBank(CThostFtdcChangeAccountField *pChangeAccount) 
{
	//emit SigChangeAccountByBank(pChangeAccount) ;
}
