#ifndef SINGLECONTROLDIALOG_H
#define SINGLECONTROLDIALOG_H

#include <QDialog>
#include "CtpInterface/ThostFtdcUserApiStruct.h"
#include"conditiondialog.h"

namespace Ui {
class SingleControlDlg;
}

class SingleControlDialog : public QDialog
{
    Q_OBJECT
public:
    explicit SingleControlDialog(QWidget *parent = 0);
    ~SingleControlDialog();

    void SetAccountID(QString strID);
signals:
    //报单请求
    void OnOrderInsert(CThostFtdcInputOrderField *pInputOrder);
    //预埋单请求
    void OnParkedOrderInsert(CThostFtdcParkedOrderField *pParkedOrder);
    //条件单请求
    void OnConditonOrderInsert(CThostFtdcInputOrderField* OrderInfo,ConditionType* oCondition);

public slots:
    //委托按钮触发事件响应
    void OnEntrust();
    //点击预埋按钮事件响应
    void OnPreEntrust();
    //点击条件按钮事件响应
    void OnCondition();
    void SelectContract(const QModelIndex & index);
    void OrderTypeChanged(QString OrderType);
    void GetCondition(ConditionType oCondition);

public:
    Ui::SingleControlDlg* ui;
    conditionDialog*    m_conditionDialog;

};

#endif // SINGLECONTROLDIALOG_H
