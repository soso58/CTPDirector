#ifndef POPMKTCOMBINEEXCHDIALOG_H
#define POPMKTCOMBINEEXCHDIALOG_H

#include <QDialog>
#include<QTableWidgetItem>

namespace Ui {
class PopMktCombineExchDialog;
}

class PopMktCombineExchDialog : public QDialog
{
    Q_OBJECT

public:
    explicit PopMktCombineExchDialog(void* pMainwindow,QWidget *parent = 0);
    ~PopMktCombineExchDialog();

	void SetMainDlg(void* pMainwindow);
	void SetContractTable();
	void UpdateExchange(QTableWidget * tablewiget,const QString & text);

signals:
    void UpdateCombineMarketdata();
    void destroyWin();

public slots:
    void OKButtonClicked();
    void FirstDoubleClicked(QTableWidgetItem* pItem);
    void SecondDoubleClicked(QTableWidgetItem* pItem);
    ///上移按钮单击响应事件
    void UpButtonClicked();
    ///下移按钮单击响应事件
    void DownButtonClicked();
    ///删除按钮单击响应事件
    void DelButtonClicked();
	//切换第一个Combobox响应事件
	void FirstUpdateExchange(const QString & text);
	//切换第二个Combobox响应事件
	void SecondUpdateExchange(const QString & text);

private:
	void* m_pMainwindow;
    Ui::PopMktCombineExchDialog *ui;
};

#endif // POPMKTCOMBINEEXCHDIALOG_H
