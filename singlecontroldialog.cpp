﻿#include "singlecontroldialog.h"
#include"ui_SingleControlDlg.h"
#include<QMessageBox>

SingleControlDialog::SingleControlDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SingleControlDlg)
{
    ui->setupUi(this);

    ui->m_OrderTypeCombox->addItem(QStringLiteral("限价"));
    ui->m_OrderTypeCombox->addItem(QStringLiteral("市价"));
    ui->m_OrderTypeCombox->addItem(QStringLiteral("套利"));
    ui->m_OrderTypeCombox->addItem(QStringLiteral("互换"));
    ui->m_OrderTypeCombox->addItem(QStringLiteral("套保"));
    ui->m_BSRadioBuy->setChecked(true);
    ui->m_OCRadioOpen->setChecked(true);

    ui->m_SpecifiedPriceSpinBox->setRange(0, 100000);//设置spinBox的值范围

    m_conditionDialog = new conditionDialog;

    //点击委托按钮事件响应
    connect(ui->m_OrderButton,SIGNAL(clicked()),this,SLOT(OnEntrust()));
    //点击预埋按钮事件响应
    connect(ui->m_PreentrustButton,SIGNAL(clicked()),this,SLOT(OnPreEntrust()));
    //点击条件按钮事件响应
    connect(ui->m_ConditionButton,SIGNAL(clicked()),this,SLOT(OnCondition()));
    //绑定用户切换订单类型 更改价格的事件响应
    connect(ui->m_OrderTypeCombox,SIGNAL(currentTextChanged(QString)),this,SLOT(OrderTypeChanged(QString)));
    //条件单确认按钮事件绑定
    connect(m_conditionDialog,SIGNAL(SigSendCondition(ConditionType)),this,SLOT(GetCondition(ConditionType)));
}
SingleControlDialog::~SingleControlDialog(){
    delete ui;
}

void SingleControlDialog::SetAccountID(QString strID){
    ui->m_AccountCombox->addItem(strID);
}

void SingleControlDialog::GetCondition(ConditionType oCondition){
    CThostFtdcInputOrderField OrderInfo;
    memset(&OrderInfo, 0, sizeof(CThostFtdcInputOrderField));

    ///合约代码
    if(ui->m_ContractLineEdit->text() != ""){
        strncpy(OrderInfo.InstrumentID, ui->m_ContractLineEdit->text().toStdString().c_str(), sizeof(TThostFtdcInstrumentIDType));
    }else{
        QMessageBox::critical(NULL,QString("ERROR"),QStringLiteral("请输入有效的合约代码！"));
        return;
    }
    ///买卖方向
    if(ui->m_BSRadioBuy->isChecked()){
        OrderInfo.Direction = THOST_FTDC_D_Buy;///买入
    }else if(ui->m_BSRadioSell->isChecked()){
        OrderInfo.Direction = THOST_FTDC_D_Sell;///卖出
    }
    if(ui->m_OrderTypeCombox->currentText() == QStringLiteral("限价")){
        ///报单价格条件类型：限价
        OrderInfo.OrderPriceType = THOST_FTDC_OPT_LimitPrice;
        ///价格
        OrderInfo.LimitPrice = oCondition.Price;
        OrderInfo.StopPrice =  oCondition.Price;
        ///有效期类型：当日有效
        OrderInfo.TimeCondition =   THOST_FTDC_TC_GFD;
    }else if(ui->m_OrderTypeCombox->currentText() == QStringLiteral("市价")){
        ///报单价格条件类型：任意价
        OrderInfo.OrderPriceType = THOST_FTDC_OPT_AnyPrice;
        ///价格
        OrderInfo.LimitPrice = 0;
        OrderInfo.StopPrice =  oCondition.Price;
        ///有效期类型：立即完成，否则撤销
        OrderInfo.TimeCondition =   THOST_FTDC_TC_IOC;
    }

    if(ui->m_OCRadioOpen->isChecked()){//开仓
        OrderInfo.CombOffsetFlag[0]='0';
    }else if(ui->m_OCRadioClose->isChecked()){//平仓
        OrderInfo.CombOffsetFlag[0]='1';
    }else if(ui->m_OCRadioCloseToday->isChecked()){//平今仓
        OrderInfo.CombOffsetFlag[0]='3';
    }
    ///组合投机套保标志 //0投机；1套保
    //OrderInfo.CombHedgeFlag[0]=GetHedgeFlag(myOrder->Hedge);
    OrderInfo.CombHedgeFlag[0]='1'	;

    ///数量
    OrderInfo.VolumeTotalOriginal = ui->m_HandsSpinBox->text().toInt();
    ///成交量类型 任意数量
    OrderInfo.VolumeCondition = THOST_FTDC_VC_AV;
    ///触发条件 最新价大于条件价
    if (oCondition.PriceKind == QStringLiteral("最新价"))
    {
        if (oCondition.Symbol == QStringLiteral(">")){
            OrderInfo.ContingentCondition = THOST_FTDC_CC_LastPriceGreaterThanStopPrice;///最新价大于条件价
        }else if(oCondition.Symbol == QStringLiteral(">=")){
            OrderInfo.ContingentCondition = THOST_FTDC_CC_LastPriceGreaterEqualStopPrice;///最新价大于等于条件价
        }else if(oCondition.Symbol == QStringLiteral("<")){
            OrderInfo.ContingentCondition = THOST_FTDC_CC_LastPriceLesserThanStopPrice;///最新价小于条件价
        }else if(oCondition.Symbol == QStringLiteral("<=")){
            OrderInfo.ContingentCondition = THOST_FTDC_CC_LastPriceLesserEqualStopPrice;///最新价小于等于条件价
        }
    }else if (oCondition.PriceKind == QStringLiteral("买价")){
        if (oCondition.Symbol == QStringLiteral(">")){
            OrderInfo.ContingentCondition = THOST_FTDC_CC_BidPriceGreaterThanStopPrice;///买一价大于条件价
        }else if(oCondition.Symbol == QStringLiteral(">=")){
            OrderInfo.ContingentCondition = THOST_FTDC_CC_BidPriceGreaterEqualStopPrice;///买一价大于等于条件价
        }else if(oCondition.Symbol == QStringLiteral("<")){
            OrderInfo.ContingentCondition = THOST_FTDC_CC_BidPriceLesserThanStopPrice;///买一价小于条件价
        }else if(oCondition.Symbol == QStringLiteral("<=")){
            OrderInfo.ContingentCondition = THOST_FTDC_CC_BidPriceLesserEqualStopPrice;///买一价小于等于条件价
        }
    }else if (oCondition.PriceKind == QStringLiteral("卖价")){
        if (oCondition.Symbol == QStringLiteral(">")){
            OrderInfo.ContingentCondition = THOST_FTDC_CC_AskPriceGreaterThanStopPrice;//卖一价大于条件价
        }else if(oCondition.Symbol == QStringLiteral(">=")){
            OrderInfo.ContingentCondition = THOST_FTDC_CC_AskPriceGreaterEqualStopPrice;///卖一价大于等于条件价
        }else if(oCondition.Symbol == QStringLiteral("<")){
            OrderInfo.ContingentCondition = THOST_FTDC_CC_AskPriceLesserThanStopPrice;///卖一价小于条件价
        }else if(oCondition.Symbol == QStringLiteral("<=")){
            OrderInfo.ContingentCondition = THOST_FTDC_CC_AskPriceLesserEqualStopPrice;///卖一价小于等于条件价
        }
    }

    ///最小成交量 1
    OrderInfo.MinVolume = 1;
    ///强平原因 非强平
    OrderInfo.ForceCloseReason = THOST_FTDC_FCC_NotForceClose;
    ///自动挂起标志 否
    OrderInfo.IsAutoSuspend = 0;
    ///用户强平标志 否
    OrderInfo.UserForceClose = 0;

    emit OnConditonOrderInsert( &OrderInfo, &oCondition);
}

void SingleControlDialog::OrderTypeChanged(QString OrderType){
    if(OrderType == QStringLiteral("市价")){
        ui->m_SpecifiedPriceSpinBox->setValue(0.0);
        ui->m_SpecifiedPriceSpinBox->setEnabled(false);
    }else{
        ui->m_SpecifiedPriceSpinBox->setEnabled(true);
    }
}

/**
 * 委托按钮触发事件响应
 * @brief ControlWidget::OnEntrust
 */
void SingleControlDialog::OnEntrust(){
    CThostFtdcInputOrderField OrderInfo;
    memset(&OrderInfo, 0, sizeof(CThostFtdcInputOrderField));

    ///合约代码
    if(ui->m_ContractLineEdit->text() != ""){
        strncpy(OrderInfo.InstrumentID, ui->m_ContractLineEdit->text().toStdString().c_str(), sizeof(TThostFtdcInstrumentIDType));
    }else{
        QMessageBox::critical(NULL,QString("ERROR"),QStringLiteral("请输入有效的合约代码！"));
        return;
    }
    ///买卖方向
    if(ui->m_BSRadioBuy->isChecked()){
        OrderInfo.Direction = THOST_FTDC_D_Buy;///买入
    }else if(ui->m_BSRadioSell->isChecked()){
        OrderInfo.Direction = THOST_FTDC_D_Sell;///卖出
    }
    if(ui->m_OrderTypeCombox->currentText() == QStringLiteral("限价")){
        ///报单价格条件类型：限价
        OrderInfo.OrderPriceType = THOST_FTDC_OPT_LimitPrice;
        ///价格
        OrderInfo.LimitPrice = ui->m_SpecifiedPriceSpinBox->text().toDouble();
        ///有效期类型：当日有效
        OrderInfo.TimeCondition =   THOST_FTDC_TC_GFD;
    }else if(ui->m_OrderTypeCombox->currentText() == QStringLiteral("市价")){
        ///报单价格条件类型：任意价
        OrderInfo.OrderPriceType = THOST_FTDC_OPT_AnyPrice;
        ///价格
        OrderInfo.LimitPrice = 0;
        ///有效期类型：立即完成，否则撤销
        OrderInfo.TimeCondition =   THOST_FTDC_TC_IOC;
    }

    if(ui->m_OCRadioOpen->isChecked()){//开仓
        OrderInfo.CombOffsetFlag[0]='0';
    }else if(ui->m_OCRadioClose->isChecked()){//平仓
        OrderInfo.CombOffsetFlag[0]='1';
    }else if(ui->m_OCRadioCloseToday->isChecked()){//平今仓
        OrderInfo.CombOffsetFlag[0]='3';
    }
    ///组合投机套保标志 //0投机；1套保
    //OrderInfo.CombHedgeFlag[0]=GetHedgeFlag(myOrder->Hedge);
    OrderInfo.CombHedgeFlag[0]='1'	;

    ///数量
    OrderInfo.VolumeTotalOriginal = ui->m_HandsSpinBox->text().toInt();
    ///成交量类型 任意数量
    OrderInfo.VolumeCondition = THOST_FTDC_VC_AV;
    ///触发条件:立即
    OrderInfo.ContingentCondition = THOST_FTDC_CC_Immediately;

    ///最小成交量 1
    OrderInfo.MinVolume = 1;
    ///强平原因 非强平
    OrderInfo.ForceCloseReason = THOST_FTDC_FCC_NotForceClose;
    ///自动挂起标志 否
    OrderInfo.IsAutoSuspend = 0;
    ///用户强平标志 否
    OrderInfo.UserForceClose = 0;

    emit OnOrderInsert(&OrderInfo);
}
//点击预埋按钮事件响应
void SingleControlDialog::OnPreEntrust(){
    CThostFtdcParkedOrderField pParkedOrder;
    memset(&pParkedOrder, 0, sizeof(CThostFtdcParkedOrderField));
    ///合约代码
    if(ui->m_ContractLineEdit->text() != ""){
        strncpy(pParkedOrder.InstrumentID, ui->m_ContractLineEdit->text().toStdString().c_str(), sizeof(TThostFtdcInstrumentIDType));
    }else{
        QMessageBox::critical(NULL,QString("ERROR"),QStringLiteral("请输入有效的合约代码！"));
        return;
    }
    ///买卖方向
    if(ui->m_BSRadioBuy->isChecked()){
        pParkedOrder.Direction = THOST_FTDC_D_Buy;///买入
    }else if(ui->m_BSRadioSell->isChecked()){
        pParkedOrder.Direction = THOST_FTDC_D_Sell;///卖出
    }
    if(ui->m_OrderTypeCombox->currentText() == QStringLiteral("限价")){
        ///报单价格条件类型：限价
        pParkedOrder.OrderPriceType = THOST_FTDC_OPT_LimitPrice;
        ///价格
        pParkedOrder.LimitPrice = ui->m_SpecifiedPriceSpinBox->text().toDouble();
        ///有效期类型：当日有效
        pParkedOrder.TimeCondition =   THOST_FTDC_TC_GFD;
    }else if(ui->m_OrderTypeCombox->currentText() == QStringLiteral("市价")){
        ///报单价格条件类型：任意价
        pParkedOrder.OrderPriceType = THOST_FTDC_OPT_AnyPrice;
        ///价格
        pParkedOrder.LimitPrice = 0;
        ///有效期类型：立即完成，否则撤销
        pParkedOrder.TimeCondition =   THOST_FTDC_TC_IOC;
    }

    if(ui->m_OCRadioOpen->isChecked()){//开仓
        pParkedOrder.CombOffsetFlag[0]='0';
    }else if(ui->m_OCRadioClose->isChecked()){//平仓
        pParkedOrder.CombOffsetFlag[0]='1';
    }else if(ui->m_OCRadioCloseToday->isChecked()){//平今仓
        pParkedOrder.CombOffsetFlag[0]='3';
    }
    ///组合投机套保标志 //0投机；1套保
    //OrderInfo.CombHedgeFlag[0]=GetHedgeFlag(myOrder->Hedge);
    pParkedOrder.CombHedgeFlag[0]='1'	;

    ///数量
    pParkedOrder.VolumeTotalOriginal = ui->m_HandsSpinBox->text().toInt();
    ///成交量类型 任意数量
    pParkedOrder.VolumeCondition = THOST_FTDC_VC_AV;
    ///触发条件:立即
    pParkedOrder.ContingentCondition = THOST_FTDC_CC_Immediately;

    ///最小成交量 1
    pParkedOrder.MinVolume = 1;
    ///强平原因 非强平
    pParkedOrder.ForceCloseReason = THOST_FTDC_FCC_NotForceClose;
    ///自动挂起标志 否
    pParkedOrder.IsAutoSuspend = 0;
    ///用户强平标志 否
    pParkedOrder.UserForceClose = 0;

    emit OnParkedOrderInsert(&pParkedOrder);
}
//点击条件按钮事件响应
void SingleControlDialog::OnCondition(){
    QString strName = ui->m_ContractLineEdit->text();
    if(strName.trimmed() == ""){
        QMessageBox::critical(NULL,QString("ERROR"),QStringLiteral("请选择有效合约!"));
    }else{
        int iCnt = ui->m_HandsSpinBox->text().toInt();
        if(0 == iCnt){
            QMessageBox::critical(NULL,QString("ERROR"),QStringLiteral("手数不符!"));
        }else{
            double dprice = ui->m_SpecifiedPriceSpinBox->text().toDouble();
            m_conditionDialog->SetPrice(dprice);
            //弹出设置触发条件对话框
            m_conditionDialog->exec();
        }
    }
}
void SingleControlDialog::SelectContract(const QModelIndex & index){
    int iRow = index.row();
    //合约
    QModelIndex oContract = index.sibling(iRow,0);
    QString strContract = oContract.data().toString();
    ui->m_ContractLineEdit->setText(strContract);
    //最新价
    QModelIndex oPrice = index.sibling(iRow,1);
    double i = oPrice.data().toDouble();
    ui->m_SpecifiedPriceSpinBox->setValue(i);
    //手数
    ui->m_HandsSpinBox->setValue(1);
}
