#ifndef MAINPOPDIALOG_H
#define MAINPOPDIALOG_H

#include <QDialog>

namespace Ui {
class MainPopDialog;
}

class MainPopDialog : public QDialog
{
    Q_OBJECT

public:
    explicit MainPopDialog(QWidget *parent = 0);
    ~MainPopDialog();

private slots:
    void on_treeWidget_clicked(const QModelIndex &index);

public:
    Ui::MainPopDialog *ui;
};

#endif // MAINPOPDIALOG_H
