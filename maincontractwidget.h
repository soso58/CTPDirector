﻿#ifndef MAINCONTRACTWIDGET_H
#define MAINCONTRACTWIDGET_H

#include <QWidget>
#include <qstandarditemmodel.h>
#include <qabstractitemmodel.h>
#include <string.h>
#include <qtableview.h>
#include <qheaderview.h>
#include <qgridlayout.h>
#include "CtpInterface/ThostFtdcUserApiStruct.h"

class MainContractWidget : public QWidget
{
    Q_OBJECT
public:
    explicit MainContractWidget(QWidget *parent = 0);

signals:

public slots:
public:
    void setupModel();
    void setupViews();
	void AddRows(QMap<QString,CThostFtdcInstrumentField> & oInstrumentMap);
    void AddRow(CThostFtdcInstrumentField* oInstrument);

public:
    QTableView* tableContract;
    QAbstractItemModel *modelContract;

private:
    QItemSelectionModel *selectionModel;
    QGridLayout *MainLayout;
};


#endif // MAINCONTRACTWIDGET_H
