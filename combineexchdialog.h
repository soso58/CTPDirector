#ifndef COMBINEEXCHDIALOG_H
#define COMBINEEXCHDIALOG_H

#include <QDialog>

namespace Ui {
class CombineExchDialog;
}

class CombineExchDialog : public QDialog
{
    Q_OBJECT

public:
    explicit CombineExchDialog(void* pMainwindow,QWidget *parent = 0);
    ~CombineExchDialog();

    void SetMainDlg(void* pMainwindow);
    void SetContractTable();
	

signals:
    void destroyWin();
public slots:
	void UpdateExchange(const QString & text);
    void OKButtonClicked();
    void ButtonAddClicked();
    void ButtonDelClicked();
    void ButtonUpClicked();
    void ButtonDownClicked();
private:
    Ui::CombineExchDialog *ui;
    void* m_pMainwindow;
};

#endif // COMBINEEXCHDIALOG_H
