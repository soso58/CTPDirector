#ifndef QUICKORDERDIALOG_H
#define QUICKORDERDIALOG_H

#include <QDialog>

namespace Ui {
class QuickOrderDialog;
}

class QuickOrderDialog : public QDialog
{
    Q_OBJECT

public:
    explicit QuickOrderDialog(QWidget *parent = 0);
    ~QuickOrderDialog();
signals:
    void destroyWin();
public slots:
    void OKButtonClicked();

private:
    Ui::QuickOrderDialog *ui;
};

#endif // QUICKORDERDIALOG_H
