#ifndef COMBINECONTROLDIALOG_H
#define COMBINECONTROLDIALOG_H

#include <QDialog>

namespace Ui {
class CombineControlDlg;
}

class CombineControlDialog : public QDialog
{
    Q_OBJECT
public:
    explicit CombineControlDialog(QWidget *parent = 0);
    ~CombineControlDialog();
    ///选择自定义组合行情
    void SelectCombContract(const QModelIndex & oIndex);
    void SetAccountName(QString strAccountName);

signals:
    //void NewCombineOrder(COMBINORDERINFO typeOrder);

public slots:
	void OKButtonClicked();
public:
    Ui::CombineControlDlg* ui;

};

#endif // COMBINECONTROLDIALOG_H
