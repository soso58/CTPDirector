#ifndef DEFAULTORDERDIALOG_H
#define DEFAULTORDERDIALOG_H

#include <QDialog>
#include <QTableWidgetItem>

#define QT_NAMESPACE

namespace Ui {
class DefaultOrderDialog;
}

class DefaultOrderDialog : public QDialog
{
    Q_OBJECT

public:
    explicit DefaultOrderDialog(void* pMainwindow,QWidget *parent = 0);
    ~DefaultOrderDialog();
	void SetMainDlg(void* pMainwindow);

signals:
    void destroyWin();
public slots:
    void OKButtonClicked();
    void ButtonCommitClicked();
    void ButtonDelClicked();
    void ItemClicked(QTableWidgetItem* pItem);
    void ItemDoubleClicked(QTableWidgetItem* pItem);

private:
    Ui::DefaultOrderDialog*	ui;
	void*					m_pMainwindow;
};

#endif // DEFAULTORDERDIALOG_H
