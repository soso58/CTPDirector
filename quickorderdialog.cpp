#include "quickorderdialog.h"
#include "ui_quickorderdialog.h"
#include<QMessageBox>

QuickOrderDialog::QuickOrderDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::QuickOrderDialog)
{
    ui->setupUi(this);
}

QuickOrderDialog::~QuickOrderDialog()
{
    delete ui;
}
void QuickOrderDialog::OKButtonClicked(){
    QMessageBox::information(NULL,QString("INFO"),QString("QuickOrderDialog OK Button Clicked"));
    emit destroyWin();
}
