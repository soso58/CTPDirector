/************************************
*************************************
       自选行情显示页面
*************************************
*************************************/
#ifndef MARKETSELFWIDGET_H
#define MARKETSELFWIDGET_H

#include <QWidget>
#include <string.h>
#include <qgridlayout.h>
#include "CtpInterface/ThostFtdcUserApiStruct.h"
#include "tableview.h"
#include <QMenu>
#include <QAction>

class MarketSelfWidget : public QWidget
{
    Q_OBJECT
public:
    explicit MarketSelfWidget(QWidget *parent = 0);

signals:
    void UpdateRow(int iIndex,QStringList row_list);
public slots:

public:
    void setupModel();
    void setupViews();
    void ShowDepthMarketData(CThostFtdcDepthMarketDataField *pDepthMarketData);

public:
    TableView*  m_tableView;
    QItemDelegate* m_MarketselfDelegate;
    //QTableView* tableMarketSelf;
    //QAbstractItemModel *modelMarketSelf;

private:
    QItemSelectionModel*	selectionModel;
    QGridLayout*			MainLayout;
};

#endif // MARKETSELFWIDGET_H
